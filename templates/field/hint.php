<?php
use Wt\Core\Templater\Tools as TemplaterTools;
?>
<div class="kit-field__hint kit-icon" data-bs-toggle="tooltip" title="<?=TemplaterTools::safeHtmlAttr($arParams['TITLE'])?>">
    <?app()->service()->templater()->render('kit:svg/hint')?>
</div>