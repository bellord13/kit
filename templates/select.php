<?php

/**
 * @var array $arParams
 */

use Wt\Core\Templater\Tools as TemplaterTools;
use Wt\Core\Tools;
use \Wt\Core\Type;

$arParamsDefault = [
    'AUTO_SELECT' => true,
    'OPTIONS_VALUE_KEY' => 'ID',
    'OPTIONS_TEXT_KEY' => 'NAME',
    'ADD_CLASS' => '',
    'CLASS' => '',
    'LABEL_CLASS' => '',
    'ID' => '',
    'NAME' => '',
    'ONCHANGE' => '',
    'HIDDEN' => true, // || 'TYPE' => 'hidden'
    'SELECTED' => ' id ',
    'LABEL' => '',
    'MULTIPLE' => '',
    'ARROW_IN_LABEL' => false,
    'OPTIONS_HTML' => '', // !!
    'CHECK_DIRTY' => true, // !!
    'OPTIONS' => [],
];
$optionDefault = [
    'ID' => '',
    'NAME' => '',
    'ATTR' => '',
    'VISIBLE' => true,
    'SELECTED' => 'Y',
    'DISABLED' => 'Y', // || RESTRICTED
];

if(!isset($arParams['AUTO_SELECT'])) {
    $arParams['AUTO_SELECT'] = $arParamsDefault['AUTO_SELECT'];
}
$arParams['AUTO_SELECT'] = Tools::isTrue($arParams['AUTO_SELECT']);

if(!isset($arParams['OPTIONS_VALUE_KEY'])) {
    $arParams['OPTIONS_VALUE_KEY'] = $arParamsDefault['OPTIONS_VALUE_KEY'];
}
if(!isset($arParams['OPTIONS_TEXT_KEY'])) {
    $arParams['OPTIONS_TEXT_KEY'] = $arParamsDefault['OPTIONS_TEXT_KEY'];
}
$arParams['OPTIONS'] = (array)$arParams['OPTIONS'];


$arrowInLabel = Tools::isTrue($arParams['ARROW_IN_LABEL']);

if(!isset($arParams['CHECK_DIRTY'])) {
    $arParams['CHECK_DIRTY'] = $arParamsDefault['CHECK_DIRTY'];
}
$checkDirty = Tools::isTrue($arParams['CHECK_DIRTY']);

$optionSelectedArray = [];
$hidden = Tools::isTrue($arParams['HIDDEN']) || ($arParams['TYPE'] == 'hidden');
$multiple = Tools::isTrue($arParams['MULTIPLE']);
$optionsHtml = '';

if($arParams['OPTIONS']) {
    if(strlen($arParams['SELECTED'])) {
        foreach ($arParams['OPTIONS'] as $option) {
            if(isset($option['VISIBLE']) && !Tools::isTrue($option['VISIBLE'])) continue;
            if($arParams['SELECTED'] == Type\Arr::get($option, $arParams['OPTIONS_VALUE_KEY'])) {
                $optionSelectedArray = $option;
                break;
            }
        }
    }

    if($arParams['AUTO_SELECT']) {
        if(!$optionSelectedArray) {
            foreach ($arParams['OPTIONS'] as $option) {
                if(isset($option['VISIBLE']) && !Tools::isTrue($option['VISIBLE'])) continue;
                if(Tools::isTrue($option['SELECTED'])) {
                    $optionSelectedArray = $option;
                    break;
                }
            }
        }

        if(!$optionSelectedArray) {
            $optionSelectedArray = reset($arParams['OPTIONS']);
        }
    }
}
$optionsCount = 0;
foreach ($arParams['OPTIONS'] as $option) {
    if(isset($option['VISIBLE']) && !Tools::isTrue($option['VISIBLE'])) continue;
    $isSelected = (Type\Arr::get($optionSelectedArray, $arParams['OPTIONS_VALUE_KEY']) == Type\Arr::get($option, $arParams['OPTIONS_VALUE_KEY']));
    $isDisabled = Tools::isTrue($option['DISABLED']) || Tools::isTrue($option['RESTRICTED']);

    $optionsCount++;
    $optionsHtml .= ' <option value="'.Type\Arr::get($option, $arParams['OPTIONS_VALUE_KEY']).'" '. TemplaterTools::getAttrByArray($option['ATTR']) .
        ($isSelected?' selected':'').''.
        ($isDisabled?' disabled':'').'>'.Type\Arr::get($option, $arParams['OPTIONS_TEXT_KEY']).'</option>';
}



?><label class="kit-field kit-field--select<?=($hidden?' kit-hidden':'')?> <?=$arParams['LABEL_CLASS']?>">
    <select data-dirty="<?=(!$checkDirty || $optionSelectedArray)?'true':'false'?>"
         name="<?=$arParams['NAME']?>"
        <?if($arParams['ID']){?>id="<?=$arParams['ID']?>"<?}?>
        <?if($multiple){?> multiple<?}?>
        <?if($arParams['ONCHANGE']){?> onchange="<?=TemplaterTools::safeHtmlAttr($arParams['ONCHANGE'])?>"<?}?>
        class="kit-field__input <?=$arParams['CLASS']?> <?=$arParams['ADD_CLASS']?>"
        <?=TemplaterTools::getAttrByArray($arParams['ATTR'])?>
        <?if($arParams['PLACEHOLDER']){?>data-placeholder="<?=$arParams['PLACEHOLDER']?>"<?}?>
    ><?echo $optionsHtml?:$arParams['OPTIONS_HTML']; ?></select>
    <?
    if($arParams['HINT']){
        app()->service()->templater()->render('kit:field/hint', ['TITLE' => $arParams['HINT']]);
    }?>
    <?if(!$arrowInLabel){?><span class="kit-field__arrow"><?=app()->service()->templater()->get('kit:svg/chevronAnimate')?></span><?}?>
    <span class="kit-field__display"><?=Type\Arr::get($optionSelectedArray, $arParams['OPTIONS_TEXT_KEY'])?:$arParams['PLACEHOLDER']?></span>
    <span class="kit-field__label"><?=$arParams['LABEL']?:$arParams['NAME']?:''?><?if($arrowInLabel){?><span class="kit-field__arrow"><?=app()->service()->templater()->get('svg/chevronAnimate')?></span><?}?></span>
    <span class="kit-field__error"></span>
    <span class="kit-field__hr"></span>
    <?if($arParams['AREA_LEFT']){?><div class="kit-field__area-left"><?=$arParams['AREA_LEFT']?></div><?}?>
    <?if($arParams['AREA_RIGHT']){?><div class="kit-field__area-right"><?=$arParams['AREA_RIGHT']?></div><?}?>
</label><?

?>



