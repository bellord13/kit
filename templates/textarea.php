<?php
/**
 * @var array $arParams
 */

use Wt\Core\Templater\Tools as TemplaterTools;
use Wt\Core\Tools;

//include ".con/fields.php";

$arParams['TYPE'] = $arParams['TYPE']?:'hidden';
$isReadonly = Tools::isTrue($arParams['READONLY']);
$isRequired = Tools::isTrue($arParams['REQUIRED']);
$isChecked = Tools::isTrue($arParams['CHECKED']);
$isHint = (string)($arParams['HINT']) !== '';
$isDatepicker = Tools::isTrue($arParams['DATEPICKER']) || is_array($arParams['DATEPICKER']);

$label = $arParams['LABEL'];

if($isDatepicker) {
    app()->service()->assets()->setPlugin('kit:datepicker');
    //include ".con/datepicker.php";
    $arParams['ADD_CLASS'] .= ' datepicker-here';
}
if($isHint) {
    //include ".con/hint.php";
}

?>

<label class="kit-field kit-field--text<?=($hidden?' hidden':'')?> <?=$arParams['WRAP_CLASS']?>"<?
    if(strlen($arParams['ID'])){?> for="<?=$arParams['ID']?>"<?}?>>
    <textarea<?
        if($isReadonly){?> readonly<?}
        if($isRequired){?> required<?}
        if($isChecked){?> checked<?}

        if(strlen($arParams['ID'])){?> id="<?=$arParams['ID']?>"<?}
        ?>
        data-dirty="<?=($arParams['PLACEHOLDER'] || $arParams['VALUE'])?'true':'false'?>"
        type="<?=$arParams['TYPE']?>"
        name="<?=$arParams['NAME']?>"
        class="kit-field__input kit-autoheight <?=$arParams['CLASS']?> <?=$arParams['ADD_CLASS']?><?
        if(strlen($arParams['MESSAGE'])) {
            if(Tools::isSuccess($arParams['MESSAGE_TYPE'])){
                ?> kit-field--success<?
            } else {
                ?> kit-field--error<?
            }
        }
        ?>"
        rows="1"
        maxlength="<?=$arParams['MAXLENGTH']?>"
        placeholder="<?=TemplaterTools::safeHtmlAttr($arParams['PLACEHOLDER']).''?>"
        <?
        echo TemplaterTools::getAttrByArray($arParams['ATTR']);
        if($isDatepicker) {
            echo TemplaterTools::getAttrByArray([
                'data-position' => 'bottom left'
            ]);
        }
        ?>
        autocomplete="off"><?=($arParams['VALUE'])?></textarea><?
    if($isHint){
        app()->service()->templater()->render('kit:field/hint', ['TITLE' => $arParams['HINT']]);
    }
    ?>
    <div class="kit-field__label"><div class="kit-field__label-content kit-line-clamp"><?=$label;?></div><?
        if(in_array($arParams['TYPE'], ['radio', 'checkbox'])) {?>
        <span class="kit-field__icon">
            <svg viewBox="0 0 24 24">
                <?if(0) {?><path d="M9.001,16.212l-4.199-4.2l-1.4,1.4l5.6,5.6l12-12l-1.4-1.4L9.001,16.212z"></path><?}?>
                <path class="kit-field__check-mark" style="mix-blend-mode: normal;fill: none; stroke-width: 3px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 4; stroke-opacity: 1;" d="m 4.5010925,8.3988745 c 0,0 4.5231587,4.0536025 7.5719575,9.6668315 C 15.906315,10.552955 23.728894,0.8779021 31.09896,-3.72797"></path>
            </svg>

        </span>
        <?
        }
    ?></div>
    <div class="kit-field__color"></div>
    <div class="kit-field__message"><?=$arParams['MESSAGE']?></div>
    <div class="kit-field__hr"></div>
</label>
<?


