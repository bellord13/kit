<?php

use \Wt\Core\Templater\Tools as TemplaterTools;
use Wt\Core\Tools as Tools;

/**
 * @var array $arParams
 */
/**
 * type : button | reset | submit
 */

$preloader = Tools::isTrue($arParams['PRELOADER']);


?><button
    <?if(Tools::isTrue($arParams['DISABLED'])){?>disabled<?}?>
    aria-label="<?=$arParams['LABEL']?>"
    type="<?=$arParams['TYPE']?:'button'?>"
    <?if($arParams['ID']){?>id="<?=$arParams['ID']?>"<?}?>
    <?if(1){?>title="<?=TemplaterTools::safeHtmlAttr($arParams['LABEL']?:$arParams['TITLE']?:$arParams['NAME']?:'')?>"<?}?>
    <?if($arParams['NAME']){?>name="<?=$arParams['NAME']?>"<?}?>
    <?if($arParams['ENTITY']){?>data-entity="<?=$arParams['ENTITY']?>"<?}?>
    <?if($arParams['VALUE']){?>value="<?=$arParams['VALUE']?>"<?}?>
    class="kit-button <?=$arParams['CLASS']?:app()->config()->get('templaterParams.button.class', '')?> <?=$arParams['ADD_CLASS']?><?=(!$preloader?' not-preloader ':'')?>"
    onclick="<?=TemplaterTools::safeHtmlAttr($arParams['ONCLICK'])?>"
    <?=TemplaterTools::getAttrByArray($arParams['ATTR']);?>
><div class="kit-button__icon kit-icon"><?=$arParams['ICON']?></div>
    <div class="kit-button__label kit-line-clamp"><?=$arParams['LABEL']?:$arParams['TITLE']?:$arParams['NAME']?:''?></div>
    <div class="kit-button__border"></div>
<?if($preloader){?><span class="circle-preloader"></span><?}?>
</button><?
