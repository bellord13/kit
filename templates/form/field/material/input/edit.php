<?php
/**
 * @var Shell $this
 * @var DefaultField $field
 * @var array $arParams
 */

use Wt\Core\Form\Field\DefaultField;
use Wt\Core\Templater\Shell;
use Wt\Core\Templater\Tools as TemplaterTools;
use Wt\Core\Tools;

$bLabel = mb_strlen($field->getLabel());
$labelAttrs = [];
if(mb_strlen($field->getId())){
    $labelAttrs['for'] = $field->getId();
}


$isReadonly = $field->isReadonly();
$isDisabled = $field->isDisabled();
$isRequired = $field->isRequired();
$isChecked = false;

$isDatepicker = in_array($field->getType(), ['data', 'time', 'datetimeLocal']);

$label = $field->getLabel();

if($isDatepicker) {
    $arParams['ADD_CLASS'] .= ' datepicker-here';
}

?>

    <label class="kit-field kit-field--text<?=($field->isHidden()?' hidden':'')?> <?=$arParams['WRAP_CLASS']?>"<?php
    if(strlen($field->getId())){?> for="<?=$field->getId()?>"<?php
    }?>>
        <input<?php
        if($isReadonly){?> readonly<?php
        }
        if($isRequired){?> required<?php
        }
        if($isChecked){?> checked<?php
        }
        if($isDisabled){?> disabled<?php
        }

        if(strlen($field->getId())){?> id="<?=$field->getId()?>"<?php
        }
        ?>
                data-dirty="<?=$field->isDirty()?'true':'false'?>"
                type="<?=$field->getType()?>"
                name="<?=$field->getName()?>"
                class="kit-field__input <?=$arParams['CLASS']?> <?=$arParams['ADD_CLASS']?><?php
                if(strlen($arParams['MESSAGE'])) {
                    if(Tools::isSuccess($arParams['MESSAGE_TYPE'])){
                        ?> kit-field--success<?php
                    } else {
                        ?> kit-field--error<?php
                    }
                }
                ?>"
                value="<?=TemplaterTools::safeHtmlAttr($field->getValue())?>"
                maxlength="255"
                placeholder="<?=TemplaterTools::safeHtmlAttr($field->getPlaceholder()).''?>"
            <?php
            echo TemplaterTools::getAttrByArray($arParams['ATTR']);
            if($isDatepicker) {
                echo TemplaterTools::getAttrByArray([
                    'data-position' => 'bottom left'
                ]);
            }
            ?> autocomplete="<?=$field->isAutocomplete()?'on':'off'?>"><?php
        ?>
        <div class="kit-field__hint kit-icon" data-bs-toggle="tooltip" title="<?=TemplaterTools::safeHtmlAttr($field->getTitle())?>"
             <?php if($field->getTitle() == ''){?>style="display: none;"<?php
        }?>>
            <?php app()->service()->templater()->render('kit:svg/hint')?>
        </div>
        <div class="kit-field__label"><div class="kit-field__label-content kit-line-clamp"><?=$label;?></div><?php
            if(in_array($field->getType(), ['radio', 'checkbox'])) {?>
                <span class="kit-field__icon">
            <svg viewBox="0 0 24 24">
                <?php if(0) {?><path d="M9.001,16.212l-4.199-4.2l-1.4,1.4l5.6,5.6l12-12l-1.4-1.4L9.001,16.212z"></path><?php
                }?>
                <path class="kit-field__check-mark" style="mix-blend-mode: normal;fill: none; stroke-width: 3px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 4; stroke-opacity: 1;" d="m 4.5010925,8.3988745 c 0,0 4.5231587,4.0536025 7.5719575,9.6668315 C 15.906315,10.552955 23.728894,0.8779021 31.09896,-3.72797"></path>
            </svg>

        </span>
                <?php
            }
            ?></div>
        <div class="kit-field__color"></div>
        <div class="kit-field__message"><?=$arParams['MESSAGE']?></div>
        <div class="kit-field__description"><?=$field->getDescription()?></div>
        <div class="kit-field__hr"></div>
    </label>
<?php
