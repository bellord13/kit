<?php
/**
 * @var Shell $this
 * @var DefaultField $field
 */

use Wt\Core\Entity\UserFieldEntity;
use Wt\Core\Form\Field\DefaultField;
use Wt\Core\Templater\Shell;

require_once app()->module()->getDir() . '/templates/admin_bitrix_assets.php';

global $USER_FIELD_MANAGER;
/** @var UserFieldEntity $propertyEntity */
$propertyEntity = $field->getPropertyEntity();
$property_fields = $propertyEntity->getRaw();
$arUserField['EDIT_FORM_LABEL'] = $property_fields['EDIT_FORM_LABEL']?: $property_fields['FIELD_NAME'];
$property_fields['USER_TYPE'] = $propertyEntity->getAdapter()->getUserTypeBuild();
$property_fields['VALUE'] = $field->getValue();

if($field->isNew()){
    $property_fields['VALUE'] = $field->getPropertyEntity()->getDefaultValue();
}
$property_fields['ENTITY_VALUE_ID'] = $property_fields['VALUE_ID'] = $field->getParentEntityId();

//$arUserFields = $USER_FIELD_MANAGER->GetUserFields($field->getPropertyEntity()->getRaw('ENTITY_ID'), $property_fields['ENTITY_VALUE_ID'], LANGUAGE_ID);
//pre($arUserFields[$propertyEntity->getCode()]['VALUE']);

echo $USER_FIELD_MANAGER->GetEditFormHTML(false, $field->getValue(), $property_fields);
