<?php
/**
 * @var \Wt\Core\Templater\Shell $this
 * @var \Wt\Core\Form\Field\DefaultField $field
 */
use Wt\Core\Templater\Tools;

$bLabel = mb_strlen($field->getLabel());
$labelAttrs = [];
if(mb_strlen($field->getId())){
    $labelAttrs['for'] = $field->getId();
}
if($bLabel){?><label <?=Tools::getAttrByArray($labelAttrs)?>><?=$field->getLabel()?></label><?}
$inputAttributes = $field->getAttributes();
$inputAttributes['data-dirty'] = $field->isDirty()?'true':'false';
if($field->isMultiple()){
    $inputAttributes['name'] = preg_replace('/\[]$/', '', $inputAttributes['name']) . '[]';
}
unset($inputAttributes['options']);

?><select <?=Tools::getAttrByArray($inputAttributes)?>><?
    foreach ($field->getOptions() as $key => $value){
        ?><option value="<?=$key?>"<?if($field->isSelectValue($key)){?> selected<?}?>><?=$value?></option><?
    }
?></select><?
echo "\n\r";