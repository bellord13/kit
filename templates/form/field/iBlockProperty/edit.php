<?php
/**
 * @var Shell $this
 * @var DefaultField $field
 * @var PropertyEntity $propertyEntity
 */

use Wt\Core\Entity\IBlock\PropertyEntity;
use Wt\Core\Form\Field\DefaultField;
use Wt\Core\Templater\Shell;

require_once app()->module()->getDir() . '/templates/admin_bitrix_assets.php';

$propertyEntity = $field->getPropertyEntity();

?>
<tr>
    <td width="30%" class="adm-detail-content-cell-l"><?=$field->getLabel()?></td>
    <td class="adm-detail-content-cell-r"><?php

        $restoredValue = $field->getPropertyValue()->getRestoredValue();
        $extValues = [];
        if($field->isMultiple()){
            foreach ($restoredValue['VALUE'] as $k => $v){
                if($v){
                    $extValues[$k] = ['VALUE' => $v, 'DESCRIPTION' => (string)$restoredValue['DESCRIPTION'][$k]];
                }
            }
        } else {
            if($restoredValue['VALUE']){
                $extValues[] = ['VALUE' => $restoredValue['VALUE'], 'DESCRIPTION' => (string)$restoredValue['DESCRIPTION']];
            }
        }


        if($field->isMultiple() ){
            $shortValues = $field->getValue();
        } else {
            $shortValues = [];
            $value = $field->getValue();
            if(mb_strlen($value)){
                $shortValues = (array)$value;
            }
        }


        if($propertyEntity->isWithDescription()) {
            $values = $extValues;
        } else {
            $values = $shortValues;
        }

        $property_fields = $propertyEntity->getRaw();
        $property_fields['USER_TYPE_SETTINGS'] = $propertyEntity->getSettings();

        if($propertyEntity->getUserType()){
            $property_fields['VALUE'] = $shortValues;
            $property_fields['~VALUE'] = $extValues;
        }
        unset($property_fields['USER_TYPE_SETTINGS_LIST']);

        _ShowPropertyField(
            $field->getName(),
            $property_fields,
            $values,
            $field->isNew(),
            false,
            50000,
            $field->getForm()->getName(),
            false
        );
        ?></td>
</tr>
