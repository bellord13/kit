<?php
/**
 * @var \Wt\Core\Templater\Shell $this
 * @var \Wt\Core\Form\Field\DefaultField $field
 */
use Wt\Core\Templater\Tools;
$bLabel = mb_strlen($field->getLabel());
$labelAttrs = [];
if(mb_strlen($field->getId())){
    $labelAttrs['for'] = $field->getId();
}
if($bLabel && !$field->isHidden()){?><label <?=Tools::getAttrByArray($labelAttrs)?>><?=$field->getLabel()?></label><?}

if($field->isMultiple()){
    $values = $field->getValue();
    if(!is_array($values)){
        $values = [];
    }
    if(!count($values)){
        $values[] = '';
    }
    foreach ($values as $value){
        $inputAttributes = $field->getAttributes();
        $inputAttributes['name'] .= '[]';
        $inputAttributes['value'] = $value;
        ?><input <?=Tools::getAttrByArray($inputAttributes)?>><?
        echo "\n\r";
    }
} else {
    $inputAttributes = $field->getAttributes();
    $inputAttributes['data-dirty'] = $field->isDirty()?'true':'false';
    ?><input <?=Tools::getAttrByArray($inputAttributes)?>><?
    echo "\n\r";
}