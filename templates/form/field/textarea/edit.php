<?php
/**
 * @var \Wt\Core\Templater\Shell $this
 * @var \Wt\Core\Form\Field\DefaultField $field
 */
use Wt\Core\Templater\Tools;

$bLabel = mb_strlen($field->getLabel());
$labelAttrs = [];
if(mb_strlen($field->getId())){
    $labelAttrs['for'] = $field->getId();
}
if($bLabel){?><label <?=Tools::getAttrByArray($labelAttrs)?>><?=$field->getLabel()?></label>
<?}
$inputAttributes = $field->getAttributes();
$inputAttributes['data-dirty'] = $field->isDirty()?'true':'false';
unset($inputAttributes['multiple']);
if($field->isMultiple()){

}
?><textarea <?=Tools::getAttrByArray($inputAttributes)?>><?=$field->getValue()?></textarea><?
echo "\n\r";