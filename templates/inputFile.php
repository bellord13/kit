<?php
/**
 * @var array $arParams
 */

use Wt\Core\Tools;

$arParams['TYPE'] = 'file';
$isReadonly = Tools::isTrue($arParams['READONLY']);
$isRequired = Tools::isTrue($arParams['REQUIRED']);
$isChecked = Tools::isTrue($arParams['CHECKED']);
$multiple = Tools::isTrue($arParams['MULTIPLE']);
$moduleId = $arParams['MODULE_ID']?:'main';

$MAX_FILE_SIZE = $arParams['MAX_FILE_SIZE']?:1048576;

if(is_array($arParams['VALUE'])) {
    $arParams['VALUE'] = array_filter($arParams['VALUE']);
}
if($multiple) {
    $value = (array)$arParams['VALUE'];
} else {
    if(is_array($arParams['VALUE'])) {
        $value = (int)$arParams['VALUE'][array_key_first($arParams['VALUE'])];
    }
    $value = (int)$arParams['VALUE'];
}
$isHint = (string)($arParams['HINT']) !== '';

#INPUT_NAME // уникальный name инпута, без него выдает ошибку
#MULTIPLE => Y || N - позволяет или не позволяет множественную загрузку
#MODULE_ID => main || iblock || blog || forum и пр. имя модуля, к которому файл привязан будет и в какую папку попадет относительно upload.
#MAX_FILE_SIZE // максимальный размер файла (вроде в байтах)
#ALLOW_UPLOAD A || F || I - какой тип файлов будем грузить: F - файлы, I - картинки, A - все подряд.
#ALLOW_UPLOAD_EXT => «*.zip,*.rar,*.doc и пр.» // какие расширения файлов можно грузить. Работает если ALLOW_UPLOAD => F

$componentParams = array(
    'INPUT_NAME' => $arParams['NAME'],
    //'INPUT_NAME_UNSAVED' => 'FILE_NEW_TMP',
    'INPUT_VALUE' => $value,
    'MAX_FILE_SIZE' => $MAX_FILE_SIZE,
    'MULTIPLE' => $multiple?'Y':'N',
    'MODULE_ID' => $moduleId,
    'ALLOW_UPLOAD' => $arParams['ALLOW_UPLOAD']?:'A',
    'ALLOW_UPLOAD_EXT' => $arParams['ALLOW_UPLOAD_EXT'],
    'HINT' => $arParams['HINT'],
    'CLASS' => $arParams['CLASS'],
    'ADD_CLASS' => $arParams['ADD_CLASS'],
    'WRAP_CLASS' => $arParams['WRAP_CLASS'],
);

$GLOBALS["APPLICATION"]->IncludeComponent('bitrix:main.file.input', 'kit', $componentParams, false, Array("HIDE_ICONS" => "Y"));