<?

use \Wt\Core\Media\Webp;
use \Wt\Core\Templater\Tools as TemplaterTools;

?>
<picture class="kit-picture <?=$arParams['CLASS']?>">
    <?
    foreach ((array)$arParams['SRCSET_LIST'] as $srcset) {
        $webp = new Webp($_SERVER['DOCUMENT_ROOT'] . $srcset['src']);
        if($webp->make()->isSuccess()) {
            ?><source srcset="<?=TemplaterTools::safeUrl($webp->getPathOut(true))?>" type="image/webp"<?if($srcset['media']){?> media="<?=TemplaterTools::safeHtmlAttr($srcset['media'])?>"<?}?>><?
        }
        ?>
        <source srcset="<?=TemplaterTools::safeUrl($srcset['src'])?>" type="<?=$webp->mime?>"<?if($srcset['media']){?> media="<?=TemplaterTools::safeHtmlAttr($srcset['media'])?>"<?}?>>
        <?
    }

    $webp = new Webp($_SERVER['DOCUMENT_ROOT'] . $arParams['SRC']);
    if($webp->make()->isSuccess()) {
        ?><source srcset="<?=TemplaterTools::safeUrl($webp->getPathOut(true))?>" type="image/webp"><?
    }
    ?>
    <img class="kit-img"<?if($arParams['SRC']) {?> src="<?=TemplaterTools::safeUrl($arParams['SRC'])?>"<?}?> alt="<?=TemplaterTools::safeHtmlAttr($arParams['ALT'])?>"<?
        if($arParams['TITLE']) {
            ?> title="<?=TemplaterTools::safeHtmlAttr($arParams['TITLE'])?>"<?
        }
        echo TemplaterTools::getAttrByArray($arParams['ATTR']);
        ?>/>
</picture>