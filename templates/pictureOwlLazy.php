<?

use \Wt\Core\Media\Webp;
use \Wt\Core\Templater\Tools as TemplaterTools;

?>
<picture class="kit-picture <?=$arParams['CLASS']?>">
    <?
    $webp = new Webp($_SERVER['DOCUMENT_ROOT'] . $arParams['SRC']);
    if($webp->make()->isSuccess()) {
        ?><source class="owl-lazy" data-srcset="<?=$webp->getPathOut(true)?>" type="image/webp"><?
    }
    ?>
    <img class="kit-img owl-lazy" data-src="<?=$arParams['SRC']?>" alt="<?=TemplaterTools::safeHtmlAttr($arParams['ALT'])?>"<?
        if($arParams['TITLE']) {
            ?> title="<?=TemplaterTools::safeHtmlAttr($arParams['TITLE'])?>"<?
        }
        echo TemplaterTools::getAttrByArray($arParams['ATTR']);
        ?>/>
</picture>