<?php


?>

<div class="flex-card aspect-ratio">
    <div class="flex-card__section">
        <div class="flex-card__content">
            <div class="flex-card__body-shell aspect-ratio">
                <div class="flex-card__body">
                    <div class="flex-card__picture kit-picture"><?=$arParams['picture']?></div>
                    <div class="flex-card__title">
                        <div class="kit-line-clamp">
                            <?=$arParams['name']?>
                        </div>
                    </div>
                    <div class="flex-card__props"><?=$arParams['props']?></div>
                </div>
            </div>
            <div class="flex-card__header"><?=$arParams['header']?></div>
            <div class="flex-card__footer"><?=$arParams['footer']?></div>
            <div class="flex-card__hover"><?=$arParams['hover']?></div>
            <div class="flex-card__ticket">
                <div class="flex-list flex-list--ticket"><?
                    foreach ($item['tickets'] as $ticket) {
                        ?>
                        <div class="flex-list-item">
                            <div class="ticket-item <?=$ticket['code']?>">
                                <div class="kit-line-clamp"><?=$ticket['name']?></div>
                            </div>
                        </div>
                        <?
                    }
                    ?></div>
            </div>
        </div>
    </div>
</div>
