<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="24" height="24"  viewBox="0 0 18 18" xml:space="preserve">
    <path style="fill:#E1251B;" d="M9,18L9,18c-5,0-9-4-9-9v0c0-5,4-9,9-9h0c5,0,9,4,9,9v0C18,14,14,18,9,18z"></path>
    <path style="fill:#FFFFFF;" d="M9,4c1.8,0,3.7,3.2,3.7,6c0,2.2-0.8,4-3.7,4c-3,0-3.7-1.9-3.7-4C5.3,7.2,7.2,4,9,4"></path>
</svg>