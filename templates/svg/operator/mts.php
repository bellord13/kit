<svg width="24" height="24" version="1.1" viewBox="0 0 18 18" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
    <rect x="0" y="0" width="18" height="18" ry="15%" style="fill-rule:evenodd;fill:#e1251b;paint-order:markers fill stroke;stroke-linejoin:round;"></rect>
    <path d="m9 3.3c2.1 0 4.3 3.7 4.3 6.9 0 2.5-0.92 4.6-4.3 4.6-3.4 0-4.3-2.2-4.3-4.6 0-3.2 2.2-6.9 4.3-6.9" style="fill:#ffffff;"></path>
</svg>
