<svg width="24" height="24" version="1.1" viewBox="0 0 18 18" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
	<path style="fill:#0E0E0E;" d="m12 5.3 0.7 0.3s1.1-0.5 1.8-1.1c0.7-0.7-0.2-1-0.2-1z"></path>
    <polygon style="fill:#8F0000;" points="1.6 16.8 6.6 2.2 5.5 2.9 0.7 16.3"></polygon>
    <path style="fill:#3A3A3A;" class="kit-svg-a1__1" d="m12 3.2v2.1s1.1-0.5 2-1.3v6.5l2.3 0.2v-10.2l-1.8 0.3c0-0.2-0.4 1.4-2.5 2.4z"></path>
    <polygon style="fill:#0E0E0E;" class="kit-svg-a1__1-shadow" points="16.3 10.7 16.3 0.5 16.8 0.9 16.8 10.6"></polygon>
    <polygon style="fill:#8F0000;" points="8.3 6.3 7.6 8 9.6 15.5 10.6 15.8"></polygon>
    <path style="fill:#E70707;" d="m13.5 15.6-3.8-12.7-3.1-0.7-5 14.6 3.4-0.3 0.8-2.9 4.1-0.2 0.7 2.5zm-6.7-4.5 1.5-4.7 1.1 4.7z"></path>
</svg>