<svg width="45" height="29" version="1.1" viewBox="0 0 33.75 21.75" xmlns="http://www.w3.org/2000/svg">
    <g transform="matrix(.27295 0 0 .27295 -3.0115 -3.0122)">
        <rect x="57.652" y="22.854" width="31.5" height="56.606" style="fill:#ff5f00"></rect>
        <path transform="translate(-322.6 -245.79)" d="m382.25 296.95a35.938 35.938 0 0 1 13.75-28.303 36 36 0 1 0 0 56.606 35.938 35.938 0 0 1-13.75-28.303z" style="fill:#eb001b"></path>
        <path transform="translate(-322.6 -245.79)" d="m454.25 296.95a35.999 35.999 0 0 1-58.245 28.303 36.005 36.005 0 0 0 0-56.606 35.999 35.999 0 0 1 58.245 28.303z" style="fill:#f79e1b"></path>
    </g>
</svg>
