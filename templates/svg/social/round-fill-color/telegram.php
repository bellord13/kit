<svg width="24" height="24" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
    <rect width="512" height="512" rx="50%" ry="50%" fill="#37aee2"></rect>
    <path d="m209 375c-8.6 0-7.8-3.1-10-11l-25-82 191-112" fill="#c8daea" style="stroke-width:.78"></path>
    <path d="m209 375c5.5 0 8.6-3.1 12-6.2l35-34-44-27" fill="#a9c9dd" style="stroke-width:.78"></path>
    <path d="m213 308 105 77c11 7 20 3.1 23-11l43-202c3.9-17-7-25-19-20l-251 97c-16 6.2-16 16-3.1 20l65 20 148-95c7-3.9 13-2.3 8.6 3.1" fill="#f6fbfe"></path>
</svg>
