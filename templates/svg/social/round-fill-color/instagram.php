<?
use Wt\Core\Type\Str;
$prefixId = Str::random(5);
?>
<svg width="24" height="24" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <radialGradient id="instagram-round-radial-gradient-<?=$prefixId?>" cx="-579" cy="-838" r="197" gradientTransform="matrix(.28 0 0 .28 163 212)" gradientUnits="userSpaceOnUse">
            <stop stop-color="#f9ed32" offset="0"></stop>
            <stop stop-color="#ee2a7b" offset=".36"></stop>
            <stop stop-color="#d22a8a" offset=".44"></stop>
            <stop stop-color="#8b2ab2" offset=".6"></stop>
            <stop stop-color="#1b2af0" offset=".83"></stop>
            <stop stop-color="#002aff" offset=".88"></stop>
        </radialGradient>
    </defs>
    <rect transform="scale(-1)" x="-24" y="-24" width="24" height="24" rx="12" ry="12" style="fill:url(#instagram-round-radial-gradient-<?=$prefixId?>)"></rect>
    <path d="m15.6 19.2h-7.3c-1.99 0-3.6-1.61-3.6-3.6v-7.3c0-1.99 1.61-3.6 3.6-3.6h7.3c1.99 0 3.6 1.61 3.6 3.6v7.4c0 1.99-1.61 3.5-3.6 3.5zm-7.3-13.1c-1.22 0-2.08 1.09-2.1 2.3l-0.1 7.3c-0.0167 1.22 0.985 2.1 2.2 2.1h7.3c1.22 0 2.2-0.885 2.2-2.1v-7.4c0-1.22-0.985-2.2-2.2-2.2z" style="fill:#ffffff"></path>
    <path d="m12 15.8a3.85 3.85 0 1 1 3.85-3.85 3.85 3.85 0 0 1-3.85 3.85zm0-6.38a2.53 2.53 0 1 0 2.53 2.53 2.53 2.53 0 0 0-2.53-2.53z" style="fill:#ffffff;stroke-width:.938"></path>
    <circle cx="15.8" cy="8.15" r=".73" style="fill:#ffffff"></circle>
</svg>
