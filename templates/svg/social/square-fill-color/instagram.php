<?
use Wt\Core\Type\Str;
$prefixId = Str::random(5);
?>
<svg width="24" height="24" version="1.1" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <radialGradient id="instagram-radial-gradient-<?=$prefixId?>" cx="-578.95" cy="-837.6" r="197.06" gradientTransform="matrix(.28125 0 0 .28125 163.31 212.06)" gradientUnits="userSpaceOnUse">
            <stop stop-color="#f9ed32" offset="0"></stop>
            <stop stop-color="#ee2a7b" offset=".36"></stop>
            <stop stop-color="#d22a8a" offset=".44"></stop>
            <stop stop-color="#8b2ab2" offset=".6"></stop>
            <stop stop-color="#1b2af0" offset=".83"></stop>
            <stop stop-color="#002aff" offset=".88"></stop>
        </radialGradient>
    </defs>
    <rect  transform="scale(-1)" x="-24" y="-24" width="24" height="24" rx="4.2" ry="4.2" style="fill:url(#instagram-radial-gradient-<?=$prefixId?>)"></rect>
    <path style="fill:#fff;" d="m16.5 21h-9a4.5 4.5 0 0 1-4.5-4.5v-9a4.5 4.5 0 0 1 4.5-4.5h9a4.5 4.5 0 0 1 4.5 4.5v9a4.5 4.5 0 0 1-4.5 4.5zm-9-16.2a2.7038 2.7038 0 0 0-2.7 2.7v9a2.7038 2.7038 0 0 0 2.7 2.7h9a2.7038 2.7038 0 0 0 2.7-2.7v-9a2.7038 2.7038 0 0 0-2.7-2.7z"></path>
    <path style="fill:#fff;" d="m12 17.1a5.1 5.1 0 1 1 5.1-5.1 5.1038 5.1038 0 0 1-5.1 5.1zm0-8.4a3.3 3.3 0 1 0 3.3 3.3 3.3038 3.3038 0 0 0-3.3-3.3z"></path>
    <circle style="fill:#fff;" cx="17.1" cy="7.2" r=".9"></circle>
</svg>
