<svg class="menu-hamburger" viewBox="0 0 24 24">
    <g transform="translate(12,12)">
        <path class="menu-hamburger__part1" d="M-10 -6 L10 -6" stroke="currentColor" stroke-width="2" fill="currentColor"></path>
        <path class="menu-hamburger__part2" d="M-10 0 L10 0" stroke="currentColor" stroke-width="2" fill="currentColor"></path>
        <path class="menu-hamburger__part3" d="M-10 6 L10 6" stroke="currentColor" stroke-width="2" fill="currentColor"></path>
    </g>
</svg>