<svg width="24" height="24" version="1.1" viewBox="0 0 960 960" xmlns="http://www.w3.org/2000/svg">
    <path d="m960 0v728h-192v232l-78-59.7-227-172h-463v-728zm-96 91h-768v546h401l13.5 9.96 162 122v-132h192z"></path>
</svg>
