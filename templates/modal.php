<?php

use \Wt\Core\Templater\Tools as TemplaterTools;

?>
<div class="modal fade <?=$arParams['WRAP_CLASS']?> <?=$arParams['CLASS']?>" id="<?=$arParams['ID']?>" tabindex="-1" aria-labelledby="<?=$arParams['ID']?>" aria-hidden="true" <?=TemplaterTools::getAttrByArray($arParams['ATTR'])?>>
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title"><?=$arParams['TITLE']?></div>
                <button type="button" class="btn-close kit-icon" data-bs-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>
                </button>
                <?=$arParams['HEADER']?>
            </div>
            <div class="modal-body"><?=$arParams['BODY']?></div>
            <div class="modal-footer"><?=$arParams['FOOTER']?></div>
        </div>
    </div>
</div>