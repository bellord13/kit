<?php

use Wt\Core\Entity\IblockElementPropertyEntity;
use Wt\Core\Templater\Shell;

/**
 * @var $this Shell
 * @var $elementPropertyEntity IblockElementPropertyEntity
 */

$elementPropertyEntity = $this->getContext();
if(!$elementPropertyEntity->isMultiple()){
    echo $elementPropertyEntity->getDisplayValue();
} else {
    echo implode(', ', $elementPropertyEntity->getDisplayValue());
}