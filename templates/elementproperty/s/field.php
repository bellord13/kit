<?php

use Wt\Core\Entity\IblockElementPropertyEntity;
use Wt\Core\Templater\Shell;

/**
 * @var $this Shell
 * @var $elementPropertyEntity IblockElementPropertyEntity
 */

$elementPropertyEntity = $this->getContext();
$blockId = $elementPropertyEntity->getFactory()->getBlockId();
$code = $elementPropertyEntity->getCode();
$fieldId = 'iblock_' . $blockId . '_prop_' . $code;
/**
 * @var array $arParams
 */

use Wt\Core\Templater\Tools as TemplaterTools;
use Wt\Core\Tools;


$arParams['TYPE'] = $arParams['TYPE'] ?: 'hidden';
$isReadonly = Tools::isTrue($arParams['READONLY']);
$isRequired = $elementPropertyEntity->isRequired();
$hidden = Tools::isTrue($arParams['HIDDEN']);
$isHint = $elementPropertyEntity->getHint() !== '';
$dirty = $arParams['PLACEHOLDER'] || mb_strlen($code);
$name = "FORM[iblock][$blockId]{$elementPropertyEntity->getCode()}]";


?>

    <div class="kit-field kit-field--text<?= ($hidden ? ' hidden' : '') ?> <?= $arParams['WRAP_CLASS'] ?>">
        <input<?
        if ($isReadonly) {?> readonly<?}
        if ($isRequired) {?> required<?}
        ?> id="<?= $fieldId ?>"<?
        ?>
            data-dirty="<?= $dirty ? 'true' : 'false' ?>"
            type="text"
            name="<?=$name?>"
            class="kit-field__input <?= $arParams['CLASS'] ?> <?= $arParams['ADD_CLASS'] ?><?
            if (strlen($arParams['MESSAGE'])) {
                if (Tools::isSuccess($arParams['MESSAGE_TYPE'])) {
                    ?> kit-field--success<?
                } else {
                    ?> kit-field--error<?
                }
            }
            ?>"
            value="<?= TemplaterTools::safeHtmlAttr($elementPropertyEntity->getValue()) ?>"
            maxlength="255"
            placeholder="<?= TemplaterTools::safeHtmlAttr($arParams['PLACEHOLDER']) . '' ?>"
            <?
            echo TemplaterTools::getAttrByArray($arParams['ATTR']);

            ?>
            autocomplete="off"><?
        if ($isHint) {
            app()->service()->templater()->render('kit:field/hint', ['TITLE' => $elementPropertyEntity->getHint()]);
        }
        ?>
        <label class="kit-field__label" for="<?=$fieldId?>">
            <div class="kit-field__label-content kit-line-clamp"><?= $elementPropertyEntity->getName() ?></div><?
            ?></label>
        <div class="kit-field__message"><?= $arParams['MESSAGE'] ?></div>
        <div class="kit-field__hr"></div>
    </div>
<?


