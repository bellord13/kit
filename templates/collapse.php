<?php


use Wt\Core\Tools;
use Wt\Core\Type\Str;


app()->service()->assets()->setPlugin('collapse');
$arParams['ID'] = $arParams['ID']?: ('collapse_' . Str::random(6));
 ?>
<div class="accordion" id="<?=$arParams['ID']?>">
    <?foreach ($arParams['ITEMS'] as $key => $data) {
        $data['ID'] = $data['ID']?:$arParams['ID'].'_'.$key;
        $data['SHOW'] = Tools::isTrue($data['SHOW']);
        ?>
        <div class="accordion-item">
            <h4 class="accordion-header" id="<?=$data['ID'].'_header'?>">
                <button class="accordion-button<?if(!$data['SHOW']){?> collapsed<?}?>" type="button" data-bs-toggle="collapse" data-bs-target="#<?=$data['ID'].'_body'?>" aria-expanded="true" aria-controls="<?=$data['ID'].'_body'?>">
                    <?=$data['HEADER']?>
                </button>
            </h4>
            <div id="<?=$data['ID'].'_body'?>" class="accordion-collapse collapse<?if($data['SHOW']){?> show<?}?>" aria-labelledby="<?=$data['ID'].'_header'?>" data-bs-parent="#<?=$arParams['ID']?>">
                <div class="accordion-body">
                    <?=$data['BODY']?>
                </div>
            </div>
        </div>
    <?}?>
</div>

