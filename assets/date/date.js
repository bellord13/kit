/**
 * date-php.js v1.7.15
 *   :-) date('Y-m-d', 1563148800000)
 *   This is a Javascript mimicking PHP datetime formatting function. It is very similar to PHP, has rich template
 *   characters, and enhances some template characters on the basis of the original.
 *
 *     -- repository https://github.com/toviLau/date-php.git
 *
 *   (c) 2019-2020 ToviLau. Released under the MIT License.
 *
 * @module date
 **/
__PluginManager__.register('date',function(require, exports, module, global) {

    (function (global, factory) {
        typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
            typeof define === 'function' && define.amd ? define(factory) :
                (global = global || self, global.date = factory());
    }(this, function () { 'use strict';

        /**
         * 补前导零(0)
         * @param {number} str 字符
         * @param {number} len 长度
         * @param {string} placeholder 前导占位符
         * @returns {string}
         */
        function pad(str, len, placeholder) {
            if ( placeholder === void 0 ) placeholder = '0';
            str = '' + str;
            if (str.length < len) {
                return new Array(++len - str.length).join(placeholder) + str;
            } else {
                return str;
            }
        }var longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var txt_ordin = {
            1: 'st',
            2: 'nd',
            3: 'rd',
            21: 'st',
            22: 'nd',
            23: 'rd',
            31: 'st',
        };
        var txt_months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var baseFigure = {
            1: '一',
            2: '二',
            3: '三',
            4: '四',
            5: '五',
            6: '六',
        };
        var lunarTime = ['子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥'];
        var ordinal = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth'];

        var lunarKe = Object.assign.apply(
            Object, [ {},
                {
                    0: '零',
                    7: '七',
                } ].concat( baseFigure )
        );

        var weekDay = Object.assign.apply(
            Object, [ {},
                {
                    0: '日',
                } ].concat( baseFigure )
        );

        var dateFigure = Object.assign.apply(
            Object, [ {},
                {
                    0: '〇', 7: '七', 8: '八', 9: '九', 10: '十',
                    20: '廿', 30: '卅',
                } ].concat( baseFigure )
        );
        var lMonth = Object.assign.apply(
            Object, [ {},
                {
                    7: '七', 8: '八', 9: '九', 10: '十', 11: '冬', 12: '腊',
                } ].concat( baseFigure )
        );

        // 取中文日期(廿七)
        var textReplace = function (res) { return res.toString()
            .split('')
            .reverse()
            .map(function (val, key) {
                var v = Math.pow(10, key) * val;
                return v ? dateFigure[v] : null;
            })
            .reverse()
            .join(''); };

        // 取中文日期2(例：一九八七)
        var textReplace2 = function (succ) { return (succ + '').split('').map(function (res) { return dateFigure[res]; }).join(''); };

        function defP(obj, key, val) {
            Object.defineProperty(obj, key, {
                get: function () { return val; },
            });
        }

        /**
         *
         * 计算持续时长
         * @param  {String} fmt 模板字符串
         * @param {Number} timestamp 时间戳
         * @param {Boolean} ms 是否是 含有毫秒
         * @return 相印时间
         */
        function duration(fmt, timestamp, ms) {
            if ( fmt === void 0 ) fmt = 'D天h:i:s';
            if ( timestamp === void 0 ) timestamp = 0;
            if ( ms === void 0 ) ms = true;

            var conversion = {
                y: 12,
                m: 30.4375,
                d: 24,
                h: 60,
                i: 60,
                s: 1000,
                v: 1000,
            };
            var tChars = {
                y: function () { return tChars.Y(); }, // 当前剩余年数,
                Y: function () { return Math.floor(tChars.M() / conversion.y); }, // 总剩余年数,

                m: function () { return pad(tChars.n(), 2); }, // 当前剩余月数(有前导零)
                n: function () { return tChars.M() % conversion.y; }, // 当前剩余月数(无前导零)
                M: function () { return Math.floor(tChars.D() / conversion.m); }, // 总剩余月数

                d: function () { return pad(tChars.j(), 2); }, // 当前剩余天数(有前导零)
                j: function () { return Math.floor(tChars.D() % conversion.m); }, // 当前剩余天数(无前导零)。
                D: function () { return Math.floor(tChars.H() / conversion.d); }, // 总剩余天数

                h: function () { return pad(tChars.g(), 2); }, // 当前小时剩余数(有前导零)
                g: function () { return Math.floor(tChars.H() % conversion.d); }, // 当前小时剩余数(无前导零)
                H: function () { return Math.floor(tChars.I() / conversion.h); }, // 总剩余小时数

                i: function () { return pad(Math.floor(tChars.I() % conversion.h),2); }, // 当前分钟剩余点数
                I: function () { return Math.floor(tChars.S() / conversion.i); }, // 总剩余分钟数

                s: function () { return pad(Math.floor(tChars.S() % conversion.i), 2); }, // 当前秒钟剩余点数
                S: function () { return Math.floor(tChars.V() / conversion.s); }, // 总剩余秒数

                v: function () { return pad(Math.floor(tChars.V() % conversion.s), 3); }, // 当前毫秒剩余数
                V: function () { return ms ? new Date(timestamp)-0 : new Date(timestamp) * conversion.v; }, // 总剩余毫秒数
            };

            return fmt.replace(/(\\?([a-z]))/ig, function (res, key) {
                var result = '';
                if (res !== key) {
                    result = key;
                } else {
                    if (tChars[key]) {
                        result = tChars[key]();
                    } else {
                        result = key.replace('\\', '');
                    }
                }
                return result;
            });
        }

        /**
         * 计算持续时间
         * @param fmt
         * @param timestamp1
         * @param timestamp2
         * @param ms
         * @return 相印时间
         */
        function countTime(fmt, timestamp1, timestamp2, ms) {
            if ( fmt === void 0 ) fmt = 'D天h:i:s';
            if ( timestamp1 === void 0 ) timestamp1 = 0;
            if ( timestamp2 === void 0 ) timestamp2 = 0;
            if ( ms === void 0 ) ms = true;

            var count = new Date(timestamp1) - new Date(timestamp2) || 0;
            return duration(fmt, Math.abs(count), ms);
        }

        var isDate = function (d) {
            return new Date(d).toString() !== 'Invalid Date';
        };

        /**
         * @memberOf kit
         * @param fmt
         * @param now
         * @param ms
         * @return {string}
         */
        var date = function (fmt, now, ms) {
            if ( fmt === void 0 ) fmt = 'Y-m-d';
            if ( now === void 0 ) now = new Date;
            if ( ms === void 0 ) ms = true;

            now = isDate(this)
                ? this
                : isDate(now)
                    ? new Date(now)
                    : new Date;
            if (ms === false) { now = new Date(now * 1000); }

            if (!isDate(now)) { throw Error((function (D) {
                return '' +
                    '参数2不正确，须传入 “日期时间对象”，或 “Unix时间戳” 或 “时间戳字符串”。\n可以参考以下值：\n' +
                    "  \"" + D + "\"\n" +
                    "  \"" + (D.toUTCString()) + "\"\n" +
                    "  " + (D.getTime()) + "  -- 推荐\n";
            })(new Date())); }


            // 模板字串替换函数
            var tChars = {
                // 日
                d: function () { return pad(tChars.j(), 2); },
                k: function () { return textReplace(tChars.j()); }, // 中文日(1.3.2+), PHP中无此功能
                D: function () { return tChars.l().substr(0, 3); },
                j: function () { return now.getDate(); },
                lt: function () { return lunarTime[Math.floor((tChars.G() >= 23 ? 0 : tChars.G() + 1) / 2)]; },
                lg: function () { return tChars.G() > 18 || tChars.G() < 5 ? Math.ceil((tChars.G() < 19 ? tChars.G() + 24 : tChars.G()) / 2) - 9 : ''; },
                lG: function () { return ("" + (tChars.lg() ? baseFigure[tChars.lg()] + '更' : '')); },
                lk: function () { return lunarKe[Math.floor(((tChars.U() + 60 * 60) % (60 * 60 * 2)) / 60 / 15)]; },
                fh: function () { return (getFestival(tChars.Y() + tChars.m() + tChars.d()).cn || []).join(); },
                lh: function () { return (getFestival(tChars.Y() + tChars.m() + tChars.d()).en || []).join(); },
                l: function () { return longDays[tChars.w()]; },
                N: function () { return tChars.w() === 0 ? 7 : tChars.w(); },
                S: function () { return txt_ordin[tChars.j()] ? txt_ordin[tChars.j()] : 'th'; },
                w: function () { return now.getDay(); },
                K: function () { return weekDay[tChars.w()]; }, // 中文周(1.3.2+)
                z: function () { return Math.ceil((now - new Date(tChars.Y() + '/1/1')) / (60 * 60 * 24 * 1e3)); },

                // 周
                W: function () {
                    var inYearDay = tChars.z(); // 当前年份中的第n天
                    var yDay = new Date(tChars.Y() + '1/1').getDay(); // 第一天周几
                    var diffDay = (yDay > 0) - 0;
                    return Math.ceil((inYearDay - yDay) / 7) + diffDay;
                },

                // 月
                F: function () { return txt_months[tChars.n()]; },
                f: function () { return textReplace(tChars.n()); }, // 中文月(1.3.2+), PHP中无此功能
                m: function () { return pad(tChars.n(), 2); },
                M: function () { return tChars.F().substr(0, 3); },
                n: function () { return now.getMonth() + 1; },
                t: function () {
                    var year = tChars.Y();
                    var nextMonth = tChars.n();
                    if (nextMonth === 12) {
                        year += 1;
                        nextMonth = 0;
                    }
                    return new Date(year, nextMonth, 0).getDate();
                },
                lq: function () { return Math.ceil((tChars.n() - 0) / 3); }, // 季度数字
                lQ: function () { return baseFigure[tChars.lq()]; }, // 季度汉字(1.6.0+)
                q: function () { return txt_ordin[tChars.lq()] ? tChars.lq() + '' + txt_ordin[tChars.lq()] : tChars.lq() + 'th'; }, // 季度英文缩写
                Q: function () { return ordinal[tChars.lq() - 1]; }, // 李度英文(1.6.0+)

                // 年
                L: function () { return Number(tChars.Y() % 400 === 0 || (tChars.Y() % 100 !== 0 && tChars.Y() % 4 === 0)); },
                o: function () {
                    var yearWeek = new Date(tChars.Y(), 0, 1).getDay();
                    var diffTime = 60 * 60 * 24 * 1000 * (7 - yearWeek);
                    var timestramp = yearWeek > 3 ? now.getTime() - diffTime : now.getTime();
                    return new Date(timestramp).getFullYear();

                },
                Y: function () { return now.getFullYear(); },
                y: function () { return (tChars.Y() + '').slice(2); },
                C: function () { return textReplace2(tChars.Y()); }, // 中文年(1.3.2+), PHP中无此功能

                // 时间
                a: function () { return tChars.G() > 11 ? 'pm' : 'am'; },
                A: function () { return tChars.a().toUpperCase(); },
                B: function () {
                    var off = (now.getTimezoneOffset() + 60) * 60;
                    var theSeconds = (tChars.G() * 3600) + (now.getMinutes() * 60) + now.getSeconds() + off;
                    var beat = Math.floor(theSeconds / 86.4);
                    // beat > 1000 ? beat -= 1000 : beat += 1000
                    if (beat > 1000) { beat -= 1000; }
                    if (beat < 0) { beat += 1000; }

                    return pad(beat, 3);
                },
                g: function () { return tChars.G() % 12 || 12; },
                G: function () { return now.getHours(); },
                h: function () { return pad(tChars.g(), 2); },
                H: function () { return pad(tChars.G(), 2); },
                i: function () { return pad(now.getMinutes(), 2); },
                s: function () { return pad(now.getSeconds(), 2); },
                u: function () { return tChars.v() + pad(Math.floor(Math.random() * 1000), 3); },
                v: function () { return (now.getTime() + '').substr(-3); },

                // 时区
                e: function () { return Intl.DateTimeFormat().resolvedOptions().timeZone; },
                I: function () {
                    var DST = null;
                    for (var i = 0; i < 12; ++i) {
                        var d = new Date(tChars.Y(), i, 1);
                        var offset = d.getTimezoneOffset();

                        if (DST === null) { DST = offset; }
                        else if (offset < DST) {
                            DST = offset;
                            break;
                        } else if (offset > DST) { break; }
                    }
                    return (now.getTimezoneOffset() === DST) | 0;
                },
                O: function () { return (now.getTimezoneOffset() > 0 ? '-' : '+') + pad(Math.abs(now.getTimezoneOffset() / 60 * 100), 4); },
                P: function () { return tChars.O().match(/[+-]?\d{2}/g).join(':'); },
                T: function () {
                    var tz = now.toLocaleTimeString(navigator.language, { timeZoneName: 'short' }).split(/\s/);
                    return tz[tz.length - 1];
                },
                Z: function () { return -(now.getTimezoneOffset() * 60); },

                // 完整日期时间
                c: function () { return tChars.Y() + '-' + tChars.m() + '-' + tChars.d() + 'T' + tChars.h() + ':' + tChars.i() + ':' + tChars.s() + tChars.P(); },
                r: function () { return now.toString(); },
                U: function () { return Math.round(now.getTime() / 1000); },
            };

            if (fmt === 'json' || fmt === 'all' || fmt === -1 || fmt === '-1') {
                var json = {};
                Object.keys(tChars).forEach(function (res, idx) { return json[res] = tChars[res](); });
                return json;
            }
            return fmt.replace(/\\?(([lf][a-z])|([a-z]))/ig, function (res, key) {
                var result = '';
                if (res !== key) {
                    result = key;
                } else {
                    if (tChars[key]) {
                        result = tChars[key]();
                    } else {
                        result = key.replace('\\', '');
                    }
                }
                return result;
            });
        };

        defP(Date.prototype, 'format', date);

        defP(date, 'version', '1.7.15');
        defP(date, 'description', function () { return (console.info('%cdate-php使用说明:\n' +
            '已经废弃，查看使用说明请移步这里\nhttps://github.com/toviLau/date-php/blob/master/README.md'
            , 'color:#c63'
        )); });
        date.duration = duration;

        date.countTime = countTime;

        date.DATA_ATOM = "Y-m-d\TH:i:sP";
        date.DATA_COOKIE = "l, d-M-Y H:i:s T";
        date.DATA_ISO8601 = "Y-m-d\TH:i:sO";
        date.DATA_RFC822 = "D, d M y H:i:s O";
        date.DATA_RFC850 = "l, d-M-y H:i:s T";
        date.DATA_RFC1036 = "D, d M y H:i:s O";
        date.DATA_RFC1123 = "D, d M Y H:i:s O";
        date.DATA_RFC7231 = "D, d M Y H:i:s \G\M\T";
        date.DATA_RFC2822 = "D, d M Y H:i:s O";
        date.DATA_RFC3339 = "Y-m-d\TH:i:sP";
        date.DATA_RFC3339_EXTENDED = "Y-m-d\TH:i:s.vP";
        date.DATA_RSS = "D, d M Y H:i:s O";
        date.DATA_W3C = "Y-m-d\TH:i:sP";
        date.DATA_DB = "Y-m-d H:i:s";

        return date;
    }));

    return module.exports;
});

