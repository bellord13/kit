/** @module support */
__PluginManager__.register('support', function(require, exports, module, global){

    window.pre = console.log;

    const KIT_DATA_PREFIX = 'kit';

    var data = function(){
        function normalizeData(val) {
            if (val === 'true') {
                return true
            }

            if (val === 'false') {
                return false
            }

            if (val === Number(val).toString()) {
                return Number(val)
            }

            if (val === '' || val === 'null') {
                return null
            }

            return val
        }

        function normalizeDataKey(key) {
            return key.replace(/[A-Z]/g, chr => `-${chr.toLowerCase()}`);
        }

        const manipulator = {
            /**
             * @param element {HTMLElement}
             * @param key
             * @param value
             * @param prefix
             */
            set(element, key, value, prefix = 'kit') {
                if(prefix !== '') {
                    prefix = '-' + prefix;
                }
                element.setAttribute(`data${prefix}-${normalizeDataKey(key)}`, value)
            },

            remove(element, key, prefix = 'kit') {
                if(prefix !== '') {
                    prefix = '-' + prefix;
                }
                element.removeAttribute(`data${prefix}-${normalizeDataKey(key)}`)
            },

            getList(element, prefix = 'kit') {
                element = getNode(element);

                if (!element) {
                    return {};
                }

                const attributes = {};

                Object.keys(element.dataset)
                    .filter(key => key.startsWith(prefix))
                    .forEach(key => {
                        var regex = new RegExp(`^${prefix}`, '');
                        var pureKey = key.replace(regex, '');
                        pureKey = pureKey.charAt(0).toLowerCase() + pureKey.slice(1, pureKey.length);
                        if(pureKey !== '') {
                            attributes[pureKey] = normalizeData(element.dataset[key])
                        }
                    });

                return attributes
            },

            get(element, key, prefix = 'kit') {
                if(prefix !== '') {
                    prefix = '-' + prefix;
                }
                return normalizeData(element.getAttribute(`data${prefix}-${normalizeDataKey(key)}`))
            },
        };
        return manipulator;
    }();


    var num = {
        round,
        ceil,
        floor,
    };
    function round(num, decimals = 0) {
        num = toNumber(num);
        decimals = toNumber(decimals);
        var k = Math.pow(10, decimals);
        num = Math.round(k * num) / k;
        return num;
    }
    function ceil(num, decimals = 0) {
        num = toNumber(num);
        decimals = toNumber(decimals);
        var k = Math.pow(10, decimals);
        num = Math.ceil(k * num) / k;
        return num;
    }
    function floor(num, decimals = 0) {
        num = toNumber(num);
        decimals = toNumber(decimals);
        var k = Math.pow(10, decimals);
        num = Math.floor(k * num) / k;
        return num;
    }

    var node = {
        isRelate: function (node, container = document) {
            return node === container || container.contains(node);
        },
        /**
         * @param node {HTMLElement}
         * @return {[]}
         */
        getChainOfParents: function getChainOfParents(node) {
            var chain = [];
            if(!isNode(node)){
                return chain;
            }
            node = node.parentNode;
            while(node){
                chain.push(node);
                node = node.parentNode;
            }
            return chain;
        },
        /**
         * @param node {HTMLElement}
         * @return {[]}
         */
        getChainOfParentsWithNode: function getChainOfParentsWithNode(node) {
            var chain = [];
            if(!isNode(node)){
                return chain;
            }
            while(node){
                chain.push(node);
                node = node.parentNode;
            }
            return chain;
        },
        isInPage: function (node) {
            return (node === document.body) ? false : document.body.contains(node);
        },
    };
    var str = {
        ucFirst: function (str) {
            if (!str) return str;
            return str[0].toUpperCase() + str.slice(1);
        }
    };
    var arr = {
        isKey: function (key) {
            var n = Number(key);
            return isInteger(n) && (''+n === ''+key);
        }
    };
    function uniqueId (length = 32, firstLetter = true) {
        var idStr = '';
        if(length < 1) return idStr;
        if(firstLetter) {
            length && (idStr = (Math.floor((Math.random() * 25)) + 10).toString(36));
            if (length === 1) return idStr;
        }
        do {
            idStr += (Math.floor((Math.random() * 35))).toString(36);
        } while (idStr.length < length);
        return (idStr);
    }
    function valueEx(value) {
        var args = [];
        if(arguments.length > 1)
            for(var i = 1; arguments.length - 1 >= i;i++) {
                args.push(arguments[i]);
            }
        if(isFunction(value)) {
            return value.apply(this, args);
        }
        return value;
    }
    function empty(val){
        return arguments.length === 0 ||
            val === undefined ||
            val === null ||
            Number.isNaN(val) === true ||
            val === '' ||
            val === 0 ||
            val === false ||
            ((isObject(val) || isArray(val)) && (function () {
                for(var k in val) return false;
                return true;
            })());
    }
    function emptyEx(val) {
        return empty(val) ||
            ((isObject(val) || isArray(val)) && (function () {
                for(var k in val) return false;
                return true;
            })());
    }
    function isUndefined(value) {
        return value === undefined || Number.isNaN(value) || value === null || (typeof value === 'number' && !isFinite(value));
    }
    function getType(value) {
        if(isUndefined(value)) {
            return 'undefined';
        } else if(isArray(value)) {
            return 'array';
        }
        return typeof value;
    }
    function isFunction(value) {
        return typeof value === 'function';
    }
    function isObject(value) {
        return getType(value)  === 'object' ? value !== null : false;
    }
    function isIterable(value) {
        return isObject(value) || isArray(value);
    }
    function isNumber(value) {
        return !Number.isNaN(value) && isFinite(value) && typeof value === 'number';
    }
    function isArray(value) {
        return Array.isArray(value);
    }
    /**
     * @return {number}
     */
    function toNumber(value, def = 0) {
        var n = Number(value);
        return isNumber(n)?n:def;
    }
    function isInteger(value) {
        return isNumber(value) && (value % 1) === 0;
    }
    function isFloat(value) {
        return isNumber(value);
    }
    function isString(value) {
        return typeof value === 'string';
    }
    function isNode(value) {
        return value instanceof Node;
    }
    function isElement(value) {
        return value instanceof Element;
    }
    function isNodeList(value) {
        return value instanceof NodeList;
    }
    function isDomObject(o) {
        return isNode(o) || isElement(o) || isNodeList(o);
    }
    function isEqualArrayValues(x, y, strict = false) {
        var n = typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y);
        if(n){
            return true;
        }
        if(strict){
            return x === y;
        }
        return x == y;
    }
    /**
     * @param selector
     * @return Element
     */
    function getNode(selector) {
        if(isString(selector) && selector !== '') {
            return document.querySelector(selector);
        }
        if(isNode(selector)) {
            return selector;
        }
        return null;
    }

    var extend = function(target) {
        if(!arguments[1]) {
            return;
        }
        for(i=1; i < arguments.length; i++) {
            var source = arguments[i];

            for(var prop in source) {
                if(!target[prop] && source.hasOwnProperty(prop)) {
                    target[prop] = source[prop];
                }
            }
        }
    };
    function isA(o,c){
        return null!=c&&"undefined"!=typeof Symbol&&c[Symbol.hasInstance]?!!c[Symbol.hasInstance](o):o instanceof c
    }
    function makeNode(html = '') {
        if(isNode(html)) {
            return html;
        }
        if(isString(html)) {
            html = html.trim();
        }
        var nodeWrap = document.createElement('div');
        nodeWrap.innerHTML = html;
        var r = nodeWrap.firstChild;
        r && r.remove();
        return r;
    }
    function makeNodeList(html = '') {
        if(isNodeList(html)) {
            return html;
        }
        if(isString(html)) {
            html = html.trim();
        }
        var nodeWrap = document.createElement('div');
        if(isNode(html)) {
            nodeWrap.append(html);
        } else {
            nodeWrap.innerHTML = html;
        }
        return nodeWrap.childNodes;
    }
    function setNodeData(node, key, value) {
        return setObjProp(node, [KIT_DATA_PREFIX, key], value);
    }
    function setObjProp(object, branch, value) {
        if(!Array.isArray(branch)) {
            branch = [branch];
        }
        if(!isObject(object)) return false;
        var i = 0, obj = {}, result = true;
        obj[i] = object;
        branch.forEach(function (key) {
            if(!result) {
                return;
            }
            ++i;
            if(i === branch.length) {
                obj[i-1][key] = value;
                return;
            }
            obj[i-1][key] = obj[i-1][key] || {};
            result = isObject(obj[i-1][key]);
            obj[i] = obj[i-1][key];
        });
        return result;
    }
    function getNodeData(node, key, def = null) {
        return getObjProp(node, [KIT_DATA_PREFIX, key], def);
    }
    function getObjProp(object, branch, def = null) {
        if(!isArray(branch)) {
            branch = [branch];
        }
        if(!isObject(object)) return def;
        var i = 0, obj = {}, result = true;
        obj[i] = object;
        branch.forEach(function (key) {
            if(!result) {
                return;
            }
            ++i;
            if(!isObject(obj[i-1]) || !obj[i-1].hasOwnProperty(key)) {
                result = false;
                return;
            }
            obj[i] = obj[i-1][key];
        });
        if(result) {
            return obj[i];
        }
        return def;
    }
    function definePropertyGet(obj, key, val) {
        Object.defineProperty(obj, key, {
            get: function () {
                return valueEx(val);
            },
        });
    }
    function args2array(_args) {
        var args = [];
        for (var i = 0; i < _args.length; i++) {
            args.push(_args[i]);
        }
        return args;
    }
    function innerHtml(wrap, content) {
        var wrapNode = getNode(wrap);
        if(!isElement(wrapNode)) {
            return false;
        }
        while (wrapNode.firstChild) {
            wrapNode.removeChild(wrapNode.firstChild);
        }
        if(isNode(content)) {
            wrapNode.append(content);
        } else if(isNodeList(content)) {
            content.forEach(function(node){
                wrapNode.append(node);
            });
        } else {
            wrapNode.innerHTML = content;
        }
        return true;
    }
    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }
    function setTagName(node, tagName) {
        if(!tagName) {
            return null;
        }
        node = getNode(node);
        if(!isNode(node)) {
            return null;
        }
        if(node.tagName.toUpperCase() === tagName.toUpperCase()) {
            return node;
        }
        var result = document.createElement(tagName);
        var childNodes = [].slice.call(node.childNodes);
        for (var i = 0; i < [].slice.call(childNodes).length; i++) {
            result.appendChild(childNodes[i]);
        }
        node.getAttributeNames().forEach(function (attrName) {
            result.setAttribute(attrName, node.getAttribute(attrName))
        });
        if(node.parentNode){
            node.parentNode.insertBefore(result, node.nextSibling);
            node.remove();
        }
        return result;
    }

    function toString(value) {
        return String(value);
    }

    function getDataOptions(node, pluginId, prefix = 'kit') {
        node = getNode(node);
        node.getAttribute('')
    }

    /**
     * @description Событие окончания CSS transition
     */
    function whichTransitionEvent() {
        var el = document.createElement("fakeelement");
        var transitions = {
            "transition": "transitionend",
            "OTransition": "oTransitionEnd",
            "MozTransition": "transitionend",
            "WebkitTransition": "webkitTransitionEnd"
        };
        for (var t in transitions) {
            if (el.style[t] !== undefined) {
                return transitions[t];
            }
        }
    };

    /**
     * @description Событие окончания CSS animation
     */
    function whichAnimationEvent() {
        var el = document.createElement("fakeelement2");
        var animations = {
            "animation": "animationend",
            "OAnimation": "oAnimationEnd",
            "MozAnimation": "animationend",
            "WebkitAnimation": "webkitAnimationEnd"
        };
        for (var t in animations) {
            if (el.style[t] !== undefined) {
                return animations[t];
            }
        }
    }

    function arrayToObject(ar){
        return Object.assign({}, ar);
    }

    function namespace(namespace, parent = window){
        if(isArray(namespace)){
            var parts=namespace;
        } else {
            var parts=namespace.split(".");
        }
        var cursor = parent;
        for(var i=0;i<parts.length;i++){
            if(typeof cursor[parts[i]]==="undefined"){cursor[parts[i]]={};}
            if(Array.isArray(cursor[parts[i]])){
                cursor[parts[i]] = arrayToObject(cursor[parts[i]]);
            }
            cursor=cursor[parts[i]];
        }
        return cursor;
    }

    /**
     * @memberOf kit
     */
    var support = {
        node: node,
        str: str,
        arr: arr,
        num: num,
        uniqueId,
        valueEx,
        empty,
        isUndefined,
        isNumber,
        getType,
        isIterable,
        isObject,
        isArray,
        isInteger,
        isFloat,
        isString,
        isFunction,
        isNode,
        isNodeList,
        isDomObject,
        isElement,
        getNode,
        extend,
        isA,
        makeNode,
        setNodeData,
        getNodeData,
        toNumber,
        definePropertyGet,
        args2array,
        innerHtml,
        makeNodeList,
        setTagName,
        tagName: setTagName,
        data,
        setObjProp,
        getObjProp,
        whichAnimationEvent,
        whichTransitionEvent,
        namespace,
        isEqualArrayValues
    };

    return module.exports = support;
});
