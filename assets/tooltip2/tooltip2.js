/** @module Tooltip2 */
__PluginManager__.register('Tooltip2',function(require, exports, module, global){

    var Viewer = require('Viewer');
    var support = require('support');
    var Collection = require('Collection');
    var ActionCollection = require('ActionCollection');
    var Action = require('Action');
    var userAction = require('userAction');
    var align = require('align');
    var animations = require('animations');

    const PLUGIN_PREFIX = 'kit';
    const PLUGIN_NAME = 'tooltip2';

    /**
     * @class Tooltip2Default
     */
    const Default = {
        hAlign: 'center',
        vAlign: 'top',
        hDirection: 'center',
        vDirection: 'top',
        offset: 10,
        contentSelector: '.tooltip-inner',
        content: '',
        template: '<div class="tooltip" role="tooltip" style="display: none">' +
            //'<div class="tooltip-arrow" data-popper-arrow="true"></div>' +
            '<div class="tooltip-inner"></div>' +
            '</div>',
    };


    /**
     * @memberOf kit
     * @param masterNode
     * @param hintNode
     * @param options
     * @constructor
     */
    var Tooltip2 = function(masterNode, hintNode, options = {}) {
        var self = this;

        this.masterNode = support.getNode(masterNode);
        if(!support.isElement(this.masterNode)){
            console.warn('Tooltip2: masterNode not valid', this.masterNode);
        }
        var prefix = PLUGIN_PREFIX + PLUGIN_NAME.charAt(0).toUpperCase() + PLUGIN_NAME.slice(1, PLUGIN_NAME.length);
        var masterOptions = support.data.getList(this.masterNode, prefix);
        /**
         * @type {Tooltip2Default}
         */
        this.options = Object.assign({}, Default, masterOptions, options);

        this.hintNode = support.getNode(hintNode);
        if(!this.hintNode) {
            this.hintNode = support.makeNode(this.options.template);
        }
        if(!support.isElement(this.hintNode)){
            console.warn('Tooltip2: masterNode not valid', this.hintNode);
        }

        if(!support.getNodeData(this.masterNode, PLUGIN_NAME)){
            support.setNodeData(this.masterNode, PLUGIN_NAME, this);
            support.data.set(this.masterNode, PLUGIN_NAME, 'loaded');
        }

        this.hintNode.remove();
        this.masterNode.appendChild(this.hintNode);
        //support.setNodeData(this.masterNode, PLUGIN_NAME, this);

        if(String(this.options.content) !== '') {
            this.setContent(this.options.content);
        }

        var limiter = window;
        var chain = support.node.getChainOfParentsWithNode(this.masterNode);

        chain.every(function(node, i){
            if(node === document.body || node.tagName === 'HTML' || node === document){
                limiter = window;
                return false;
            }
            limiter = node;
            var style = getComputedStyle(node);
            var overflowX = style.overflowX;
            var overflowY = style.overflowY;
            if(!(overflowX === 'visible' || overflowX === 'auto') || !(overflowY === 'visible' || overflowY === 'auto')){
                return false;
            }
            return true;
        });

        this.align = new align.Class(this.hintNode, this.masterNode, limiter);
        var hint = new userAction.Hint(this.masterNode, this.hintNode);
        this.viewer = hint.viewer;
        this.viewer.delayHide = 400;
        this.viewer.animShowClass = animations.fadeIn;
        this.viewer.animHideClass = animations.fadeOut;
        this.viewer.onShow.push(function () {
            self.render();
        });
        this.action = new ActionCollection([
            hint
        ]);
    };

    var __proto = Tooltip2.prototype;

    var vDirection = new Collection(['top', 'bottom']);
    var hDirection = new Collection(['left', 'right']);

    __proto.__setOffset = function(){
        var offset = this.options.offset;
        this.align.offset = [0, 0];
        var directions = new Collection([this.options.hDirection, this.options.vDirection]);
        if(hDirection.includes(this.options.hDirection) || directions.includes('start'))
        this.align.offset[0] = offset;
        if(vDirection.includes(this.options.vDirection) || directions.includes('end'))
        this.align.offset[1] = offset;
    };

    __proto.setContent = function(html = ''){
        var titleNode = this.hintNode.querySelector(this.options.contentSelector);
        support.innerHtml(titleNode, html);
    };

    __proto.run = function(){
        this.action.run();
    };
    __proto.stop = function(){
        this.action.stop();
    };
    __proto.unset = function(){
        this.viewer.hide();
        this.stop();
        support.setNodeData(this.masterNode, PLUGIN_NAME, null);
    };
    __proto.render = function(){
        if(this.viewer.isShown()) {
            this.__setOffset();
            this.align.set(this.options.hAlign, this.options.vAlign, this.options.hDirection, this.options.vDirection);
        }
    };
    __proto.show = function(){
        this.viewer.show();
    };
    __proto.hide = function(){

    };
    /**
     * @param options {Tooltip2Default}
     */
    __proto.setOptions = function(options){
        Object.assign(this.options, options);
        this.render();
    };

    module.exports = Tooltip2;

    return module.exports;
});