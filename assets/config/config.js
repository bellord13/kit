/**
 * @namespace kit
 * @module kit
 */
window.kit = window.kit || {};
/**
 * @typedef kit.service
 */
kit.service = kit.service || {};
/**
 * @typedef kit.config
 */
kit.config = kit.config || {};

// [PluginManager]
Object.assign(kit.config, {
    debug: true,
    assetsPath: '/local/templater.dev/kit/assets',
    requiredDependencies: [],
    autoloadPluginList: ['tooltip'],
    defaultPhoneMask: '+7 (999) 999-99-99',
});