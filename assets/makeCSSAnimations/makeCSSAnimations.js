/** @module makeCSSAnimations */
__PluginManager__.register('makeCSSAnimations', function(require, exports, module, global) {

    var support = require('support');
    var $ = require('jQuery');

    var whichAnimationEvent = support.whichAnimationEvent();

    /**
     * @memberOf kit
     * @param options {{container: jQuery, animationClass: string, stateClass: string, direction: 'fwd'|'back', before: null|function,  after: null|function}}
     */
    var makeCSSAnimations = function (options) {

        var defaults = {
            container: null,
            animationClass: null,
            stateClass: null,
            before: null,
            after: null,
            direction: 'fwd' // 'back'
        };

        var settings = $.extend(defaults, options);

        if (settings.before !== null && typeof settings.before === 'function') {
            settings.before();
        }

        if (whichAnimationEvent) {

            if (settings.direction === 'fwd') {
                settings.container.addClass(settings.stateClass);
            }

            settings.container.addClass(settings.animationClass);

            settings.container.one(whichAnimationEvent, function () {
                settings.container.removeClass(settings.animationClass);
                if (settings.direction === 'back') {
                    settings.container.removeClass(settings.stateClass);
                }
                if (settings.after !== null && typeof settings.after === 'function') {
                    settings.after();
                }
            });
        } else {

            if (settings.direction === 'fwd') {
                settings.container.addClass(settings.stateClass);
            } else if (settings.direction === 'back') {
                settings.container.removeClass(settings.stateClass);
            }

            if (settings.after !== null && typeof settings.after === 'function') {
                settings.after();
            }
        }
    };

    return module.exports = makeCSSAnimations;
});