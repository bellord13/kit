var __PluginManagerConfigHandler__ = function() {
    var root = __pmConfig__.assetsPath;

    var hasJQuery = function(){
        return window.hasOwnProperty('jQuery') && window.jQuery.hasOwnProperty('fn');
    }

    return {
        menu: {
            path: root + '/menu',
            module: '/menu.js',
            css: [
                '/menu.css'
            ],
            type: 'HMD',
            deps: ['lib', 'align', 'dataStructures', 'animations', 'userAction', 'Collection', 'support', 'ActionCollection', 'Action', 'Viewer']
        },
        lib: {
            path: root + '/lib',
            module: '/lib.js',
            type: 'HMD',
        },
        align: {
            path: root + '/align',
            module: '/align.js',
            type: 'HMD',
            deps: ['support']
        },
        flexKit: {
            path: root + '/flex-kit',
            css: '/flex-kit.css'
        },
        support: {
            path: root + '/support',
            module: '/support.min.js',
            type: 'HMD',
        },
        jQueryProxy: {
            type: 'HMD',
            handler: function(require, exports, module, global){
                init = function(c, prop = '$', args = []){
                    c.push([prop, args]);

                    return new Proxy({}, {
                        get(target, prop) {
                            return function(){
                                return init(c, prop, arguments)
                            }
                        }
                    });
                };
                var J = function(){
                    if(!J.init){
                        J.init = true;
                        __PluginManager__.ready('jQuery');
                    }
                    var c = [];
                    J.queue.push(c);
                    return init(c, '$', arguments);
                }
                J.queue = [];
                J.init = false;

                function JQueryProxy(){}
                JQueryProxy.prototype.J = J;
                JQueryProxy.prototype.hasJQuery = function(){
                    return window.hasOwnProperty('jQuery') && window.jQuery.hasOwnProperty('fn');
                };
                JQueryProxy.prototype.hasJQueryIniter = function(){
                    return window.hasOwnProperty('jQuery') && Object.is(window.jQuery, J);
                };
                JQueryProxy.prototype.onJQueryRegister = function(plugin){
                    if(!this.hasJQuery()){
                        (function(w){
                            w.$ = w.jQuery = J;
                            plugin.module.initer = J;
                        })(window);
                    }
                };
                JQueryProxy.prototype.onJQueryInstall = function(plugin){
                    var self = this;
                    if(!plugin.module.initer){
                        return;
                    }
                    (function(w){
                        var q = plugin.module.initer.queue;
                        if(self.hasJQueryIniter()){
                            w.$ = w.jQuery = plugin.module.exports;
                            for (var ch of q){
                                var root = w;
                                for (var c of ch){
                                    try {
                                        root = root[c[0]].apply(root, c[1]);
                                    } catch (e){
                                        console.warn(ch, e);
                                        break;
                                    }
                                }
                            }
                        }
                    })(window);
                };

                module.exports = new JQueryProxy();
            },
        },
        jQuery: {
            path: root + '/jquery',
            module: '/jquery.min.js',
            type: 'UMD',
            alias: ['$', 'jquery'],
            onRegister: function (plugin) {
                if(hasJQuery()){
                    plugin.module.exports = window.jQuery;
                    plugin.config.module = '';
                } else {
                    plugin.proxy = kit.require('jQueryProxy');
                    plugin.proxy.onJQueryRegister(plugin);
                }
            },
            onInstall: function(plugin){
                if(plugin.proxy){
                    plugin.proxy.onJQueryInstall(plugin);
                }
            },
        },
        popper: {
            path: root + '/popper',
            alias: 'popper.js',
            module: '/popper.js',
            type: 'HMD',
            css: '/popper.css',
        },
        bsCore: {
            path: root + '/bsCore',
            module: '/bsCore.min.js',
            type: 'HMD',
            css: [
                '/bsCore.css'
            ]
        },
        bsToast: {
            path: root + '/bsToast',
            module: '/bsToast.js',
            type: 'HMD',
            deps: ['bsCore'],
        },
        bsToastService: {
            path: root + '/bsToastService',
            module: '/bsToastService.js',
            type: 'HMD',
            css: [
                '/bsToastService.css'
            ],
            deps: ['bsToast', 'support', 'Collection'],
        },
        Tooltip: {
            path: root + '/tooltip',
            module: '/tooltip.js',
            type: 'HMD',
            css: '/tooltip.css',
            onInstall: function (plugin) {
                var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
                var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return __pmGlobal__.Tooltip.getOrCreateInstance(tooltipTriggerEl, {sanitize: false});
                });
            },
            deps: ['popper', 'bsCore'],
        },
        Tooltip2: {
            path: root + '/tooltip2',
            module: '/tooltip2.js',
            type: 'HMD',
            css: '/tooltip2.css',
            onInstall: function (plugin) {
                document.addEventListener('mouseover', function (event) {
                    // console.log(event);
                    // console.log(event.target);
                    if (!(event.target && event.target.dataset.kitTooltip2 === 'autoload')) {
                        return;
                    }
                    // console.log(1);
                    if (kit.support.getNodeData(event.target, 'tooltip2')) {
                        return;
                    }
                    // console.log(2);
                    var tooltip = new __pmGlobal__.Tooltip2(event.target);
                    tooltip.run();
                    tooltip.show();
                });
                // var elements = [].slice.call(document.querySelectorAll('[data-kit-tooltip2="autoload"]'));
                // elements.map(function (element) {
                //     var tooltip =  new __pmGlobal__.Tooltip2(element);
                //     tooltip.run();
                //     console.log(tooltip);
                // });
            },
            deps: ['Viewer', 'support', 'ActionCollection', 'Action', 'userAction', 'align', 'Collection', 'animations'],
        },
        Popover: {
            path: root + '/popover',
            module: '/popover.js',
            type: 'HMD',
            css: '/popover.css',
            onInstall: function () {
                var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
                var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
                    return new __pmGlobal__.Popover(popoverTriggerEl);
                });
            },
            deps: ['popper', 'bsCore', 'Tooltip'],
        },
        mask: {
            path: root + '/mask',
            module: '/mask.js',
            type: 'HMD',
            deps: ['jQuery'],
        },
        toolkit: {
            path: root + '/toolkit',
            module: '/toolkit.min.js',
            type: 'HMD',
            css: ['/toolkit.css'],
            deps: ['jQuery', 'mask', 'support'],
        },
        Modal: {
            path: root + '/modal',
            module: '/modal.min.js',
            type: 'HMD',
            css: [
                '/modal.css',
            ],
            deps: ['bsCore'],
            onRegister: function (plugin) {
                plugin.onLoadConditionHandler = function (event) {
                    if (event.target && event.target.dataset && event.target.dataset.bsTarget && event.target.dataset.bsToggle === 'modal') {
                        kit.exec(plugin.id, function () {
                            var newEvent = new Event("click");
                            event.target.dispatchEvent(newEvent);
                        });
                    }
                };
                window.addEventListener('click', plugin.onLoadConditionHandler, false);
            },
            onInstall: function (plugin) {
                if (plugin.onLoadConditionHandler) {
                    window.removeEventListener('click', plugin.onLoadConditionHandler, false);
                }
            },
        },
        collapse: {
            path: root + '/collapse',
            module: '/collapse.min.js',
            type: 'HMD',
            css: [
                '/collapse.css',
            ],
            deps: ['bsCore'],
        },
        datepicker: {
            path: root + '/datepicker',
            module: '/js/datepicker.js',
            type: 'HMD',
            css: [
                '/css/datepicker.css',
            ],
            deps: ['jQuery'],
        },
        cookie: {
            path: root + '/cookie',
            module: '/cookie.min.js',
            type: 'HMD',
        },
        animations: {
            path: root + '/animations',
            css: '/animations.css',
            module: '/animations.js',
            type: 'HMD',
        },
        Collection: {
            path: root + '/Collection',
            module: '/Collection.js',
            type: 'HMD',
            deps: ['support'],
        },
        dataStructures: {
            path: root + '/dataStructures',
            module: '/dataStructures.min.js',
            type: 'HMD',
        },
        userAction: {
            path: root + '/userAction',
            module: '/userAction.js',
            type: 'HMD',
            deps: ['support', 'ActionCollection', 'Action', 'Viewer'],
        },
        Viewer: {
            path: root + '/Viewer',
            module: '/Viewer.js',
            type: 'HMD',
            deps: ['Collection', 'support', 'ActionCollection', 'Action'],
        },
        ActionCollection: {
            path: root + '/ActionCollection',
            module: '/ActionCollection.js',
            type: 'HMD',
            deps: ['Collection', 'support', 'Action'],
        },
        Action: {
            path: root + '/Action',
            module: '/Action.js',
            type: 'HMD',
            deps: ['support'],
        },
        date: {
            path: root + '/date',
            module: '/date.js',
            type: 'HMD',
            deps: [],
        },
        testing: {
            path: root + '/testing',
            module: '/testing.js',
            type: 'HMD',
            deps: ['log'],
        },
        log: {
            path: root + '/log',
            module: '/log.js',
            css: '/log.css',
            type: 'HMD',
            deps: ['support', 'Collection', 'lib']
        },
        fancybox2: {
            path: root + '/fancybox/v2',
            module: '',
            js: ['/jquery.fancybox.pack.js'],
            css: ['/jquery.fancybox.css'],
            type: 'default',
            alias: ['jQuery.Fancybox2'],
            deps: ['jQuery'],
        },
        fancybox3: {
            path: root + '/fancybox/v3',
            module: '',
            js: ['/jquery.fancybox.min.js'],
            css: ['/jquery.fancybox.min.css'],
            type: 'default',
            alias: ['jQuery.Fancybox3'],
            deps: ['jQuery'],
        },
        fancybox4: {
            path: root + '/fancybox/v4',
            module: '/fancybox.umd.js',
            js: [],
            css: ['/fancybox.css'],
            type: 'UMD',
            alias: [],
        },
        fancybox5: {
            path: root + '/fancybox/v5',
            module: '/fancybox.umd.js',
            js: [],
            css: ['/fancybox.css'],
            type: 'UMD',
            alias: ['fancybox'],
        },
        lightGallery: {
            path: root + '/lightGallery',
            css: '/lightGallery.css',
            js: '/lightGallery.js',
            deps: ['jQuery'],
        },
        owlCarousel: {
            path: root + '/owlCarousel',
            css: '/owlCarousel.css',
            js: '/owlCarousel.js',
            deps: ['jQuery'],
        },
        'jQuery.Chosen': {
            path: root + '/jquery/chosen',
            css: '/chosen.css',
            js: '/chosen.jquery.js',
            type: 'HMD',
            alias: ['jquery-chosen', 'chosen', 'harvesthq/chosen'],
            deps: ['jQuery'],
        },
        makeCSSAnimations: {
            path: root + '/makeCSSAnimations',
            js: '/makeCSSAnimations.js',
            deps: ['jQuery', 'support'],
        },
        mCustomScrollbar: {
            path: root + '/mCustomScrollbar',
            alias: 'malihu-custom-scrollbar-plugin',
            js: '/mCustomScrollbar.js',
            css: '/jquery.mCustomScrollbar.css',
            deps: ['jQuery', 'jqueryMousewheel'],
            onInstall: function (plugin) {
                plugin.module.exports(kit.require('jQuery'));
            }
        },
        'jQuery.Mousewheel': {
            path: root + '/jquery/Mousewheel',
            module: '/jquery.mousewheel.min.js',
            type: 'UMD',
            alias: ['jquery-mousewheel', 'jqueryMousewheel'],
            deps: ['jQuery'],
            onInstall: function (plugin) {
                plugin.module.exports(kit.require('jQuery'));
            }
        },
        'jQuery.Lazy': {
            path: root + '/jquery/Lazy',
            alias: ['jquery-lazy', 'jqueryLazy'],
            js: [
                '/jquery.lazy.min.js',
                '/jquery.lazy.plugins.min.js',
            ],
            deps: ['jQuery'],
        },
        slickCarousel: {
            path: root + '/slickCarousel',
            alias: ['slick', 'slick-carousel'],
            js: '/slick.min.js',
            css: [
                '/slick.css',
                '/slick-theme.css',
            ],
            deps: ['jQuery'],
        },
        mustache: {
            path: root + '/mustache',
            alias: ['mustache.js'],
            js: '/mustache.js',
            deps: [],
        },
        bxSlider: {
            path: root + '/bxSlider',
            module: '/jquery.bxslider.min.js',
            css: [
                '/jquery.bxslider.min.css',
            ],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.Selectric': {
            path: root + '/jquery/Selectric',
            module: '/jquery.selectric.min.js',
            css: [
                '/selectric.css',
            ],
            type: 'UMD',
            alias: ['jquerySelectric', 'selectric'],
            deps: ['jQuery'],
        },
        'jQuery.jBox': {
            path: root + '/jquery/jBox',
            module: '/jBox.min.js',
            css: [
                '/jBox.min.css',
            ],
            type: 'default',
            alias: ['jBox'],
            deps: ['jQuery'],
        },
        intlTelInput: {
            path: root + '/intl-tel-input',
            module: '/js/intlTelInput.min.js',
            css: [
                '/css/intlTelInput.min.css',
            ],
            type: 'UMD',
            alias: ['intl-tel-input'],
        },
        autoComplete: {
            path: root + '/autoComplete',
            module: '/autoComplete.min.js',
            css: [
                '/css/autoComplete.css',
            ],
            type: 'HMD',
        },
        'jQuery.FlexSlider': {
            path: root + '/jquery/FlexSlider',
            module: '',
            js: [
                '/jquery.flexslider-min.js',
            ],
            css: [
                '/flexslider.css',
            ],
            alias: ['jquery.flexslider', 'FlexSlider', 'flexslider'],
            type: 'default',
            deps: ['jQuery'],
        },
        /**
         * Получите фактическую ширину / высоту невидимых элементов DOM с помощью jQuery.
         * @see https://github.com/dreamerslab/jquery.actual
         */
        'jQuery.Actual': {
            path: root + '/jquery/actual',
            module: '',
            js: [
                '/jquery.actual.js',
            ],
            css: [],
            alias: ['jquery.actual'],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.jqModal': {
            path: root + '/jquery/jqModal',
            module: '/jqModal.js',
            js: [],
            css: [],
            alias: ['jqmodal', 'jqModal'],
            type: 'UMD',
            deps: ['jQuery'],
        },
        'jQuery.History': {
            path: root + '/jquery/history',
            module: '',
            js: ['/jquery.history.min.js'],
            css: [],
            alias: ['jquery.history'],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.Validation': {
            path: root + '/jquery/validation',
            module: '/jquery.validate.min.js',
            js: [
                '/localization/messages_ru.js'
            ],
            css: [],
            loadJsAfterModule: true,
            alias: ['jquery-validation', 'jquery.validation', 'jquery.validate'],
            type: 'UMD',
            deps: ['jQuery'],
        },
        'jQuery.Inputmask': {
            path: root + '/jquery/inputmask',
            module: '/min/jquery.inputmask.bundle.min.js',
            js: [],
            css: [],
            alias: ['jquery.inputmask'],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.Easing': {
            path: root + '/jquery/easing',
            module: '/jquery.easing.min.js',
            js: [],
            css: [],
            alias: ['jquery.easing'],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.Sly': {
            path: root + '/jquery/sly',
            module: '/sly.min.js',
            js: [],
            css: [],
            alias: ['jquery.sly', 'sly'],
            type: 'default',
            deps: ['jQuery'],
        },
        /**
         * выравнивает высоту элементов с классом equalize в группах или на всей странице
         * @see https://github.com/eustasy-archive/jQuery.equalize
         * @deprecated
         */
        'jQuery.Equalize': {
            path: root + '/jquery/equalize.js',
            module: '/js/equalize.min.js',
            js: [],
            css: [],
            alias: ['equalize', 'equalize.js', 'jquery.equalize'],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.Alphanumeric': {
            path: root + '/jquery/alphanumeric',
            module: '/jquery.alphanumeric.pack.js',
            js: [],
            css: [],
            alias: ['jquery.alphanumeric'],
            type: 'default',
            deps: ['jQuery'],
        },
        'jQuery.Cookie': {
            path: root + '/jquery/cookie',
            module: '/src/jquery.cookie.js',
            js: [],
            css: [],
            alias: ['jquery.cookie'],
            type: 'UMD',
            deps: ['jQuery'],
        },
        'jQuery.Plugin': {
            path: root + '/jquery/plugin',
            module: '/jquery.plugin.min.js',
            js: [],
            css: [],
            alias: ['jquery.plugin', 'kbw-plugin'],
            type: 'default',
            deps: ['jQuery'],
        },
    };
};