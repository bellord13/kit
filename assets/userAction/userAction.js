/** @module userAction */
__PluginManager__.register('userAction', function(require, exports, module, global) {

    var support = require('support');
    var ActionCollection = require('ActionCollection');
    var Action = require('Action');
    var Viewer = require('Viewer');


    /**
     *
     * @param masterNode
     * @param slaveNode
     * @return {Popover}
     * @constructor
     */
    var Popover = function (masterNode, slaveNode) {
        this.__setMasterNode(masterNode);
        this.__setSlaveNode(slaveNode);
        this.__documentAction = new Action(document, 'click', this.__documentHandler.bind(this));
        this.__masterAction = new Action(this.__masterNode, 'click', this.__masterHandler.bind(this));

        this.viewer = new Viewer(this.__slaveNode, this.__masterNode);
        this.viewer.onShow.set('documentActionRun', this.__documentAction.run.bind(this.__documentAction));
        this.viewer.onHide.set('documentActionStop', this.__documentAction.stop.bind(this.__documentAction));
        return this;
    };
    (function () {
        var proto = Popover.prototype;

        proto.__documentHandler = function(event) {
            if(
                !support.node.isRelate(event.target, this.__masterNode) &&
                !support.node.isRelate(event.target, this.__slaveNode)
            ) {
                this.viewer.hide();
            }
        };
        proto.__masterHandler = function(event) {
            if(
                !support.node.isRelate(event.target, this.__slaveNode)
            ) {
                this.viewer.toggle();
            }
        };
        proto.__setMasterNode = function(selector){
            this.__masterNode = support.getNode(selector);
        };
        proto.__setSlaveNode = function(selector){
            this.__slaveNode = support.getNode(selector);
        };
        proto.run = function(){
            if(this.viewer.isShown()) {
                this.__documentAction.run();
            }
            this.__masterAction.run();
        };
        proto.stop = function(){
            this.__documentAction.stop();
            this.__masterAction.stop();
        };
    })();


    /**
     * @class
     * @param masterNode
     * @param slaveNode
     * @constructor
     */
    function Hint (masterNode, slaveNode) {
        this.__setMasterNode(masterNode);
        this.__setSlaveNode(slaveNode);
        this.viewer = new Viewer(this.__slaveNode, this.__masterNode);

        this.__masterAction = new ActionCollection([
            new Action(this.__masterNode, 'mouseover', function (event) {
                if(support.node.isRelate(event.target, this.__masterNode)) {
                    this.viewer.show();
                    // console.log('master over ', event.target.id, ' > ', this.__slaveNode.id, 'show');
                    // console.log(event);
                }
            }.bind(this)),
            new Action(this.__masterNode, 'mouseout', function (event) {
                if(
                    !support.node.isRelate(event.relatedTarget, this.__masterNode) &&
                    !support.node.isRelate(event.relatedTarget, this.__slaveNode)
                ) {
                    this.viewer.hide();
                    // console.log('master out ', event.target.id, ' > ', this.__slaveNode.id, 'hide');
                    // console.log(event);
                }
            }.bind(this))
        ]);
        this.__slaveAction = new ActionCollection([
            new Action(this.__slaveNode, 'mouseover', function (event) {
                // this.viewer.show();
                // console.log('slave over ', event.target.id, ' > ', this.__slaveNode.id, 'show');
            }.bind(this)),
            new Action(this.__slaveNode, 'mouseout', function (event) {
                if(
                    !support.node.isRelate(event.relatedTarget, this.__masterNode) &&
                    !support.node.isRelate(event.relatedTarget, this.__slaveNode)
                ) {
                    this.viewer.hide();
                    // console.log('slave out ', event.target.id, ' > ', this.__slaveNode.id, 'hide');
                }
            }.bind(this))
        ]);
        this.__documentAction = new Action(document, 'click', this.__documentHandler.bind(this));

        this.viewer.onShow.set('documentActionRun', (function (){
            this.__slaveAction.run();
            this.__documentAction.run();
        }).bind(this));
        this.viewer.onHide.set('documentActionStop', (function (){
            this.__slaveAction.stop();
            this.__documentAction.stop();
        }).bind(this));
        return this;
    };
    (function () {
        var proto = Hint.prototype;

        proto.__documentHandler = function(event) {
            if (
                !support.node.isRelate(event.target, this.__slaveNode) &&
                !support.node.isRelate(event.target, this.__masterNode) &&
                this.viewer.isShown()
            ) {
                this.viewer.hide();
            }
        };
        proto.__setMasterNode = function(selector){
            this.__masterNode = support.getNode(selector);
        };
        proto.__setSlaveNode = function(selector){
            this.__slaveNode = support.getNode(selector);
        };
        proto.run = function(){
            if(this.viewer.isShown()) {
                this.__documentAction.run();
                this.__slaveAction.run();
            }
            this.__masterAction.run();
        };
        proto.stop = function(){
            this.__documentAction.stop();
            this.__masterAction.stop();
            this.__slaveAction.stop();
        };
    })();

    /**
     * @memberOf kit
     */
    var userAction = {
        Popover,
        /**
         * @type {Hint}
         */
        Hint,
    };

    return module.exports = userAction;
});
