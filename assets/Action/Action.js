/** @module Action */
__PluginManager__.register('Action', function(require, exports, module, global) {

    var support = require('support');

    /**
     * @memberOf kit
     */
    var Action = function (node, eventName, handler) {
        this.node = support.getNode(node);
        this.eventName = eventName;
        this.handler = function () {
            if(this.count > -1) {
                this.count--;
                if(this.count === -1) {
                    return;
                }
                if(this.count === 0) {
                    this.stop();
                }
            }
            handler.apply(this, arguments);
        }.bind(this);
        this.count = -1;
    };
    Action.prototype.run = function (count = -1) {
        this.stop();
        this.count = count;
        this.node.addEventListener(this.eventName, this.handler, false);
    };
    Action.prototype.stop = function () {
        this.count = -1;
        this.node.removeEventListener(this.eventName, this.handler, false);
    };

    return module.exports = Action;
});