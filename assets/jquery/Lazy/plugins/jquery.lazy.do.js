/*!
 * jQuery & Zepto Lazy - YouTube Plugin - v1.5
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // load youtube video iframe, like:
    // <iframe data-loader="do" data-callback="__callback" ></iframe>
    $.lazy(['do'], function(element, response) {
        var callback = element.attr('data-callback');
        if (typeof window[callback] === 'function') {
            window[callback](element, response);
            // remove attribute
            if (this.config('removeAttribute')) {
                element.removeAttr('data-callback');
            }
        } else {
            response(false);
        }
    });
})(window.jQuery || window.Zepto);