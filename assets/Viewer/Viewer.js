/** @module Viewer */
__PluginManager__.register('Viewer', function(require, exports, module, global) {

    var Collection = require('Collection');
    var support = require('support');
    var Action = require('Action');
    var ActionCollection = require('ActionCollection');



    var AnimationTrap = function (node, callbackBefore = function(){}, callbackAfter = function(){}) {
        this.node = support.getNode(node);
        this.active = false;
        this.__timeoutId = 0;
        this.__callbackOptions = {};
        this.callbackBefore = callbackBefore;
        this.callbackAfter = callbackAfter;
        this.__callbackAfterHandler = function () {
            this.callbackAfter(this.__callbackOptions);
            this.stop();
        }.bind(this);
        this.__animationstart = false;
        this.__animationStartAction = new Action(this.node, 'animationstart', () => {
            this.__animationstart = true;
        });

        this.__animationEndAction = new ActionCollection([
            new Action(this.node, 'animationend', this.__callbackAfterHandler),
            new Action(this.node, 'animationcancel', this.__callbackAfterHandler)
        ]);
    };
    AnimationTrap.prototype.run = function (options = {}) {

        if(this.active){
            return;
        }

        this.__callbackOptions = Object.assign({
            useAnim: true
        }, options);


        if(!this.__callbackOptions.useAnim){
            this.callbackBefore(this.__callbackOptions);
            this.callbackAfter(this.__callbackOptions);
            this.stop();
            return;
        }

        this.active = true;
        this.__animationStartAction.run(1);
        this.callbackBefore(this.__callbackOptions);
        this.__animationEndAction.run(1);

        this.__timeoutId = setTimeout(function () {
            if(!this.__animationstart) {
                this.__callbackAfterHandler(this.__callbackOptions);
            }
        }.bind(this), 100);
    };
    AnimationTrap.prototype.stop = function () {
        clearTimeout(this.__timeoutId);
        this.__timeoutId = 0;
        this.__callbackOptions = {};
        this.__animationstart = false;
        this.__animationStartAction.stop();
        this.__animationEndAction.stop();
        this.active = false;
    };




    var removeAnimation = function(animationClass){
        if(animationClass && this.node.classList.contains(animationClass)){
            this.node.classList.remove(animationClass);
        }
    };
    var addAnimation = function(animationClass){
        if(animationClass && !this.node.classList.contains(animationClass)){
            this.node.classList.add(animationClass);
        }
    };


    /**
     * @memberOf kit
     */
    var Viewer = function (node, masterNode) {
        this.node = support.getNode(node);
        this.masterNode = support.getNode(masterNode);
        this.onShow = new Collection();
        this.onShowBefore = new Collection();
        this.onHide = new Collection();
        this.onHideBefore = new Collection();
        this.animShowClass = '';
        this.animHideClass = '';
        this.delayHide = 0;
        this.activeMasterClass = 'active';

        this.__aminationTrapHide = new AnimationTrap(this.node, function (options) {
            if(options.onHideBefore !== false) {
                this.onHideBefore.eachCall(this.node, options);
            }
            removeAnimation.call(this, this.animShowClass);
            addAnimation.call(this, this.animHideClass);
        }.bind(this), function (options) {
            if(this.masterNode && this.activeMasterClass) {
                this.masterNode.classList.remove(this.activeMasterClass);
            }
            this.node.classList.add('hide');
            this.node.classList.remove('show');
            if(options.onHide !== false) {
                this.onHide.eachCall(this.node, options);
            }
            this.node.style.display = 'none';
        }.bind(this));

        this.__aminationTrapShow = new AnimationTrap(this.node, function (options = {}) {
            if(options.onShowBefore !== false) {
                this.onShowBefore.eachCall(this.node, options);
            }
            this.node.classList.remove('hide');
            this.node.classList.add('show');
            if(this.node.style.display === 'none') {
                this.node.style.display = '';
            }
            if(this.masterNode && this.activeMasterClass) {
                this.masterNode.classList.add(this.activeMasterClass);
            }

            removeAnimation.call(this, this.animHideClass);
            addAnimation.call(this, this.animShowClass);
            if(options.onShow !== false) {
                this.onShow.eachCall(this.node, options);
            }
        }.bind(this), function (options = {}) {

        }.bind(this));

    };

    var deferredHideCancel = function () {
        if(this.__deferredHideID) {
            clearTimeout(this.__deferredHideID);
            this.__deferredHideID = 0;
        }
    };

    var proto = Viewer.prototype;
    proto.show = function(options = {}){
        //console.log('viewer show', this.node.kit.menu.id, options);
        if(this.isShown() && !options.force) {
            return;
        }

        options.useAnim = !!this.animHideClass;

        this.stop();

        this.__aminationTrapShow.run(options);
    };

    proto.hide = function(options = {}){
        //console.log('viewer hide', this.node.kit.menu.id);
        if(this.isHidden() && !options.force) {
            return;
        }

        options.useAnim = !!this.animHideClass;

        this.__aminationTrapShow.stop();

        var hideDeferred = function () {
            deferredHideCancel.call(this);
            if(this.delayHide) {
                this.__deferredHideID = setTimeout(hide.bind(this), this.delayHide);
                return;
            }
            hide();
        }.bind(this);

        var hide = function() {
            deferredHideCancel.call(this);
            this.__aminationTrapHide.run(options);
        }.bind(this);

        hideDeferred();
    };
    proto.toggle = function(){
        if(this.isHidden()) {
            this.show();
            return true;
        }
        this.hide();
        return false;
    };
    proto.isShown = function(){
        return !this.isHidden();
    };
    proto.isHidden = function(){
        return this.__aminationTrapHide.active || this.__deferredHideID || this.node.classList.contains('hide') ||
            (getComputedStyle(this.node).display === 'none') ||
            (this.masterNode && this.activeMasterClass && !this.masterNode.classList.contains(this.activeMasterClass));

        // var result = this.node.classList.contains('hide');
        // result = result || getComputedStyle(this.node).display === 'none';
        // if(this.masterNode && this.activeMasterClass) {
        //     result = result || !this.masterNode.classList.contains(this.activeMasterClass);
        // }
        // return result || this.__aminationTrapHide.active || this.__deferredHideID;
    };
    proto.stop = function () {
        this.__aminationTrapHide.stop();
        this.__aminationTrapShow.stop();
        deferredHideCancel.call(this);
        // removeAnimation.call(this, this.animHideClass);
        // removeAnimation.call(this, this.animShowClass);
    };
    return module.exports = Viewer;
});
