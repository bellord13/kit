/** @module testing */
__PluginManager__.register('testing', function(require, exports, module, global){

    // var loggerService = require('loggerService');

    /**
     * @constructor
     */
    function Unit() {
        this.logger = kit.service.logger;
    }

    /**
     * @param logger {kit.Logger}
     */
    Unit.prototype.setLogger = function (logger) {
        this.logger = logger;
    };

    Unit.prototype.assertSame = function (expected, actual, message) {
        if(expected === actual) {
            this.logger.success({pass: message});
            return true;
        }
        if(expected == actual) {
            this.logger.warning({warn: message}, {expected: expected, actual: actual});
            return false;
        }
        this.logger.error({fail: message}, {expected: expected, actual: actual});
        return false;
    };

    /**
     * @memberOf kit
     */
    var testing = {
        Unit,
    };

    return module.exports = testing;
});
