/** @module bsToast */
__PluginManager__.register('bsToast', function(require, exports, module){
    var bsCore = require('bsCore');

    var
        defineJQueryPlugin = bsCore.util.index.defineJQueryPlugin,
        getElementFromSelector = bsCore.util.index.getElementFromSelector,
        isRTL = bsCore.util.index.isRTL,
        isVisible = bsCore.util.index.isVisible,
        reflow = bsCore.util.index.reflow,
        typeCheckConfig = bsCore.util.index.typeCheckConfig,
        EventHandler = bsCore.dom.EventHandler,
        Manipulator = bsCore.dom.Manipulator,
        SelectorEngine = bsCore.dom.SelectorEngine,
        getSelector = bsCore.util.index.getSelector,
        isDisabled = bsCore.util.index.isDisabled,
        enableDismissTrigger = bsCore.util.componentFunctions.enableDismissTrigger,
        ScrollBarHelper = bsCore.util.ScrollBarHelper,
        BaseComponent = bsCore.BaseComponent,
        Backdrop = bsCore.util.Backdrop;


    const _interopDefaultLegacy = e => e && typeof e === 'object' && 'default' in e ? e : { default: e };

    const EventHandler__default = /*#__PURE__*/_interopDefaultLegacy(EventHandler);
    const Manipulator__default = /*#__PURE__*/_interopDefaultLegacy(Manipulator);
    const BaseComponent__default = /*#__PURE__*/_interopDefaultLegacy(BaseComponent);


    /**
     * --------------------------------------------------------------------------
     * Bootstrap (v5.1.3): toast.js
     * Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)
     * --------------------------------------------------------------------------
     */
    /**
     * ------------------------------------------------------------------------
     * Constants
     * ------------------------------------------------------------------------
     */

    const NAME = 'toast';
    const DATA_KEY = 'bs.toast';
    const EVENT_KEY = `.${DATA_KEY}`;
    const EVENT_MOUSEOVER = `mouseover${EVENT_KEY}`;
    const EVENT_MOUSEOUT = `mouseout${EVENT_KEY}`;
    const EVENT_FOCUSIN = `focusin${EVENT_KEY}`;
    const EVENT_FOCUSOUT = `focusout${EVENT_KEY}`;
    const EVENT_HIDE = `hide${EVENT_KEY}`;
    const EVENT_HIDDEN = `hidden${EVENT_KEY}`;
    const EVENT_SHOW = `show${EVENT_KEY}`;
    const EVENT_SHOWN = `shown${EVENT_KEY}`;
    const CLASS_NAME_FADE = 'fade';
    const CLASS_NAME_HIDE = 'hide'; // @deprecated - kept here only for backwards compatibility

    const CLASS_NAME_SHOW = 'show';
    const CLASS_NAME_SHOWING = 'showing';
    const DefaultType = {
        animation: 'boolean',
        autohide: 'boolean',
        delay: 'number'
    };
    const Default = {
        animation: true,
        autohide: true,
        delay: 5000
    };
    /**
     * ------------------------------------------------------------------------
     * Class Definition
     * ------------------------------------------------------------------------
     */

    class Toast extends BaseComponent__default.default {
        constructor(element, config) {
            super(element);
            this._config = this._getConfig(config);
            this._timeout = null;
            this._hasMouseInteraction = false;
            this._hasKeyboardInteraction = false;

            this._setListeners();
        } // Getters


        static get DefaultType() {
            return DefaultType;
        }

        static get Default() {
            return Default;
        }

        static get NAME() {
            return NAME;
        } // Public


        show() {
            const showEvent = EventHandler__default.default.trigger(this._element, EVENT_SHOW);

            if (showEvent.defaultPrevented) {
                return;
            }

            this._clearTimeout();

            if (this._config.animation) {
                this._element.classList.add(CLASS_NAME_FADE);
            }

            const complete = () => {
                this._element.classList.remove(CLASS_NAME_SHOWING);

                EventHandler__default.default.trigger(this._element, EVENT_SHOWN);

                this._maybeScheduleHide();
            };

            this._element.classList.remove(CLASS_NAME_HIDE); // @deprecated


            reflow(this._element);

            this._element.classList.add(CLASS_NAME_SHOW);

            this._element.classList.add(CLASS_NAME_SHOWING);

            this._queueCallback(complete, this._element, this._config.animation);
        }

        hide() {
            if (!this._element.classList.contains(CLASS_NAME_SHOW)) {
                return;
            }

            const hideEvent = EventHandler__default.default.trigger(this._element, EVENT_HIDE);

            if (hideEvent.defaultPrevented) {
                return;
            }

            const complete = () => {
                this._element.classList.add(CLASS_NAME_HIDE); // @deprecated


                this._element.classList.remove(CLASS_NAME_SHOWING);

                this._element.classList.remove(CLASS_NAME_SHOW);

                EventHandler__default.default.trigger(this._element, EVENT_HIDDEN);
            };

            this._element.classList.add(CLASS_NAME_SHOWING);

            this._queueCallback(complete, this._element, this._config.animation);
        }

        dispose() {
            this._clearTimeout();

            if (this._element.classList.contains(CLASS_NAME_SHOW)) {
                this._element.classList.remove(CLASS_NAME_SHOW);
            }

            super.dispose();
        } // Private


        _getConfig(config) {
            config = { ...Default,
                ...Manipulator__default.default.getDataAttributes(this._element),
                ...(typeof config === 'object' && config ? config : {})
            };
            typeCheckConfig(NAME, config, this.constructor.DefaultType);
            return config;
        }

        _maybeScheduleHide() {
            if (!this._config.autohide) {
                return;
            }

            if (this._hasMouseInteraction || this._hasKeyboardInteraction) {
                return;
            }

            this._timeout = setTimeout(() => {
                this.hide();
            }, this._config.delay);
        }

        _onInteraction(event, isInteracting) {
            switch (event.type) {
                case 'mouseover':
                case 'mouseout':
                    this._hasMouseInteraction = isInteracting;
                    break;

                case 'focusin':
                case 'focusout':
                    this._hasKeyboardInteraction = isInteracting;
                    break;
            }

            if (isInteracting) {
                this._clearTimeout();

                return;
            }

            const nextElement = event.relatedTarget;

            if (this._element === nextElement || this._element.contains(nextElement)) {
                return;
            }

            this._maybeScheduleHide();
        }

        _setListeners() {
            EventHandler__default.default.on(this._element, EVENT_MOUSEOVER, event => this._onInteraction(event, true));
            EventHandler__default.default.on(this._element, EVENT_MOUSEOUT, event => this._onInteraction(event, false));
            EventHandler__default.default.on(this._element, EVENT_FOCUSIN, event => this._onInteraction(event, true));
            EventHandler__default.default.on(this._element, EVENT_FOCUSOUT, event => this._onInteraction(event, false));
        }

        _clearTimeout() {
            clearTimeout(this._timeout);
            this._timeout = null;
        } // Static


        static jQueryInterface(config) {
            return this.each(function () {
                const data = Toast.getOrCreateInstance(this, config);

                if (typeof config === 'string') {
                    if (typeof data[config] === 'undefined') {
                        throw new TypeError(`No method named "${config}"`);
                    }

                    data[config](this);
                }
            });
        }

    }

    enableDismissTrigger(Toast);
    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     * add .Toast to jQuery only if jQuery is present
     */

    defineJQueryPlugin(Toast);


    /**
     * @memberOf {kit}
     * @class bsToast
     **/
    var bsToast = Toast;

    module.exports = Toast;

    return module.exports;
});
