/** @module log */
__PluginManager__.register('log', function(require, exports, module, /** @param {kit}*/global){

    var support = require('support');
    var Collection = require('Collection');
    var lib = require('lib');

    var LogLevel = {
        EMERGENCY: 'emergency',
        ALERT: 'alert',
        CRITICAL: 'critical',
        ERROR: 'error',
        WARNING: 'warning',
        NOTICE: 'notice',
        INFO: 'info',
        DEBUG: 'debug',
        SUCCESS: 'success',
    };

    /**
     * @class Logger
     */
    class Logger
    {
        constructor() {
            this.collection = new Collection();
            this.onLog = new Collection();
        }
    }


    /**
     * @param collection {kit.Collection}
     */
    Logger.prototype.setCollection = function (collection) {
        this.collection = collection;
    };

    Logger.prototype.emergency = function (message, context) {
        this.log(LogLevel.EMERGENCY, message, context);
    };
    Logger.prototype.alert = function (message, context) {
        this.log(LogLevel.ALERT, message, context);
    };
    Logger.prototype.critical = function (message, context) {
        this.log(LogLevel.CRITICAL, message, context);
    };
    Logger.prototype.error = function (message, context) {
        this.log(LogLevel.ERROR, message, context);
    };
    Logger.prototype.warning = function (message, context) {
        this.log(LogLevel.WARNING, message, context);
    };
    Logger.prototype.notice = function (message, context) {
        this.log(LogLevel.NOTICE, message, context);
    };
    Logger.prototype.info = function (message, context) {
        this.log(LogLevel.INFO, message, context);
    };
    Logger.prototype.debug = function (message, context) {
        this.log(LogLevel.DEBUG, message, context);
    };
    Logger.prototype.success = function (message, context) {
        this.log(LogLevel.SUCCESS, message, context);
    };
    Logger.prototype.log = function (level, message, context = []) {
        var data = {
            level: level,
            message: message,
            context: context,
        };
        this.__logData(data);
    };
    Logger.prototype.__logData = function (data) {
        this.collection.push(data);
        this.onLog.eachCall(this, data);
    };

    const HINT_DATA_KEY_LIST = ['id', 'createdAt', 'userId', 'ip'];
    function getTooltipContent(data) {
        var contentList = [];
        HINT_DATA_KEY_LIST.map(function (item) {
            if(data[item]){contentList.push(item+': ' + data[item]);}
        });
        return contentList.join('<br>');
    }
    function getOptionsTemplate(data) {
        var optionsTemplate = '';
        var content = getTooltipContent(data);
        if(content) {
            optionsTemplate = '<logger-item__options class="kit-picture-line" ' +
                'data-kit-tooltip2="autoload" ' +
                'data-kit-tooltip2-h-align="left" ' +
                'data-kit-tooltip2-h-direction="left" ' +
                'data-kit-tooltip2-v-align="top" ' +
                'data-kit-tooltip2-v-direction="top" ' +
                'data-kit-tooltip2-offset="0" ' +
                'data-kit-tooltip2-content="'+content+'">'+
                lib.svg.options+
                '</logger-item__options>';
        }
        return optionsTemplate;
    }

    function getRowNode(key, value, level = '', data = {
        id: 0,
        createdAt: '',
        userId: '',
    }){
        key = String(key);
        value = String(value);
        var result = '<logger-item class="kit-logger-row--'+level+'">' +
            '<logger-item__key>'+key+'</logger-item__key>' +
            '<logger-item__value>'+value+'</logger-item__value>' +
            getOptionsTemplate(data) +
            '</logger-item>';

        return support.makeNode(result);
    }

    function getLoggerLogNode(id = ''){
        var entity = '', css = '', style = '';
        return support.makeNode('<logger-log></logger-log>');
    }
    function getLoggerPairNode(depth = 1){
        return support.makeNode('<logger-pair style="--logger-depth:'+support.toNumber(depth, 1)+';"></logger-pair>');
    }

    function getLoggerAdditionalHtml(html = ''){
        return '<logger-additional>'+html+'</logger-additional>';
    }

    function getLoggerToggerHtml(id, html = ''){
        return '<label class="logger-toggler" for="'+id+'">'+html+'</label>';
    }

    function getContextNode(id = ''){
        var entity = '', css = '', style = '';
        if(id) {
            css = '<style>input#'+id+':checked ~ [data-entity-id="'+id+'"] {display: block !important;}</style>';
            entity = ' data-entity-id="'+id+'"';
            style = ' style="display: none"'
        }
        return support.makeNode('<logger-context'+entity+style+'>'+css+'</logger-context>');
    }

    /**
     * @extends Logger
     * @class LoggerVisual
     * @param nodeOrSelector
     * @param maxDepth
     * @param openDepth
     */
    class LoggerVisual extends Logger
    {
        constructor(nodeOrSelector = '', maxDepth = 3, openDepth = 1){
            super();
            /**
             * @type {Element}
             * @private
             */
            this.__htmlCollection = support.getNode(nodeOrSelector);
            if(this.__htmlCollection) {
                support.data.set(this.__htmlCollection, 'log', this);
            }
            this.__maxDepth = support.toNumber(maxDepth, 3);
            this.__openDepth = support.toNumber(openDepth, 1);

            this.onLog.push(this.__logHandler);
            this.__htmlCollectionTemplater = '<div class="kit-logger kit-logger--fixed"></div>';
        }
    }


    /**
     *
     * @return {HTMLElement}
     */
    LoggerVisual.prototype.getHtmlCollection = function() {
        if (!this.__htmlCollection) {
            /**
             * @type {HTMLElement}
             */
            this.__htmlCollection = document.getElementById('kit-logger') || document.querySelector('.kit-logger') || support.makeNode(this.__htmlCollectionTemplater);

            if((this.__htmlCollection instanceof HTMLElement) && !this.__htmlCollection.parentElement) {
                document.body.appendChild(this.__htmlCollection)
            }
            if(this.__htmlCollection) {
                support.data.set(this.__htmlCollection, 'log', this);
            }
        }

        return this.__htmlCollection;
    };

    function getPairNode(depth, key, value, logLevel, maxDepth = 6, openDepth = 1, isRoot = true, data = {}) {
        var pairNode = getLoggerPairNode(depth);
        var contextNode,
            isIterable = support.isIterable(value),
            isEmptyValue = support.empty(value),
            id = isIterable?support.uniqueId():'',
            skipValue = value === window || value === document,
            valueType = support.getType(value),
            len = isIterable?Object.keys(value).length:0,
            lenPrint = isIterable?' [' + Object.keys(value).length + ']':'',
            val = value,inp, j;

        if(support.isIterable(value)){

            if(value === window) {
                val = 'window ' + getLoggerAdditionalHtml(valueType);
            } else if(value === document) {
                val = 'document ' + getLoggerAdditionalHtml(valueType);
            } else {
                if(isRoot && support.empty(value)){
                    val = '';
                } else
                if(len){
                    if(maxDepth){
                        val = getLoggerToggerHtml(id, valueType + lenPrint)
                    } else {
                        val = getLoggerAdditionalHtml(valueType + lenPrint);
                    }
                } else {
                    val = getLoggerAdditionalHtml(valueType + ' empty');
                }
            }
        }
        if(isIterable&&maxDepth&&!skipValue) {
            pairNode.appendChild(inp = support.makeNode('<input id="'+id+'" type="checkbox" style="display: none;">'));
            inp.checked = !!openDepth;
        }

        if(support.isIterable(key)){
            pairNode.appendChild(x = getRowNode('', '' + val, logLevel));
            x.querySelector('logger-item__key').appendChild(getPairNode(depth, null, key, logLevel, 1, 1, isRoot));
        } else {
            if(!(isRoot&&support.isUndefined(key))) {
                pairNode.appendChild(getRowNode(key, '' + val, logLevel, data));
            }
        }

        if(isIterable&&maxDepth&&!skipValue) {
            if(!(isRoot&&support.isUndefined(key))) {
                ++depth;
            }
            maxDepth = Math.max(0, maxDepth - 1);
            openDepth = Math.max(0, openDepth - 1);
            pairNode.appendChild(contextNode = getContextNode(id));
            for(j in value) {
                if(value.hasOwnProperty(j)) {
                    contextNode.appendChild(getPairNode(depth, j, value[j], '', maxDepth, openDepth, false));
                }
            }
        }
        return pairNode;
    }

    LoggerVisual.prototype.__logHandler = function(data = {
        level: '',
        message: '',
        context: [],
    }){
        var logCollectionNode = this.getHtmlCollection();
        var loggerLogNode = getLoggerLogNode();
        loggerLogNode.appendChild(getPairNode(1, data.message, data.context, data.level, this.__maxDepth, this.__openDepth, true, data));
        logCollectionNode.appendChild(loggerLogNode);
        logCollectionNode.scrollTop = logCollectionNode.scrollHeight;
    };

    LoggerVisual.prototype.logList = function (logList) {
        var self = this;
        logList = new Collection(logList);
        logList.map(function (log) {
            self.__logData(log);
        });
    };

    /**
     * @var logger
     * @memberOf {kit.service}
     * @type LoggerVisual
     */
    global.service.logger = new LoggerVisual();

    /**
     * @memberOf kit
     */
    var log = {
        Logger,
        LogLevel,
        LoggerVisual
    };
    return module.exports = log;
});
