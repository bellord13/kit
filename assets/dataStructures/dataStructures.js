/** @module dataStructures */
__PluginManager__.register('dataStructures', function(require, exports, module, global){


    /**
     *
     * @param context
     * @param callback
     * @param interval
     * @constructor
     */
    function Interval(context, callback, interval = 1000){
        /** @private */
        this.context = context || window;
        /** @private */
        this.callback = callback;
        /** @private */
        this.interval = interval;

    }
    Interval.prototype.start = function () {
        /** @private */
        this.intervalId = setInterval(function () {
            this.callback.apply(this.context, arguments);
        }.bind(this), this.interval)
    };
    Interval.prototype.stop = function () {
        if(this.intervalId){
            clearInterval(this.intervalId);
        }
    };




    /**
     * Stack – LIFO
     * Queue – FIFO
     * @memberOf kit
     */
    const dataStructures = {


        /**
         * @class StackExecutionLimiter
         * @memberOf dataStructures#
         * @constructor
         */
        StackExecutionLimiter: (function () {

            var stack = function (context, callback, timeout = 1000) {
                this.counter = 0;
                this.timerId = 0;
                this.timeout = 0;
                this.arguments = null;
                this.context = null;
                this.executeBeforeTimeout = false;
                this.setContext(context);
                this.setCallback(callback);
                this.setTimeout(timeout);
            };
            stack.prototype.setCallback = function(callback){
                this.callback = callback;
            };
            stack.prototype.setContext = function(context){
                this.context = context || window;
            };
            stack.prototype.setTimeout = function(timeout){
                this.timeout = timeout;
            };
            stack.prototype.exec = function() {
                this.arguments = arguments;
                this.counter++;

                if(this.timerId === 0) {
                    if(this.executeBeforeTimeout) {
                        this.__execBeforeTimeout(this.counter);
                    } else {
                        this.__execAfterDelay();
                    }
                }
            };
            stack.prototype.__execBeforeTimeout = function (counter) {
                this.timerId = setTimeout((function(){
                    if(this.counter > counter) {
                        this.__execBeforeTimeout(this.counter);
                        return null;
                    }
                    this.timerId = 0;
                }).bind(this), this.timeout);
                this.callback.apply(this.context, this.arguments);
            };
            stack.prototype.__execAfterDelay = function () {
                this.timerId = setTimeout((function(){
                    this.callback.apply(this.context, this.arguments);
                    this.timerId = 0;
                }).bind(this), this.timeout);
            };
            stack.prototype.reset = function () {
                clearTimeout(this.timerId);
                this.timerId = 0;
                this.counter = 0;
            };
            return stack;
        })(),

        Interval,
    };

    return module.exports = dataStructures;
});
