/**
 * @link https://animista.net/
 * @module animations
 **/
__PluginManager__.register('animations',function(require, exports, module, global) {

    /**
     * @memberOf kit
     */
    var animations = {
        scaleIn: 'anim-scale-in',
        scaleOut: 'anim-scale-out',
        fadeIn: 'anim-fade-in',
        fadeInTop: 'anim-fade-in-top',
        fadeOut: 'anim-fade-out',
        fadeOutTop: 'anim-fade-out-top',
    };

    //Object.assign(animations, global.animations || {});

    return module.exports = animations;
});
