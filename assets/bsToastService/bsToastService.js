/** @module bsToastService */
__PluginManager__.register('bsToastService', function(require, exports, module, global){
    var support = require('support');
    var bsToast = require('bsToast');
    var Collection = require('Collection');


    function Service() {
        this.close = true;
        this.option = {
            animation: true,
            autohide: true,
            delay: 4000,
        };
        this.closeTemplate = '<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Закрыть"/>';
        this.iconTemplate = '<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%"/></svg>';
        /**
         * @type {HTMLElement}
         */
        this.container = null;
        this.onShow = new Collection();
    }


    Service.prototype.setContainer = function (parentNode = document.body, opClass = 'default') {
        parentNode = support.getNode(parentNode);
        if(opClass) {
            opClass = 'toast-container--'+opClass;
        }

        var opClassSelector = (opClass !== '')?('.'+opClass):'';
        var container = parentNode.querySelector('.toast-container'+opClassSelector);
        if(container) {
            this.container = container;
            return;
        }

        var toastContainerTemplate = '<div class="toast-container '+opClass+'"></div>';
        this.container = support.makeNode(toastContainerTemplate);
        if(support.isElement(parentNode)) {
            parentNode.appendChild(this.container);
        }
    };


    Service.prototype.getToastNode = function (title, message, desc = '', htmlClass = '') {
        var closeTemplate = this.close?this.closeTemplate:'';
        var toastTemplate = '<div class="toast fade hide ' + htmlClass + '" role="alert" aria-live="assertive" aria-atomic="true"><div class="toast-header"><div class="kit-icon toast-icon">'+this.iconTemplate+'</div><strong class="toast-title">'+title+'</strong><small class="toast-desc">' + desc+ '</small>'+closeTemplate+'</div><div class="toast-body">'+message+'</div></div>';
        return support.makeNode(toastTemplate);
    };

    Service.prototype.info = function (title, message, desc = '') {
        this.__show(title, message, desc, 'toast--info');
    };

    Service.prototype.success = function (title, message, desc = '') {
        this.__show(title, message, desc, 'toast--success');
    };

    Service.prototype.warning = function (title, message, desc = '') {
        this.__show(title, message, desc, 'toast--warning');
    };

    Service.prototype.error = function (title, message, desc = '') {
        this.__show(title, message, desc, 'toast--error');
    };

    Service.prototype.__show = function (title, message, desc = '', htmlClass = '') {
        var toastNode = this.getToastNode(title, message, desc, htmlClass);
        var toast = new bsToast(toastNode, this.option);
        toast.data = {
            title: title,
            message: message,
            desc: desc,
            date: new Date()
        };
        support.setNodeData(toastNode, 'toast', toast);
        this.container.appendChild(toastNode);
        toast.show();
        this.onShow.eachCall(this, toast, toastNode);
    };

    /**
     * @namespace kit.service
     * @class toast
     **/
    var toast = new Service();
    toast.setContainer();

    global.service.toast = toast;

    module.exports = toast;
});
