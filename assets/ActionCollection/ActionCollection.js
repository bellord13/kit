/** @module ActionCollection */
__PluginManager__.register('ActionCollection', function(require, exports, module, global) {

    var Collection = require('Collection');
    var support = require('support');

    /**
     * @class ActionCollection
     * @extends {Collection}
     * @memberOf {kit}
     */
    var ActionCollection = function (list = []) {
        Collection.prototype.constructor.apply(this, arguments);
    };
    var proto = ActionCollection.prototype;

    proto.run = function (count = -1) {
        this.map(function (action) {
            action.run(count);
        })
    };
    proto.stop = function () {
        this.map(function (action) {
            action.stop();
        })
    };

    support.extend(ActionCollection.prototype, Collection.prototype);

    return module.exports = ActionCollection;
});