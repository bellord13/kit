__PluginManager__.register('toolkit', function(require, exports, module, global) {

    var $ = require('jQuery');
    var support = require('support');

    var __Helper = function () {

        function Helper() {}

        /**
         * @description Вызывает событие
         * @param {object} elem
         * @param {string} eventName Название события
         */
        Helper.prototype.triggerEvent = function triggerEvent(elem, eventName) {
            var ev = document.createEvent('HTMLEvents');
            ev.initEvent(eventName, true, false);
            return elem.dispatchEvent(ev);
        };

        return Helper;
    }();
    var helper = new __Helper();


    var __Device = (function(){

        var isMobile = undefined;
        var Device = function () {};
        Device.prototype.isMobile = function () {
            if(isMobile === undefined) {
                isMobile = (function(a){return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
            }
            return isMobile;
        };

        return Device;
    })();
    var device = new __Device();

    var __Events = function Events() {
        function Events() {
            this.submitForm();
        }

        Events.prototype.submitForm = function () {
            $('form.js-kit-form').attr('novalidate', true);
            $(document).on('submit.kit-event', 'form.js-kit-form', function (e) {
                e.preventDefault();

                inputs.clearForm(e.target);
                message.clear(e.target, true);

                var formNode = $(e.target);
                var sendData = formNode.serializeArray();
                var action = formNode.attr('action');
                var additionalData = {};

                var jqxhr = $.post(action, sendData, function (response) {
                    console.log('success');
                    if (typeof response == 'object') {

                        Ajax.ajaxProcessingSuccess(response, $(e.target));

                    }
                }, 'json')
                    .done(function (response) {
                        // console.log('done');
                        // console.log(response);
                    })
                    .fail(function (response) {
                        console.log('fail');
                        console.log(response);
                    })
                    .always(function (response) {
                        // console.log('always');
                        // console.log(response);
                    });


            });
        };

        return Events;
    }();
    var events = new __Events();


    var __Message = function () {

        function Message() {
            this.selector = '.kit-message';
        }

        Message.prototype.clear = function (node, recursive = false) {
            node = $(node);
            if (!node.length) {
                return;
            }
            function clearNode(node) {
                node.removeClass('kit-message-error kit-message-success kit-message-warning')
                    .html('');
            }
            if(node.is(this.selector)) {
                clearNode(node);
            }
            if(recursive) {
                clearNode(node.find(this.selector));
            }
        };

        return Message;
    }();
    var message = new __Message();

    var __Ajax = function () {

        function Ajax() {

        }

        Ajax.prototype.ajaxProcessingSuccess = function ajaxProcessingSuccess(data, $form) {
            console.log('ajaxProcessingSuccess');
            $form = $($form);


            if (data.hasOwnProperty('inputs') && Array.isArray(data.inputs)) {
                data.inputs.forEach(function (item) {

                    var inputNode = $('[name="' + item.name + '"]');
                    if (inputNode.length) {
                        if (item.hasOwnProperty('error')) {
                            inputs.showFieldError(inputNode, item.error)
                        }
                        if (item.hasOwnProperty('readonly')) {
                            inputNode.prop('readonly', item.readonly);
                        }
                        if (item.hasOwnProperty('disabled')) {
                            inputNode.prop('disabled', item.disabled);
                        }
                        if (item.hasOwnProperty('type')) {
                            inputNode.prop('type', item.type);
                            if (item.type === 'hidden') {
                                inputNode.parent().removeClass('kit-hidden').addClass('kit-hidden');
                            } else {
                                inputNode.parent().removeClass('kit-hidden');
                            }
                        }
                    }
                });
            }
            if (data.hasOwnProperty('do') && Array.isArray(data.do)) {
                data.do.forEach(function (item) {
                    var $do = false;
                    var selector = $(document);
                    if (item.hasOwnProperty('form') && item.form === true) {
                        selector = $form;
                        if (selector.length) {
                            $do = true
                        }
                    }
                    if (item.hasOwnProperty('selector')) {
                        selector = $(item.selector);
                        if (selector.length) {
                            $do = true
                        }
                    }

                    if ($do) {
                        if (item.hasOwnProperty('html')) {
                            selector.html(item.html);
                        }
                        if (item.hasOwnProperty('replace')) {
                            selector.replaceWith(item.replace);
                        }
                        if (item.hasOwnProperty('addClass')) {
                            selector.addClass(item.addClass);
                        }
                        if (item.hasOwnProperty('removeClass')) {
                            selector.removeClass(item.removeClass);
                        }
                        if (item.hasOwnProperty('disabled')) {
                            selector.prop('disabled', item.disabled);
                        }
                        if (item.hasOwnProperty('trigger')) {
                            selector.trigger(item.trigger);
                        }
                    }


                    if (item.hasOwnProperty('hide')) {
                        item.hide && selector.find(item.hide).hide();
                    }
                    if (item.hasOwnProperty('show')) {
                        item.show && selector.find(item.show).show();
                    }
                });
            }

            if (data.hasOwnProperty('success')) {
                this.showMessageSmart(data.success, $form, 'success');
            }
            if (data.hasOwnProperty('warning')) {
                this.showMessageSmart(data.warning, $form, 'warning');
            }
            if (data.hasOwnProperty('errors')) {
                this.showMessageSmart(data.errors, $form, 'error');
            }
        };

        Ajax.prototype.showMessageSmart = function showMessageSmart(messageObject, formNode, type = 'error') {
            console.log('showMessageSmart');
            console.log(messageObject);
            console.log(formNode);
            console.log(type);

            formNode = $(formNode);
            console.log(formNode);
            var unknownMessage = [];
            var msg = [];
            var inputNode,
                key;
            if (Object.keys(messageObject).length) {

                for (key in messageObject) {
                    msg = messageObject[key];
                    console.log(msg);
                    inputNode = formNode.find("[name='" + key + "']");

                    if (inputNode.length) {
                        if (type === 'success') {
                            inputs.showFieldSuccess(inputNode, msg);
                        } else if (type === 'warning') {
                            inputs.showFieldWarning(inputNode, msg);
                        } else {
                            inputs.showFieldError(inputNode, msg);
                        }
                    } else {
                        unknownMessage[unknownMessage.length] = msg;
                    }
                }
                console.log('unknownMessage');
                console.log(unknownMessage);
                var messageNode = formNode.find('.kit-message');
                if (unknownMessage.length) {
                    console.log('1');
                    console.log(messageNode.length ? 'messageNode.length' : 'no messageNode.length');
                    messageNode.length && messageNode.html(unknownMessage.join('<br>')).addClass('kit-message-' + type);
                } else {
                    console.log(unknownMessage);
                }
            }
        };

        return Ajax;
    }();
    var Ajax = new __Ajax();


    function select() {

        if (helper.isMobile) {
            //document.body.classList.add('kit-select-fix');
        }

        document.addEventListener('click', clickHandler, false);
        document.addEventListener('change', changeHandler, false);
        document.addEventListener('keydown', escapeKeyHandler, false);
        window.addEventListener('resize', resizeHandler, false);

        var MINWIDTH = 200;
        var MAXHEIGHT = 8 * 40;
        var MINHEIGHT = 4 * 40;

        function clickHandler(e) {

            //if (_helper2.default.isMobile) return;

            var fieldDisplay = e.target;
            var container = fieldDisplay.parentNode;
            var dropdownOptionNode = e.target.closest('.kit-dropdown__option');
            /**
             * клик по .kit-field__display
             * открывает селект
             */
            if (hasClass(e.target, 'kit-field__display')) {
                e.preventDefault();

                if (!hasClass(container, 'kit-select--open')) {

                    closeAll(container);
                    addDropdown(container);
                } else {
                    closeAll(null);
                }
            }
            /**
             * клик по option
             */
            else if (hasClass(e.target, 'kit-dropdown__option') || dropdownOptionNode) {
                var wrapper, wrapper2;
                if (hasClass(e.target, 'kit-dropdown__option')) {
                    wrapper = e.target.parentNode;
                    wrapper2 = e.target;
                } else {
                    wrapper2 = dropdownOptionNode;
                    wrapper = wrapper2.parentNode;
                }

                var stamp = wrapper.getAttribute('data-for-select-id');
                var select = document.querySelector('[ data-select-id = "' + stamp + '" ]');
                var option = select.querySelectorAll('option')[wrapper2.getAttribute('data-index')];

                if (select.hasAttribute('multiple')) {
                    wrapper2.classList.toggle('kit-active');
                    option.selected = hasClass(wrapper2, 'kit-active');
                    helper.triggerEvent(select, 'change');
                    closeAll(null);
                } else {
                    option.selected = true;
                    helper.triggerEvent(select, 'change');
                    closeAll(null);
                }
            } else {
                closeAll(null);
            }
        }

        function closeAll(exceptSelect) {

            var arrayOfOpenedSelects = document.querySelectorAll('.kit-select--open');

            [].filter.call(arrayOfOpenedSelects, function (elem) {
                return elem !== exceptSelect;
            }).forEach(function (elem) {
                removeClass(elem, 'kit-select--open');
            });

            detachDropDown();
        }

        // изменение инпута
        function changeHandler(e) {
            var select = e.target || e;
            changeSelect(select);
        }


        function __dirtyTests(value, dirtyValidTestData) {
            var r = true;
            dirtyValidTestData.forEach(function(attr, i){
                if(attr.name === 'data-dirty-min') {
                    r = r && (Number(value) >= Number(attr.value))
                }
            });
            return r;
        }

        /**
         * изменение инпута
         * @param select {HTMLElement}
         */
        function changeSelect(select) {
            if (hasClass(select, 'kit-field__input') && select.tagName === 'SELECT') {
                var selected = select.querySelectorAll('option:checked')[0];
                var data = {};
                var dataAttrs = [];
                if(selected) {
                    dataAttrs.filter.call(selected.attributes, function (attr) {
                        return (/^data-/.test(attr.name)
                        );
                    });
                }

                var dirtyValidTestData = [];
                dirtyValidTestData = dirtyValidTestData.filter.call(select.attributes, function (attr) {
                    return (/^data-dirty-/.test(attr.name)
                    );
                });

                var isMultiple = select.hasAttribute('multiple');

                var parent = select.parentNode;

                var display = select.parentNode.querySelector('.kit-field__display');

                var bDirty = !!selected;
                if(isMultiple) {
                    bDirty = false;
                }
                if(!isMultiple) {
                    bDirty = bDirty && __dirtyTests(select.value, dirtyValidTestData);
                }

                display.textContent = '';

                if(bDirty) {
                    display.textContent = selected.textContent;
                }

                removeClass(parent, 'kit-select--open');

                select.setAttribute('data-dirty', bDirty?'true':'false');

                removeClass(select, 'kit-field--error');

                // for(let key in attrs){
                // 	if( attrs[key].nodeName !== 'value' && attrs[key].nodeName !== undefined){
                // 		select.setAttribute( attrs[key].nodeName, attrs[key].nodeValue )
                // 	}
                // }

                forEach(dataAttrs, function (index, attr) {
                    data[normalizeDataAttrName(attr.nodeName)] = attr.nodeValue;
                });

                select.data = data;

                setTimeout(function () {
                    helper.triggerEvent(select, 'kit-select-change');
                }, 0);
            }
        }



        function resizeHandler() {

            var dropdownWrapper = document.querySelectorAll('.kit-dropdown')[0] || null;
            var stamp = void 0;
            var select = void 0;
            var container = void 0;

            if (dropdownWrapper !== null) {

                stamp = dropdownWrapper.getAttribute('data-for-select-id');
                select = document.querySelector('[ data-select-id = "' + stamp + '" ]') || [];
                container = select.parentNode;

                dropdownWrapper.style.top = container.getBoundingClientRect().top + container.offsetHeight + window.pageYOffset + 'px';
                dropdownWrapper.style.maxHeight = calcMaxHeight(container);
                dropdownWrapper.style.left = calcPosition(container).left;
                dropdownWrapper.style.width = container.offsetWidth + 'px';
            }
        }

        function addDropdown(container) {

            var select = container.querySelector('select');
            var optionElementList = select.querySelectorAll('option');
            var dropdownWrapper = document.createElement('div');
            var stamp = Date.now();
            var isMultiple = select.hasAttribute('multiple');

            if (select.disabled) return;

            select.classList.forEach(function(value, key, listObj){
                if(value !== 'kit-field__input') {
                    dropdownWrapper.classList.add(value);
                }
            });

            select.setAttribute('data-select-id', stamp);
            dropdownWrapper.setAttribute('data-for-select-id', stamp);

            addClass(dropdownWrapper, 'kit-dropdown');

            if (isMultiple) {
                dropdownWrapper.setAttribute('data-multiple', 'true');
            }

            forEach(optionElementList, function (i, item) {

                var dropdownInner = document.createElement('div');
                var textContent = item.textContent;
                var dropdownInnerIcon = document.createElement('span');
                var dropdownInnerIconCheck = '<svg viewBox="0 0 24 24"><path d="M9.001,16.212l-4.199-4.2l-1.4,1.4l5.6,5.6l12-12l-1.4-1.4L9.001,16.212z"></path></svg>';
                var dataClass = String(item.getAttribute('data-class'));
                addClass(dropdownInnerIcon, 'kit-field__icon');
                $(dropdownInnerIcon).append(dropdownInnerIconCheck);
                dropdownInner.textContent = textContent;
                dropdownInner.setAttribute('data-index', i);

                addClass(dropdownInner, 'kit-dropdown__option');
                addClass(dropdownInner, dataClass);

                if (isMultiple) {
                    dropdownInner.appendChild(dropdownInnerIcon);
                }
                if (item.disabled) {
                    addClass(dropdownInner, 'kit-disabled');
                }
                if (item.selected) {
                    addClass(dropdownInner, 'kit-active');
                }
                dropdownWrapper.appendChild(dropdownInner);
            });

            helper.triggerEvent(select, 'beforeOpen');

            document.body.appendChild(dropdownWrapper);

            dropdownWrapper.style.position = 'absolute';
            dropdownWrapper.style.minWidth = MINWIDTH + 'px';
            dropdownWrapper.style.top = calcPosition(container).top;
            dropdownWrapper.style.left = calcPosition(container).left;
            dropdownWrapper.style.minHeight = calcMinHeight(dropdownWrapper);
            dropdownWrapper.style.maxHeight = calcMaxHeight(container);
            dropdownWrapper.style.width = container.offsetWidth + 'px';
            dropdownWrapper.style.zIndex = 20002;

            setTimeout(function () {
                addClass(dropdownWrapper, 'kit-active');
                addClass(container, 'kit-select--open');
            }, 0);
        }

        function calcPosition(container) {
            var offsetTop = getOffset(container).top;
            var offsetLeft = getOffset(container).left;

            var scrollTop = window.pageYOffset;
            var scrollLeft = window.pageXOffset;

            var left = 0;
            var top = offsetTop + container.offsetHeight + window.pageYOffset;

            if (viewport().width - MINWIDTH > offsetLeft || container.offsetWidth >= MINWIDTH) {
                left = offsetLeft + scrollLeft;
            } else {
                left = offsetLeft + scrollLeft + container.offsetWidth - MINWIDTH;
            }

            return {
                top: top + 'px',
                left: left + 'px'
            };
        }

        function calcMinHeight(el) {

            var height = el.offsetHeight;

            if (height > MINHEIGHT) {
                return MINHEIGHT + 'px';
            } else {
                return height + 'px';
            }
        }

        function calcMaxHeight(container) {

            var offsetTop = getOffset(container).top;
            var _maxHeight = viewport().height + window.pageYOffset - offsetTop - container.offsetHeight - window.pageYOffset;

            if (_maxHeight > MAXHEIGHT) {
                return MAXHEIGHT + 'px';
            } else {
                return _maxHeight + 'px';
            }
        }

        function escapeKeyHandler(e) {
            if (e.keyCode === 27) {
                closeAll(null);
            }
        }

        function detachDropDown() {
            var elements = document.querySelectorAll('.kit-dropdown');
            forEach(elements, function (i, element) {
                element.parentNode.removeChild(element);
            });
        }

        /**
         * https://toddmotto.com/ditch-the-array-foreach-call-nodelist-hack/
         */
        function forEach(array, callback, scope) {
            for (var i = 0; i < array.length; i++) {
                callback.call(scope, i, array[i]);
            }
        }

        function hasClass(el, className) {
            if (el.classList) return el.classList.contains(className);else return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
        }

        function addClass(el, className) {
            if (el.classList) return el.classList.add(className);else return el.className += ' ' + className;
        }

        function removeClass(el, className) {
            if (el.classList) return el.classList.remove(className);else return el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }

        function normalizeDataAttrName(string) {
            var _arguments = arguments;

            return string.replace(/^data-/g, '').replace(/(\-\w)/g, function () {
                return _arguments[0][1].toUpperCase();
            });
        }

        function viewport() {

            var height = void 0;
            var width = void 0;

            if (document.compatMode === 'BackCompat') {
                height = document.body.clientHeight;
                width = document.body.clientWidth;
            } else {
                height = document.documentElement.clientHeight;
                width = document.documentElement.clientWidth;
            }

            return {
                width: width,
                height: height
            };
        }

        function getOffset(el) {
            return {
                top: el.getBoundingClientRect().top,
                left: el.getBoundingClientRect().left
            };
        }

        function setLabelByNode(selectNode, html) {

            selectNode = (0, _jquery2.default)(selectNode);

            if(!selectNode.length) return;

            var select = selectNode[0];

            if (hasClass(select, 'kit-field__input') && select.tagName == 'SELECT') {



                var selected = select.querySelectorAll('option:checked')[0];
                var data = {};
                var dataAttrs = [];
                if(selected) {
                    dataAttrs.filter.call(selected.attributes, function (attr) {
                        return (/^data-/.test(attr.name)
                        );
                    });
                }

                var isMultiple = select.hasAttribute('multiple');

                var parent = select.parentNode;

                var display = select.parentNode.querySelector('.kit-field__display');
                var label = select.parentNode.querySelector('.kit-field__label');
                var labelContent = label.querySelector('.kit-field__label-content');
                if(labelContent) {
                    label = labelContent;
                }

                var arrow;

                var arrowInLabel = false;
                if(label) {
                    arrow = label.querySelector('.kit-field__arrow');
                    if(arrow) {
                        arrowInLabel = true;
                    }
                }

                if(arrowInLabel) {
                    (0, _jquery2.default)(label).html('').append(html).append(arrow);
                } else {
                    (0, _jquery2.default)(label).html('').append(html);
                }

            }
        }

        var __selectClass = function(){

        };
        __selectClass.prototype.setLabelByNode = function(select, html) {
            setLabelByNode(select, html);
        };
        __selectClass.prototype.change = function(select) {
            changeSelect(select);
        };

        return new __selectClass();
    }

    var __Inputs = function () {

        function Inputs() {

        }

        Inputs.prototype.init = function() {
            var _this = this;

            this.initFields();

            window.addEventListener('resize', function () {
                return _this.refresh();
            }, false);
            document.addEventListener('inputs.reset', function () {
                return _this.refresh();
            }, false);
            document.addEventListener('tabs.toggle', function () {
                return _this.refresh();
            }, false);
            document.addEventListener('modal.open', function () {
                return _this.refresh();
            }, false);
            document.addEventListener('input', function (e) {
                return _this.changeFiled(e);
            }, false);
            document.addEventListener('paste', function (e) {
                return _this.changeFiled(e);
            }, false);
            document.addEventListener('drop', function (e) {
                return _this.changeFiled(e);
            }, false);
            document.addEventListener('cut', function (e) {
                return _this.changeFiled(e);
            }, false);
            document.addEventListener('change', function (e) {
                return _this.switchField(e);
            }, false);
        };

        Inputs.prototype.setDirty = function setDirty(element) {
            var bDirty = false;

            if(element.placeholder.length) {
                element.setAttribute('data-dirty', 'true');
                return true;
            }

            if(element.mask) {
                var raw = element.mask.getRaw();
                bDirty = !!raw.length;
            } else {
                bDirty = !!element.value;
            }

            element.setAttribute('data-dirty', bDirty?'true':'false');
            return bDirty;
        };

        Inputs.prototype.changeFiled = function changeFiled(e) {
            var _this2 = this;
            var target = e.target || e;
            var isInput = target.tagName === 'INPUT', isTextarea = target.tagName === 'TEXTAREA';
            if (target.classList.contains('kit-field__input')) {
                if (isInput || isTextarea) {
                    this.setDirty(target);

                    var value_min = $(target).attr('value_min');
                    var value_max = $(target).attr('value_max');
                    if ((value_min !== "") && (Number(value_min) > Number(target.value))) {
                        this.showFieldError(target, 'Минимальное значение ' + value_min);
                    } else if ((value_max !== "") && (Number(value_max) < Number(target.value))) {
                        this.showFieldError(target, 'Максимальное значение ' + value_max);
                    } else {
                        this.fieldNormalize(target);
                    }

                    if (!$(target).hasClass('kit-field--error')) {
                        $(target).siblings('.kit-field__message').text('');
                    }
                    if (target.classList.contains('kit-autoheight')) {
                        this.calcAutoHeight(target);
                    }
                }
            }
        };


        Inputs.prototype.checkAllInputs = function checkAllInputs() {
            var _this2 = this;

            var inputNode = document.querySelectorAll('input.kit-field__input, textarea.kit-field__input');
            [].forEach.call(inputNode, function (item) {
                if (item.tagName === 'TEXTAREA') {
                    if (item.classList.contains('kit-autoheight')) {
                        _this2.calcAutoHeight(item);
                    }
                }
                _this2.setDirty(item);
            });
        };

        Inputs.prototype.calcAutoHeight = function calcAutoHeight(node) {
            node.style.height = node.style.minHeight = 'auto';
            node.rows = 1;
            var h, styles = getComputedStyle(node);

            var minH = support.toNumber(styles.getPropertyValue('padding-top').trim().replace('px', ''))
                + support.toNumber(styles.getPropertyValue('padding-bottom').trim().replace('px', ''))
                + support.toNumber(styles.getPropertyValue('line-height').trim().replace('px', ''));

            if (node.scrollHeight < minH) {
                h = '' + minH + 'px';
            } else {
                h = node.scrollHeight + 'px';
            }
            node.style.height = node.style.minHeight = h;
        };


        Inputs.prototype.switchField = function switchField(e) {
            var target = e.target;
            var input = target.parentNode.querySelector('.kit-field__input');
            if(!input){
                return;
            }
            if (input.type.toLowerCase() === 'checkbox') {
                input.classList.remove('kit-field--error');
            } else if (input.type.toLowerCase() === 'radio') {
                var name = input.parentNode.querySelector('.kit-field__input').name;
                var otherRadio = document.querySelectorAll('[name= ' + name + ']');
                input.classList.remove('kit-field--error');
                [].forEach.call(otherRadio, function (item) {
                    item.classList.remove('kit-field--error');
                });
            }
        };


        Inputs.prototype.clearForm = function clearForm(formNode) {
            formNode = $(formNode);
            if (formNode.length) {
                formNode.find('.kit-field--error').removeClass('kit-field--error');
                formNode.find('.kit-field__message').html('');
            }
        };

        Inputs.prototype.setTelMask = function (node = false) {
            var _this3 = this;

            if(support.isElement(node) || support.isNodeList(node)){
                node = $(node);
            } else if(node === false){
                node = $('[type=tel]:not([data-kit-mask-skip])');
            } else {
                return;
            }

            if(global.config.defaultPhoneMask){
                node.mask(global.config.defaultPhoneMask, {
                    placeholder: '_', completed: function completed() {
                        _this3.fieldNormalize(this);
                    }
                }).on('blur.mask', function (e) {
                    //_this3.checkDirty(e.currentTarget);
                });
            }
        };

        Inputs.prototype.setMask = function (node = false) {
            var _this3 = this;

            if(support.isElement(node) || support.isNodeList(node)){
                node = $(node);
            } else if(node === false){
                node = $('[data-mask]');
            } else {
                return;
            }

            node.each(function(index, item){
                item = $(item);
                var placeholder = item.attr('data-mask-placeholder');
                placeholder = placeholder?placeholder:'_';
                item.mask(item.attr('data-mask'), {
                    placeholder: placeholder,
                    completed: function completed() {
                        _this3.fieldNormalize(this);
                    }
                }).on('blur.mask', function (e) {
                    //_this3.checkDirty(e.currentTarget);
                });
            });
        };

        Inputs.prototype.unsetTelMask = function() {
            $('input[type=tel]:not([data-kit-mask-skip])').unmask();
        };

        Inputs.prototype.initFields = function() {
            this.checkAllInputs();
            this.setMask();
            this.setTelMask();
        };

        Inputs.prototype.refresh = function refresh() {
            this.unsetTelMask();
            this.initFields();
        };


        Inputs.prototype.fieldNormalize = function fieldNormalize(inputNode) {
            inputNode = $(inputNode);
            if (inputNode.length) {
                //var parentNode = inputNode.parent();
                inputNode.removeClass('kit-field--warning kit-field--success kit-field--error');
                inputNode.siblings('.kit-field__message').html('');
            }
            return inputNode;
        };
        Inputs.prototype.showFieldError = function showFieldError(inputNode, msg) {
            inputNode = this.fieldNormalize(inputNode);
            if (inputNode.length) {
                //var parentNode = inputNode.parent();
                inputNode.addClass('kit-field--error');
                inputNode.siblings('.kit-field__message').html(msg);
            }
        };
        Inputs.prototype.showFieldSuccess = function showFieldSuccess(inputNode, msg) {
            inputNode = this.fieldNormalize(inputNode);
            if (inputNode.length) {
                //var parentNode = inputNode.parent();
                inputNode.addClass('kit-field--success');
                inputNode.siblings('.kit-field__message').html(msg);
            }
        };
        Inputs.prototype.showFieldWarning = function showFieldWarning(inputNode, msg) {
            inputNode = this.fieldNormalize(inputNode);
            if (inputNode.length) {
                //var parentNode = inputNode.parent();
                inputNode.addClass('kit-field--warning');
                inputNode.siblings('.kit-field__message').html(msg);
            }
        };
        Inputs.prototype.hideFieldError = function hideFieldError(inputNode) {
            inputNode = $(inputNode);
            if (inputNode.length) {
                var parentNode = inputNode.parent();
                inputNode.removeClass('kit-field--error');
                inputNode.siblings('.kit-field__message').html('');
            }
        };


        return Inputs;
    }();
    var inputs = new __Inputs();

    kit.ready([], function(){
        inputs.init();
    });

    /**
     * @class kitToolkit
     */
    var toolkit = {
        helper: helper,
        select: select(),
        message: message,
        inputs: inputs,
        device: device,
    };


    Object.assign(module.exports, toolkit);



    return module.exports;
});
