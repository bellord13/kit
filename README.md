# wt.core assets source [![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/bellord13/kit/)
## is a dependency wt.core [![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/andreyleoua/wt.core/)


Предназначен для асинхронной работы с ресурсами на веб-проектах.

## Установка

В директории [php:wt.core] `app()->config()->get('templater.path') . '/kit'` (по умолчанию `/local/templater.dev/kit`) выполнить клонирование проекта 

```bash
git clone https://bellord@bitbucket.org/bellord13/kit.git ./
```


## Термины и определения в контексте фреймворка
`Плагин` - сущность включающая в себя модуль, js-, css-файлы, шрифты.

`Модуль` - составляющая плагина, определяемая как основной файл плагина (его наличие не обязательно в плагине)

`PluginManager` - класс для работы с плагинами

## Базовые принципы

✨ Инициализация 

Выдержка из плагина `core`:

```js
manager = new PluginManager(global, plugins = {});
manager.register('PluginManager', {
    alias: 'pm',
    type: moduleTypeList.loader,
    loader: function(module, exports, require, global) {
        // return определяет, что выкинуть в global
        return module.exports = manager;
    }
});
manager.install('PluginManager');
```
Создается экземпляр класса PluginManager и регистрирует сам себя.
Переменная `global` выполняем роль фасада для все установленных плагинов с допустимостью использования `alias`. Это позволит использовать функционал вне ядра. В качестве глобальной переменной используется `window.kit`. 

Пример вызова
```js
kit.$
kit.jquery
```

✨ Основные этапы работы с плагином:

| этап | метод |
| ------ | ------ |
| Регистрация | register(`pluginId`, `config`) или registerPlugins(`object`) |
| Загрузка | load(`pluginId`, `callback`) |
| Установка | install(`pluginId`, `callback`) |
| Запрос | require(`pluginId`, `callback`) |


## Пример конфигурации 

```js
__PluginManager__.register('menu', {
        path: kit.config.assetsPath + '/menu',
        module: '/menu.js',
        type: 'loader',
        css: [
            '/menu.css'
        ],
        deps: ['lib', 'align', 'utils']
    }
);
```

## Описание параметров конфига для плагина

| name | value | description |
| --- | --- | --- |
| type | import | Значение по умолчанию. Нативный загрузчик модулей. Файл модуля содержит ключевые фразы "export" и "export default" |
| * | loader | Файл модуля содержит функцию kit.PluginManager.register(pluginId, function(module, exports, require, global){}) |
| alias | Array string | Список ключей, которые могут быть использованы вместо pluginId, как и в require() так и в global |
| import | string | Ссылка на файл с модулем |
| loader | callback | Используется для загрузки модулей типа loader |
| deps | Array string | Зависимости, Id плагинов |
| css | Array string | Массив путей к стилям. Стили грузятся синхронно при загрузке плагина. Без событий |
| js | Array string | Массив путей к скриптам. Загрузка идет по цепочке. Для синхронной загрузки скрипты ложатся в массив. пр: [1.js, [1.1.js, 1.2.js]] |
| loadJsAfterModule | bool | default: false. Определяет порядок загрузки скриптов и модуля |
| onRegister | callback | функция выполняемая после регистрации плагина (однократно) |
| onLoad | callback | функция выполняемая после загрузки плагина (однократно) |
| onInstall | callback | функция выполняемая после установки плагина (однократно) |
| path | string | префикс пути для модуля, скриптов и стилей |
| modulePath | string | префикс пути для модуля. если не указан, то берется config.path |

## Использование

Запуск исполняемого кода после установки зависимостей:

```js
PluginManager.exec(deps, callback, argArr, context)
```

Запуск исполняемого кода после установки зависимостей и при загруженной странице:

```js
PluginManager.ready(deps, callback)
```
> `kit.config.requiredDependencies` определяет зависимости (deps) по умолчанию

## Глобальный конфиг 

Параметры определяются в плагине `config`

> Параметры допустимо определять до загрузки `PluginManager`

| name | default | description |
| --- | --- | --- |
| assetsPath | /local/templater.dev/kit/assets |  |
| requiredDependencies | ['bsCore', 'toolkit'] | дополнительно определяет список зависимостей для любого колбека вызванного функциями `exec` и `ready` |
| autoloadPluginList | ['tooltip'] | Автоматически загрузит ресурсы указанных плагинов. В случае модуля с типом `loader` код не будет исполнен сразу после загрузки. Исполнение бдует определятся функций `install` |
| debug | false | Расширит список выводимой в консоль информации  |



## Глобальные переменные (window)

переменная kit может быть задана любой, поэтому в некоторых местах корректно будет использовать универсальные глобальные переменные

| name | link | description |
| --- | --- | --- |
| `__pmGlobal__` | аналог window.kit |  |
| `__PluginManager__` |аналог window.kit.PluginManager |  | 
| `__pmConfig__` | аналог window.kit.config |  |


## z-index map

| z-index | plugin | selector |
| --- | --- | --- |
| 100 | flex-kit | .flex-card:hover .flex-card__content |
| 1000 | bs | dropdown |
| 1020 | bs | sticky |
| 1030 | bs | fixed |
| 1040 | bs | modal-backdrop |
| 1050 | bs | offcanvas |
| 1060 | bs | modal |
| 1070 | bs | popover |
| 1080 | bs | tooltip |




[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
