<?
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('wt.core');
?>
<script src="../../assets/config/config.js"></script>
<script src="../../assets/core/core.plugins.config.js"></script>
<script src="../../assets/core/core.js"></script>
<script src="script.js"></script>
<script type="application/javascript">

</script>
<style>
    .kit-logger {
        width: 100%;
        height: 100%;
        flex: 1 1 100% !important;
        --flex-padding: 0;
    }
    .kit-logger__title {
        background-color: hsl(0, 0%, 20%);
        color: #ccc;
        font-family: monospace;
        --flex-padding: 5px;
    }
    .kit-logger-item__created-at {
        display: inline-block;
        float: right;
        color: #9f9f9f !important;
        font-size: 0.9em !important;
    }
    logger-item__options > svg {
        pointer-events: none;
    }
</style>
<div class="flex-list" style="height: 100%;--col-count: 3; --gap: 0; --flex-col-count: 1">
    <div class="flex-list-item" style="height: 100%;">
        <div class="flex-col flex-line-count" style="--flex-line-count: 1;height: 100%;">
            <div class="kit-logger kit-logger--full"></div>
        </div>
    </div>
    <div class="flex-list-item" style="height: 100%;--col-span: 2;">
        <div class="flex-list flex-list--static" style="--col-count: 3;--line-count:3;height: 100%;">
        <?
        foreach (\Wt\Core\Log\LogLevel::$list as $level){
            ?>
                <script>
                    kit.ready(['flexKit', 'log', 'dataStructures', 'jQuery', 'date', 'Tooltip2'], function () {
                        window.loggerList = window.loggerList || {};
                        window.loggerList.<?=$level?> = new kit.log.LoggerVisual('.kit-logger--<?=$level?>');
                        window.loggerList.<?=$level?>.<?=$level?>(kit.date(kit.date.DATA_DB));
                    });
                </script>
                <div class="flex-list-item flex-col flex-cells-g-align--stretch" style="--flex-padding: 0;">
                    <div class="kit-logger__title"><?=$level?></div>
                    <div class="kit-logger kit-logger--<?=$level?>"></div>
                </div>
            <?
        }
        ?>
        </div>
    </div>
</div>



