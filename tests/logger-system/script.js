kit.ready(['flexKit', 'log', 'dataStructures', 'jQuery', 'date'], function () {

    window.loggerList = window.loggerList || {};
    window.loggerList.full = new kit.log.LoggerVisual('.kit-logger--full', 3, 0);
    window.loggerList.full.notice(kit.date(kit.date.DATA_DB));
    var lastId = 0;

    var interval = new kit.dataStructures.Interval(null, function () {
        kit.jQuery
            .ajax('/ajax/LoggerSystemAjax/filter?>id=' + lastId)
            .done(function(response){
                var data = response.data || {};
                var logs = new kit.Collection(data);
                logs.map(function (data) {
                    console.log(data);
                    lastId = Math.max(lastId, data['id']);
                    window.loggerList[data.level].__logData(data);
                });
                window.loggerList.full.logList(data);
            });
    }, 5000);
    interval.start();
});