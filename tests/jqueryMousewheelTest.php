<script src="../assets/config/config.js"></script>
<script src="../assets/core/core.plugins.config.js"></script>
<script src="../assets/core/core.js"></script>

<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>
<script>
    kit.ready(['jquery-mousewheel', 'testing'], function () {

        $ = kit.jQuery;

        var unit = new kit.testing.Unit();

        unit.assertSame('function', typeof $.fn.mousewheel, '$.fn.mousewheel');
        unit.assertSame('object', typeof $.event.special.mousewheel, '$.event.special.mousewheel');

    });
</script>
