<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<style  type="text/css">
    .section-product-list {
        --col-count: 5;

    }
    .section-product-list .flex-card {
        --kit-color-content: #FFF;
        --aspect-ratio: calc(200 / 250);
    }
    .section-product-list .flex-card__picture {
        flex-grow: 1;
    }
    .flex-card__hover {
        text-align: center;
        color: grey;
        font-size: 0.8em;
    }
</style>
<?

$items = array_fill(0, 40, [
    'id' => 1,
    'name' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit  Ab accusamus aliquid, consequuntur corporis ducimus',
    'hover' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit  Ab accusamus aliquid, consequuntur corporis ducimus',
    'picture' => '<img src="https://picsum.photos/200/200">',
    'text_preview' => 'asdas asdas dasdasd',
    'tickets' => [
        [
            'code' => 'news',
            'name' => 'Новинка'
        ],
        [
            'code' => 'discount',
            'name' => 'Скидка'
        ]
    ]
]);
/**
 * @todo card list
 */
?>
<div class="kit-wrapper">
    <section class="section-product-list">
        <div>
            <h1>Каталог</h1>
        </div>
        <div class="flex-list flex-card-list--list-">
            <?foreach($items as $item) {?>
                <div class="flex-list-item">
                    <?
                    echo app()->service()->templater()->get('kit:card/block', $item);
                    ?>
                </div>
            <?}?>
        </div>
    </section>
</div>