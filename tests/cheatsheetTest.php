<?php
use Bitrix\Main;

require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/prolog_before.php');?>
<?php
try {
    if (Main\Loader::includeModule('wt.core')) {
        app()->service()->assets()->setPlugins([
            'kit:config',
            'kit:flex-kit',
            'kit:core',
            'kit:bsCore',
            'kit:collapse',
            'kit:site',
            'kit:toolkit',
            'kit:theme',
        ])->render();
    }
} catch (Main\LoaderException $e) {

}
global $APPLICATION;
$APPLICATION->ShowHead();

$c = app()->config();
$c['form_field_profile_id'] = 'material';

/**
 * @param $name
 * @param $filePath
 */
function include_section ($name, $filePath){
    ?><div class="kit-section"><h2><?=$name?></h2><? include $filePath; ?></div><?
}

?>
<script>
    kit.ready(['popover', 'datepicker', 'toolkit'], function () {

    });
</script>
<div class="kit-wrapper">
    <h1 style="text-align: center;">BX.KIT FRONT-END FRAMEWORK</h1>

    <?php
    include_section('Text samples',__DIR__ . '/cheatSheet/text.php');
    include_section('Tooltip',__DIR__ . '/cheatSheet/tooltip.php');
    include_section('Popover',__DIR__ . '/cheatSheet/popover.php');
    include_section('Modals',__DIR__ . '/cheatSheet/modals.php');
    include_section('Color fields',__DIR__ . '/cheatSheet/field/color.php');
    include_section('Tel fields',__DIR__ . '/cheatSheet/field/tel.php');
    include_section('Number fields',__DIR__ . '/cheatSheet/field/number.php');
    include_section('Select fields',__DIR__ . '/cheatSheet/field/select.php');
    include_section('Checkbox fields',__DIR__ . '/cheatSheet/field/checkbox.php');
    include_section('Radio fields',__DIR__ . '/cheatSheet/field/radio.php');
    include_section('Text fields',__DIR__ . '/cheatSheet/field/text.php');
    include_section('Textarea fields',__DIR__ . '/cheatSheet/field/textarea.php');
    include_section('File fields',__DIR__ . '/cheatSheet/field/file.php');
    include_section('Buttons',__DIR__ . '/cheatSheet/button/buttons.php');
    include_section('Form',__DIR__ . '/cheatSheet/form.php');
    ?>
</div>
<?php