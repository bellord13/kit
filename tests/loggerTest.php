<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<script>
    kit.ready(['testing', 'date', 'log'], function () {
        var unit = new kit.testing.Unit();

        var logger = new kit.log.Logger();

        logger.emergency('emergency');
        logger.alert('alert');
        logger.critical('critical');
        logger.error('Привет мир!');
        logger.warning('warning');
        logger.notice('notice');
        logger.info('info');
        logger.debug('debug');
        logger.success('success');
        logger.notice(' ');
        logger.notice(null);
        logger.notice(NaN);
        logger.notice('bigData', {win: window, doc: document});
        logger.notice('context array', [100, 200]);

        logger.notice(kit.date('Y-m-d H:i:s'), {
            test1: {
                test1: kit.date('Y-m-d H:i:s'),
                test2: kit.date('Y-m-d'),
                test3: kit.date('H:i:s'),
            },
            test2: {
                test1: kit.date('Y-m-d H:i:s'),
                test2: kit.date('Y-m-d H:i:s'),
                test3: kit.date('Y-m-d H:i:s'),
            },
            test3: {
                test1: kit.date('Y-m-d H:i:s'),
                test2: kit.date('Y-m-d H:i:s'),
                test3: kit.date('Y-m-d H:i:s'),
            },
        });

        logger.notice(kit.date('Y-m-d H:i:s'), {
            test1: {
                test2: {
                    test3: {
                        test4: {
                            test5: {
                                test6: {
                                    test7: {
                                        test1: kit.date('Y-m-d H:i:s'),
                                        test2: kit.date('Y-m-d H:i:s'),
                                        test3: kit.date('Y-m-d H:i:s'),
                                    },
                                },
                            },
                        },
                    },
                },
            },
        });

        kit.service.logger.logList(logger.collection);

        unit.assertSame(16, kit.service.logger.collection.length, 'Проверка количества логов в логерСервисе после подгрузки логов из класса Logger через loggerService.logList');

    });
</script>

<div  class="kit-wrapper">

</div>
<?
?>