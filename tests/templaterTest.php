<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();
?>
<div class="kit-wrapper">
    <div class="flex-list" style="--col-count: 3">
        <div class="flex-list-item"><?
            service()->templater()->render('kit:input', [
                'TYPE' => 'text',
                'HINT' => '',
                'ID' => '',
                'VALUE' => '"',
                'LABEL' => 'value = дв ковычки',
            ]); ?>
        </div>
        <div class="flex-list-item"><?
            service()->templater()->render('kit:input', [
                'TYPE' => 'text',
                'HINT' => '',
                'ID' => '',
                'VALUE' => '\'',
                'LABEL' => 'value = одинарные ковычки',
            ]); ?>
        </div>
        <div class="flex-list-item"><?
            service()->templater()->render('kit:input', [
                'TYPE' => 'text',
                'HINT' => '',
                'ID' => '',
                'VALUE' => '\'~!@#$%^&*()_+<>?\'" <i>i</i>',
                'LABEL' => 'value = спецсимволы',
            ]); ?>
        </div>
        <div class="flex-list-item"><?
            service()->templater()->render('kit:input', [
                'TYPE' => 'text',
                'HINT' => '',
                'ID' => '',
                'VALUE' => '\\/<>"\'',
                'LABEL' => 'value = лев пр слеши, лев пр стрелки, дв од кав',
            ]); ?>
        </div>
        <div class="flex-list-item"><?
            service()->templater()->render('kit:input', [
                'TYPE' => 'text',
                'HINT' => '',
                'ID' => '',
                'VALUE' => '&quot;&amp;',
                'LABEL' => 'value = &quot',
            ]); ?>
        </div>
    </div>
</div>