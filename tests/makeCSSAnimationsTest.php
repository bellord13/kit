<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<script>
    kit.ready(['makeCSSAnimations', 'animations'], __handler = function () {

        kit.makeCSSAnimations({
            container: kit.jQuery('.test-object'),
            animationClass: kit.animations.scaleOut,
            stateClass: 'active',
            direction: "fwd",
            before: function () {
                //pre('makeCSSAnimations fwd before', (new Date).toString());
            },
            after: function () {
                //pre('makeCSSAnimations fwd after', (new Date).toString());
                kit.makeCSSAnimations({
                    container: kit.jQuery('.test-object'),
                    animationClass: kit.animations.scaleIn,
                    stateClass: 'active',
                    direction: "back",
                    before: function () {
                        //pre('makeCSSAnimations back before', (new Date).toString());
                    },
                    after: function () {
                        //pre('makeCSSAnimations back after', (new Date).toString());
                        __handler();
                    },
                });
            },
        });
    });
    kit.ready(['makeCSSAnimations', 'animations'], __handler2 = function () {

        kit.makeCSSAnimations({
            container: kit.jQuery('.test-object2'),
            animationClass: kit.animations.scaleIn,
            stateClass: 'active',
            direction: "fwd",
            before: function () {
                //pre('makeCSSAnimations fwd before', (new Date).toString());
            },
            after: function () {
                //pre('makeCSSAnimations fwd after', (new Date).toString());
                kit.makeCSSAnimations({
                    container: kit.jQuery('.test-object2'),
                    animationClass: kit.animations.scaleOut,
                    stateClass: 'active',
                    direction: "back",
                    before: function () {
                        //pre('makeCSSAnimations back before', (new Date).toString());
                    },
                    after: function () {
                        //pre('makeCSSAnimations back after', (new Date).toString());
                        __handler2();
                    },
                });
            },
        });
    });
</script>

<div  class="kit-wrapper">
    <div class="test-wrapper" style="padding: 100px;--animation-duration:3s;">
        <div class="test-ob test-object" style="background-color: green; width: 50px; height: 50px;border-radius: 50%;"></div>
        <div class="test-ob test-object2 active" style="background-color: green; width: 50px; height: 50px;border-radius: 50%;"></div>
    </div>
    <style>
        .test-ob {
            visibility: hidden;
        }
        .test-ob.active {
            visibility: visible;
        }
    </style>
</div>
<?
?>