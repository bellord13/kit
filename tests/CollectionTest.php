<script src="../assets/config/config.js"></script>
<script src="../assets/core/core.plugins.config.js"></script>
<script src="../assets/core/core.js"></script>

<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>

<script>

    o = {
        1: 10,
        2: 20,
        keyAs: 'vAs'
    };
    a = [10,20,30];
    a['as'] = 100;

    (function () {
        window.args = arguments;
    })(1, 2, 3, 'as');


kit.ready(['collection', 'testing', 'flexKit'], function () {


    var unit = new kit.testing.Unit();
    unit.logger.info('Коллекции by php collection: Array ( [10] => 10 [1] => 1 )');
    var assertSame = unit.assertSame.bind(unit);


    var arr = [];
    arr[10] = 100;
    var collection = new kit.Collection(arr);
    assertSame(1, collection.count(), 'type array: Количество');
    assertSame(true, collection.has(10), 'type array: has (int) 10');
    assertSame(true, collection.has('10'), 'type array: has 10');
    assertSame(false, collection.has('5'), 'type array: has 5');
    assertSame(2, collection.get(3, 2), 'type array: get');
    assertSame(100, collection.get(10, 2), 'type array: get');
    assertSame(undefined, collection.get(11), 'type array: get');


    var ob = {
        a: 1,
        b: 2,
        10: 3,
    };

    collection = new kit.Collection(ob);
    assertSame(3, collection.count(), 'Количество');
    assertSame(true, collection.has(10), 'has');
    assertSame(true, collection.has('10'), 'has');
    assertSame(false, collection.has('5'), 'has');
    assertSame(1, collection.get('a'), 'get');
    assertSame(2, collection.get(3, 2), 'get');
    assertSame(3, collection.get(10, 2), 'get');
    assertSame(undefined, collection.get(11), 'get');
    assertSame(JSON.stringify({
        a: 1,
        b: 2,
        10: 3,
        5: 19,
    }), JSON.stringify(collection.set(5, 19).all()), 'get all');


    var testFunction = function (q, w, e, r, t, y) {
        var c = new kit.Collection(arguments);
        assertSame(8, c.count(), 'arguments count');
        assertSame(1, c.get(0), 'arguments get');
        assertSame(8, c.get(7), 'arguments get');
        assertSame(80, c.get(8, 80), 'arguments get');
    }
    testFunction(1, 2, 3, 4, 5, 6, 7, 8);




    collection = new kit.Collection([1, 2, 3, '', 0, NaN, undefined, null, false, true]);
    assertSame(4, collection.filter().count(), 'filter');

    collection = new kit.Collection([4, 5, 6]);
    assertSame(true, collection.every(function (item) {
        return item > 2;
    }), 'every');
    assertSame(false, collection.every(function (item) {
        return item > 5;
    }), 'every');
    assertSame(false, collection.every(function (item) {
        return item < 1;
    }), 'every');


    collection = new kit.Collection([10, 20, 30]);
    assertSame(10, collection.first(), 'first');
    assertSame(20, collection.first(function (item) {
        return item > 15;
    }), 'first');
    assertSame(undefined, collection.first(function (item) {
        return item < 0;
    }), 'first');

    assertSame(30, collection.last(), 'first');
    assertSame(20, collection.last(function (item) {
        return item < 25;
    }), 'first');
    assertSame(undefined, collection.last(function (item) {
        return item < 0;
    }), 'first');

    assertSame(3, collection.keys().count(), 'keys');

    var xOb = {1: 1, 100: 100};
    var x = new kit.Collection(xOb);
    x.push(200);
    assertSame(200, x.last(), 'object push last');
    x.push(0);
    assertSame(0, x.last(), 'object push last');
    x.push(undefined);
    assertSame(undefined, x.last(), 'object push last');
    x.push('window');
    assertSame('window', x.last(), 'object push last');
    assertSame('window', x.get(x.lastKey()), 'get lastKey');
    assertSame(6, x.length, 'length');

    x = new kit.Collection([1, 100]);
    x.push(200);
    assertSame(200, x.last(), 'array push last');
    x.push(0);
    assertSame(0, x.last(), 'array push last');
    assertSame(4, x.length, 'length');

    x = new kit.Collection({1: 1, 100: 100});
    x.push(10);
    x.set('as', 13);
    assertSame(4, x.length, 'length');
    x.forget('as');
    assertSame(3, x.length, 'length after forget');
    assertSame(33, x.get('as', 33), 'get after forget');

    xOb = {1: 10, 100: 101};
    var x = new kit.Collection(xOb);
    assertSame(101, x.pull(100), 'pull');
    assertSame(1, x.length, 'length after pull');

    xOb = {1: 10, 100: 101};
    x = new kit.Collection(xOb);
    assertSame(101, x.pop(), 'pop');
    assertSame(1, x.length, 'length after pop');

    xOb = {1: 10, 100: 101};
    x = new kit.Collection(xOb);
    assertSame(10, x.shift(), 'shift');
    assertSame(1, x.length, 'length after shift');

    xOb = {a: 'a10', b: 'b11'};
    x = new kit.Collection(xOb);
    x.set(1, 10);
    assertSame('a10', x.shift(), 'shift +');
    assertSame(2, x.length, 'length after shift');
    assertSame(10, x.last(), 'last after shift');

    xOb = {a: 'a10', b: 'b11'};
    x = new kit.Collection(xOb);
    x2 = x.map(function (value, key) {
        return value + '!';
    });
    assertSame('a10!', x2.get('a'), 'map object');


    xOb = [10, 20, 30];
    x = new kit.Collection(xOb);
    x2 = x.map(function (value, key) {
        return value * 2;
    });
    assertSame(60, x2.last(), 'map array');

    xOb = {a: 'a10', b: 'b11', 10: 101};
    x = new kit.Collection(xOb);
    x.set(11, 111);
    x2 = x.except([10]);
    assertSame(111, x2.last(), 'except last+');
    assertSame(3, x2.length, 'except length');
    x3 = x2.except('11');
    assertSame('b11', x3.last(), 'except last+');
    assertSame(2, x3.length, 'except length');


    xOb = {a: 'a10', b: 'b11', 10: 101};
    x = new kit.Collection(xOb);
    x.set(11, 111);
    x2 = x.only(['a', 'b', '11']);
    assertSame(111, x2.last(), 'only last+');
    assertSame(3, x2.length, 'only length');
    x3 = x2.only('11');
    assertSame(111, x3.last(), 'only last+');
    assertSame(1, x3.length, 'only length');


    xOb = {a: 'a10', b: 'b11', 10: 101};
    x = new kit.Collection(xOb);
    x.set(11, 111);
    assertSame(3, x.indexKey(11), 'indexKey (int)');
    assertSame(3, x.indexKey('11'), 'indexKey (string)');
    assertSame(-1, x.indexKey('111'), 'indexKey (-1)');

    xOb = [
        {
            name: 'Igor',
            age: 20,
        },
        {
            name: 'Max',
            age: 19,
        },
        {
            name: 'Nikita',
            age: 18,
        },
    ];
    x = new kit.Collection(xOb);
    y = x.pluck('name');
    assertSame('Igor-Max-Nikita', y.toArray().join('-'), 'pluck');
    y = x.pluck('age');
    assertSame('20-19-18', y.toArray().join('-'), 'pluck с проверкой сохранения порядка');


    c = new kit.Collection(['start', 'end']);
    assertSame(true, c.includes('end'), 'includes');
    assertSame(true, c.includes('start'), 'includes');
    assertSame(false, c.includes('center'), 'includes');

});

</script>

