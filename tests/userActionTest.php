<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<script>
    kit.ready(['userAction', 'animations'], function () {
        var m1 = new kit.userAction.Popover('#object1', '#object2');
        m1.run();
        var m2 = new kit.userAction.Popover('#object2', '#object3');
        m2.run();

        var m3 = new kit.userAction.Hint('#object4', '#object5');
        m3.run();
        m3.viewer.animShowClass = kit.animations.scaleIn;
        var m4 = new kit.userAction.Hint('#object5', '#object6');
        m4.run();


        var m5 = new kit.userAction.Hint('#object7', '#object8');
        m5.viewer.animShowClass = kit.animations.scaleIn;
        m5.viewer.animHideClass = kit.animations.scaleOut;
        m5.run();
        // var m6 = new kit.userAction.Hint();
        var m6 = new kit.userAction.Hint('#object8', '#object9');
        m6.run();
        m6.viewer.animShowClass = kit.animations.scaleIn;
        m6.viewer.animHideClass = kit.animations.scaleOut;
        m6.run();

        var m7 = new kit.userAction.Hint();
        try {
            m7.run();
        } catch (e) {
            console.log(e);
        }
        try {
            m7.stop();
        } catch (e) {
            console.log(e);
        }
    });
</script>
    <style>
        .hide {
            display: none;
        }
    </style>
<div class="kit-wrapper">

<div style="padding: 40px;">
    <div id="object1" style="width: 300px; height: 50px; background-color: #eaeaea; border: 1px solid;">1</div>

    <div id="object2" style="width: 300px; height: 50px; background-color: #3860ff; border: 1px solid; display: none" class="hide">
        2
        <div id="object3" style="width: 300px; height: 50px; background-color: #ff1d34; border: 1px solid; display: none" class="hide">3</div>
    </div>
</div>
<div style="padding: 40px;">
    <div id="object4" style="width: 300px; height: 50px; background-color: #eaeaea; border: 1px solid;">4</div>
    <div id="object5" style="width: 300px; height: 50px; background-color: #3860ff; border: 1px solid; display: none" class="hide">5</div>
    <div id="object6" style="width: 300px; height: 50px; background-color: #ff1d34; border: 1px solid; display: none" class="hide">6</div>
</div>

<div style="padding: 40px;" id="box3">
    <div id="object7" style="width: 300px; height: 50px; background-color: #eaeaea; border: 1px solid;">
        <div>7</div>
        <div id="object8" style="width: 300px; height: 50px; background-color: #3860ff; border: 1px solid; display: none" class="hide">
            <div>8</div>
            <div id="object9" style="width: 300px; height: 50px; background-color: #ff1d34; border: 1px solid; display: none" class="hide">9</div>
        </div>
    </div>
</div>


</div>
<?
?>