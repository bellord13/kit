<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<script>
    kit.ready(['bsToastService', 'testing', 'date', ], function () {

        var support = kit.support;

        var unit = new kit.testing.Unit();
        unit.logger.info('Тест bootstrap.Toast 5.1.3');
        unit.logger.info('Тест service.toast');
        unit.logger.notice(' ');

        kit.service.toast.onShow.add('test', function(toast, node){
            unit.assertSame('title', toast.data.title, 'Проверка toast.data.title в событии onShow');
            unit.assertSame('message', toast.data.message, 'Проверка toast.data.message в событии onShow');
            unit.assertSame('desc', toast.data.desc, 'Проверка toast.data.desc в событии onShow');
            unit.assertSame(node, toast._element, 'Проверка node в событии onShow');
            unit.assertSame(support.getNodeData(node, 'toast'), toast, 'Проверка node.kit.toast в событии onShow');
        });
        kit.service.toast.info('title', 'message', 'desc');
        kit.service.toast.onShow.forget('test');


        var audio = new Audio('https://issue.megapolis-it.ru/local/media/new_message_tone.mp3');
        kit.service.toast.onShow.push(function (toast, node) {
            unit.logger.info(toast.data.title, toast.data);
            //audio.play();
        });

        kit.service.toast.option.delay = 18000;
        var intervalId = setInterval(function(){
            kit.service.toast.info('Новая заявка', 'Заявка от Пупкина на дальний склад. Очень срочно! Нужно еще вчера...', kit.date('H:i:s'));
            //stop();
        }, 5000);

        var stop = function(){
            clearInterval(intervalId);
        };
    });
</script>

<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>