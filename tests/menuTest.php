<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();
?>
<div style="width: 2000px; height: 1000px"></div>
<style  type="text/css">
    .chat-message {
        border: 1px solid;
    }
    .chat-message--manager-item {
        min-width: 55px;
        align-self: flex-start;
        background: aliceblue;
    }
    .chat-message--manager-item-menu {
        cursor: pointer;
    }

</style>

<body>
    <div class="kit-wrapper" style="background: beige; --width-wrapper: 800px;">
        <div class="flex-col">



            <div class="chat-message      flex-line flex-box-fill flex-aspect-ratio" id="chat-message-13">
                <div class="chat-message--manager-item">
                    <div>13</div>
                </div>
                <div class="flex-cell-aspect-auto">
                    <div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit, tempora veritatis voluptas. Architecto corporis dicta minus necessitatibus similique voluptates?
                    </div>
                </div>
                <div class="chat-message--manager-item">
                    <div class="chat-message--manager-item-menu">|||</div>
                </div>
            </div>



            <div class="chat-message      flex-line flex-box-fill flex-aspect-ratio" id="chat-message-14">
                <div class="chat-message--manager-item">
                    <div>14</div>
                </div>
                <div class="flex-cell-aspect-auto">
                    <div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit, tempora veritatis voluptas. Architecto corporis dicta minus necessitatibus similique voluptates?
                    </div>
                </div>
                <div class="chat-message--manager-item">
                    <div>Q</div>
                </div>
                <div class="chat-message--manager-item">
                    <div class="chat-message--manager-item-menu">|||</div>
                </div>
            </div>




            <div class="chat-message      flex-line flex-box-fill flex-aspect-ratio" id="chat-message-15">
                <div class="chat-message--manager-item">
                    <div>15</div>
                </div>
                <div class="chat-message--manager-item">
                    <div id="change-context-menu-r" style="font-size: 40px">R</div>
                </div>
                <div class="flex-cell-aspect-auto">
                    <div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit, tempora veritatis voluptas. Architecto corporis dicta minus necessitatibus similique voluptates?
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit, tempora veritatis voluptas. Architecto corporis dicta minus necessitatibus similique voluptates?
                    </div>
                </div>
                <div class="chat-message--manager-item">
                    <div>Q</div>
                </div>
                <div class="chat-message--manager-item">
                    <div id="change-context-menu-x" class="chat-message--manager-item-menu"><span style="
                        font-size: 10px;
                        text-align: center;
                        color: red;
                        font-weight: bold;
                        ">click me</span></div>
                </div>
            </div>





        </div>
        <div class="kit-logger"></div>
    </div>

<script>
kit.ready(['menu', 'animations'], function () {

    console.log(new kit.align.metric.Rect(window));

    console.log('menu init');
    /**
     *
     * тут управление
     *
     * схема: menu - item
     *
     * возможность создавать меню через классы и через options
     *
     * для итема можно использовать слудующие колбеки:
     * - click
     * - dblclick
     * аргументы:
     * event - текущее событие,
     * menuItem - объект итема,
     * menu - объект меню в котором лежит итем
     *
     *
     *
     *
     */
    window.i = 0;
    window.otherMenu = function() {
        window.i++;
        if(window.i > 20) {
            return null;
        }
        var menu;
        menu = kit.menu.getInstance({
            alignOffset: [0, 5],
            eventType: 'popover',
        }, [
            {
                label: 'еще (' + window.i + ')',
                click: function(e, menuItem) {console.log('клик by ' + menuItem.options.label);},
                child: otherMenu()
            }
        ]);
        menu.addItem({
            label: '<b>деф</b> колбек',
            href: window.location.href
        });

        //menu.init();
        return menu;
    };



    window.menuBuilder = (function(){
        console.log('menuBuilder');

        var menuBase = new kit.menu.Class();
        window.menuBase = menuBase;

        menuBase.setOptions({
            animation: kit.animations.scaleIn,
            alignOffset: [5, 5],
            hideDelay: 1000,
            parent: '#chat-message-15 .chat-message--manager-item-menu',
            /**
             * доступно 2 режима popover / hint,
             * понимать так: данное меню появляется по Клику на родитя (по дефолту hover)
             */
            eventType: 'popover',
        });

        /**
         * тут построение дерева через options
         */
        menuBase.addItem({
            label: 'create by options - child: [{}, {}]',
            click: function(e, menuItem, menu) {console.log('клик by ' + menuItem.options.label);},
            child: [
                {
                    label: 'example',
                    child: [
                        {
                            label: 'LVL 3',
                            click: function(e, menuItem, menu) {console.log('клик by ' + menuItem.options.label);},
                        },
                        {
                            label: 'LVL 3 (+)',
                            click: function(e, menuItem, menu) {console.log('клик by ' + menuItem.options.label);},
                        }
                    ],
                },
                {
                    label: 'Малява',
                    click: function(e, menuItem) {console.log('клик by ' + menuItem.options.label);},
                }
            ],
        });

        /**
         * тут в child отдаем класс Menu
         */
        menuBase.addItem({
            label: 'открыть по клику',
            click: function(e, menuItem) {console.log('клик by ' + menuItem.options.label);},
            child: otherMenu(),
        });


        menuBase.addItem({
            label: 'Игнор',
            click: function(e, menuItem) {console.log('клик by ' + menuItem.options.label);},
        });

        /**
         * пример
         * по клику на себя инкримент (инк лежит в самом классе итема)
         */
        menuBase.addItem({
            label: 'change label',
            sort: 5,
            click: function(e, menuItem, menu) {
                console.log('клик by ' + menuItem.options.label);
                !menuItem.customToggle && (menuItem.customToggle = 0);
                menuItem.setLabel('change label (' + (++menuItem.customToggle) + ')');
            },
        });
        /**
         * пример
         * добавить на лету итем с колбеком по клику удаляющий себя
         */
        window.i = 0;
        menuBase.addItem({
            label: 'Добавить сюда меню',
            /**
             * @param e {MouseEvent}
             * @param menuItem {kit.menu.MenuItem}
             * @param menu {kit.menu.Class}
             * */
            click: function(e, menuItem,  menu) {
                console.log('клик by ' + menuItem.options.label);
                console.log(menu);
                menu.addItem({
                    label: '--- remove me (' + (++window.i) + ') ---',
                    sort: 4000,
                    click: function(e, menuItem, menu) {
                        console.log('клик by ' + menuItem.options.label);
                        menuItem.remove();
                    },
                });
                menu.init();
            },
        });
        /**
         * пример
         * переселяем меню к другому родителю
         */
            // var item1 = new menuBase.MenuItem(menuBase.id);
            // item1.setLabel()
            // var item2 = new menuBase.MenuItem(menuBase.id);
        var item1 = menuBase.addItem({

                label: 'меняем предка на R',
                /**
                 *
                 * @param e
                 * @param menuItem
                 * @param {kit.menu.Class} menu
                 */
                click: function(e, menuItem, menu) {
                    console.log('клик by ' + menuItem.options.label);
                    menuItem.setVisible(false);
                    var tMenu = menu.getItemById(item2.getId());

                    if(tMenu) {
                        tMenu.setVisible(true);
                    }
                    menu.setParent('#change-context-menu-r');
                },
            });
        var item2 = menuBase.addItem({
            label: 'меняем предка на X',
            visible: false,
            click: function(e, menuItem, menu) {
                console.log('клик by ' + menuItem.options.label);
                menuItem.setVisible(false);
                var tMenu = menu.getItemById(item1.getId());
                if(tMenu) {
                    tMenu.setVisible(true);
                }
                menu.setParent('#change-context-menu-x');
            },
        });

        menuBase.init();

    });


    menuBuilder();
})

kit.ready(['testing'], function () {
    kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');kit.service.logger.notice('adf');
});
</script>

</body>