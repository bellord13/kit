<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

//$templatePath =  app()->service()->templater()->getComponent('kit')->getTemplatePath();
$templatePath =  app()->service()->templater()->getPath() . '/kit/templates';
$templateSvgPath =  $templatePath . '/svg';


function scanTemplates($path, $basePath, $recursive = false){

    $tree = glob($path . '/*.php');
    ?><div class="flex-list"><?
    foreach ((array)$tree as $t) {
        $templateName = ltrim(substr($t, strlen($basePath), strlen($t) - strlen($basePath) - 4), "/");
        echo '<div class="flex-list-item">' . app()->service()->templater()->get('kit:card/block', [
                'name' => $templateName,
                'picture' => '<div class="kit-picture">'.app()->service()->templater()->get('kit:'.$templateName).'</div>',
                'header' => filesize($t),
            ]) . '</div>';
    }
    ?></div><?

    if($recursive) {
        $treeDir = glob($path . '/*', GLOB_ONLYDIR);
        foreach ($treeDir as $dir) {
            scanTemplates($dir, $basePath, true);
        }
    }
}

?>
<div class="kit-wrapper">
    <?
    scanTemplates($templateSvgPath, $templatePath, true);
    ?>
</div>
<style>
    .flex-card__title {
        --line-clamp: 3;
    }
    .flex-list {
        --col-count: 10;
    }
    .flex-card__title {
        font-size: 0.7em;
        text-align: center;
    }
    .flex-card__header {
        font-size: 0.6em;
    }
</style>