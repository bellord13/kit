<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<script>
    kit.ready(['date', 'testing'], function () {

        var nowClock = document.getElementsByClassName('date-time');


        var unit = new kit.testing.Unit();
        unit.logger.info(kit.date.version);
        unit.logger.notice('<div class="date-time">--:--</div>');
        unit.assertSame('2:00:02', kit.date.countTime('H:i:s', (3600 * 2 + 2) * 1000, 0, 1), 'countTime');

        x = setInterval(function() {
            for(let i=0; i<nowClock.length; i+=1){
                nowClock[i].innerHTML=kit.date(kit.date.DATA_DB);
            }
            //clearInterval(x);
        }, 1000);

    });
</script>

<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>
<?
?>