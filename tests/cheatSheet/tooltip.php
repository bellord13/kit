<div class="flex-line-wrap flex-col-count" style="--flex-col-count: 5">
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Tooltip on top',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'tooltip',
                'data-bs-placement' => 'top',
                'title' => 'Tooltip on top'
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Tooltip on end',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'tooltip',
                'data-bs-placement' => 'right',
                'title' => 'Tooltip on end'
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Tooltip on bottom',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'tooltip',
                'data-bs-placement' => 'bottom',
                'title' => 'Tooltip on bottom'
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Tooltip on start',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'tooltip',
                'data-bs-placement' => 'left',
                'title' => 'Tooltip on start'
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Tooltip with HTML',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'tooltip',
                'data-bs-html' => 'true',
                'title' => '<em>Tooltip</em> <u>with</u> <b>HTML</b>'
            ]
        ]);
        ?>
    </div>
</div>