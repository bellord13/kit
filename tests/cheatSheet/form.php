<?php

$formService = app()->service()->form();

$loginField = $formService->field()->text();
$loginField->setLabel('Name');
$loginField->setName('login');
$loginField->setAutocomplete('on');

$passField = $formService->field()->password();
$passField->setLabel('Password');
$passField->setName('password');
$passField->setAutocomplete('on');

$submit = $formService->field()->submit();
$submit->setValue('Send');
global $APPLICATION;
$form = $formService->make([
    'name' => 'form_auth',
    'method' => 'post',
    'target' => 'auth_frame',
    'autocomplete' => 'on',
    'action' => $APPLICATION->GetCurPage(),
]);
$form->addFields([
    $loginField,
    $passField,
    $submit
]);
$form->render();