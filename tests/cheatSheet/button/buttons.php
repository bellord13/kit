<div class="flex-list" style="--col-count: 7;">
    <div class="flex-list-item"></div>
    <div class="flex-list-item">bordered</div>
    <div class="flex-list-item">bordered radius</div>
    <div class="flex-list-item">bordered round</div>
    <div class="flex-list-item">material</div>
    <div class="flex-list-item">material radius</div>
    <div class="flex-list-item">material round</div>
    <?
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'test ',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'default',
        'CLASS' => 'kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';



    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'test ',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'default',
        'CLASS' => 'kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';
    echo '<div class="flex-list-item">';



    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'primary',
        'CLASS' => 'kit-button--primary kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';





    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--primary kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';





    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'secondary',
        'CLASS' => 'kit-button--secondary kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';





    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--secondary kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';





    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'error',
        'CLASS' => ' kit-button--error kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';




    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--error kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';






    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'warning',
        'CLASS' => ' kit-button--warning kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';




    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => ' kit-button--warning kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';








    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'LABEL' => 'success',
        'CLASS' => ' kit-button--success kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';


    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success kit-button--bordered',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success kit-button--bordered kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success kit-button--bordered kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success kit-button--material',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success kit-button--material kit-button--radius',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';echo '<div class="flex-list-item">';
    service()->templater()->render('kit:button', [
        'DISABLED' => true,
        'LABEL' => 'disabled',
        'CLASS' => 'kit-button--success kit-button--material kit-button--round',
        'ICON' => service()->templater()->get('kit:svg/star'),
    ]);
    echo '</div>';

    /** ICON */

    if(1){

        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon test ',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';



        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon test ',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'default',
            'CLASS' => 'kit-button--icon kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
        echo '<div class="flex-list-item">';



        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'primary',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';





        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--primary kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';





        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'secondary',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';





        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--secondary kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';





        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'error',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';




        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--error kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';






        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'warning',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';




        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon  kit-button--warning kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';








        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'LABEL' => 'success',
            'CLASS' => 'kit-button--icon  kit-button--success kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';


        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success kit-button--bordered',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success kit-button--bordered kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success kit-button--bordered kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success kit-button--material',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success kit-button--material kit-button--radius',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';echo '<div class="flex-list-item">';
        service()->templater()->render('kit:button', [
            'DISABLED' => true,
            'LABEL' => 'disabled',
            'CLASS' => 'kit-button--icon kit-button--success kit-button--material kit-button--round',
            'ICON' => service()->templater()->get('kit:svg/star'),
        ]);
        echo '</div>';
    }

    ?>
</div>