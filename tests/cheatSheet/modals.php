<?

$closeButtonTemplate = service()->templater()->getTemplate('kit:button', [
    'LABEL' => 'Close',
    'CLASS' => ' ',
    'ATTR' => [
        'data-bs-dismiss' => 'modal',
    ],
]);
$endButtonTemplate = service()->templater()->getTemplate('kit:button', [
    'LABEL' => 'End',
    'CLASS' => ' ',
    'ATTR' => [
        'data-bs-dismiss' => 'modal',
    ],
]);
?>
<div class="d-flex justify-content-between flex-wrap">
    <?
    service()->templater()->render('kit:button', [
        'LABEL' => 'Vertically centered scrollable modal',
        'CLASS' => 'kit-button--primary kit-button--bordered',
        'ATTR' => [
            'data-bs-toggle' => 'modal',
            'data-bs-target' => '#exampleModalCenteredScrollable',
        ],
    ]);
    ?>
</div>
<?
service()->templater()->render('kit:modal', [
    'ID' => 'exampleModalCenteredScrollable',
    'TITLE' => 'Modal title',
    'BODY' => '<p>This is some placeholder content to show the scrolling behavior for modals.</p>'
                    .str_repeat('<br>', 50)
                    .'<p>This content should appear at the bottom after you scroll.</p>',
    'FOOTER' => $closeButtonTemplate,
]);
?>

    <br>

<div class="d-flex justify-content-between flex-wrap">
    <?
    service()->templater()->render('kit:button', [
        'LABEL' => 'Start Dialogue chain',
        'CLASS' => 'kit-button--primary kit-button--bordered',
        'ATTR' => [
            'data-bs-toggle' => 'modal',
            'data-bs-target' => '#modalStep1',
        ],
    ]);
    ?>
</div>
<?
$nextButtonTemplate = function($selector){
    return service()->templater()->getTemplate('kit:button', [
        'LABEL' => 'Next',
        'CLASS' => '',
        'ATTR' => [
            'data-bs-dismiss' => 'modal',
            'data-bs-target' => $selector,
            'data-bs-toggle' => 'modal',
        ],
    ]);
};
$prevButtonTemplate = function($selector){
    return service()->templater()->getTemplate('kit:button', [
        'LABEL' => 'Prev',
        'CLASS' => ' ',
        'ATTR' => [
            'data-bs-dismiss' => 'modal',
            'data-bs-target' => $selector,
            'data-bs-toggle' => 'modal',
        ],
    ]);
};
service()->templater()->render('kit:modal', [
    'ID' => 'modalStep1',
    'TITLE' => 'Step 1',
    'CLASS' => 'modal--dialogue-chain',
    'BODY' => '<p></p>',
    'FOOTER' => $nextButtonTemplate('#modalStep2'),
]);
service()->templater()->render('kit:modal', [
    'ID' => 'modalStep2',
    'TITLE' => 'Step 2',
    'CLASS' => 'modal--dialogue-chain',
    'BODY' => '<p></p>',
    'FOOTER' => $prevButtonTemplate('#modalStep1').$nextButtonTemplate('#modalStep3'),
]);
service()->templater()->render('kit:modal', [
    'ID' => 'modalStep3',
    'TITLE' => 'Step 3',
    'CLASS' => 'modal--dialogue-chain',
    'BODY' => '<p></p>',
    'FOOTER' => $prevButtonTemplate('#modalStep2').$endButtonTemplate,
]);

