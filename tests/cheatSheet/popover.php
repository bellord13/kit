

<div class="flex-line-wrap flex-col-count" style="--flex-col-count: 5">
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Popover on top',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'popover',
                'data-bs-placement' => 'top',
                'data-bs-content' => 'Top popover',
                'data-bs-container' => 'body',
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Popover on right',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'popover',
                'data-bs-placement' => 'right',
                'data-bs-content' => 'Right popover',
                'data-bs-container' => 'body',
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Popover on bottom',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'popover',
                'data-bs-placement' => 'bottom',
                'data-bs-content' => 'Bottom popover',
                'data-bs-container' => 'body',
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'Popover on left',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'popover',
                'data-bs-placement' => 'left',
                'data-bs-content' => 'Left popover',
                'data-bs-container' => 'body',
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => htmlspecialchars('<HTML> & header'),
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'popover',
                'data-bs-content' => 'And here\'s some amazing content. It\'s very engaging. <em>Right</em>?',
                'data-bs-container' => 'body',
                'data-bs-html' => 'true',
                'title' => '<em>Tooltip</em> <u>with</u> <b>HTML</b>'
            ]
        ]);
        ?>
    </div>
    <div class="flex-list-item">
        <?
        service()->templater()->render('kit:button', [
            'LABEL' => 'trigger focus',
            'CLASS' => '',
            'ATTR' => [
                'data-bs-toggle' => 'popover',
                'data-bs-content' => 'And here\'s some amazing content. It\'s very engaging. <em>Right</em>?',
                'data-bs-container' => 'body',
                'data-bs-html' => 'true',
                'data-bs-trigger' => 'focus',
                'title' => '<em>Tooltip</em> <u>with</u> <b>HTML</b>'
            ]
        ]);
        ?>
    </div>
</div>