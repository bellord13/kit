<div class="flex-list" style="--col-count: 3">

    <div class="flex-list-item"><?
        service()->templater()->render('kit:textarea', [
            'CLASS' => 'kit-field--check-mark-stroke',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'textarea',
            'VALUE' => '',
            'MAXLENGTH' => 1000,
            'LABEL' => 'textarea label',
        ]); ?>
    </div>

    <div class="flex-list-item"><?
        service()->templater()->render('kit:textarea', [
            'CLASS' => 'kit-field--check-mark-stroke',
            //'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'textarea',
            'VALUE' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'MAXLENGTH' => 1000,
            'LABEL' => 'textarea label',
        ]); ?>
    </div>
</div>