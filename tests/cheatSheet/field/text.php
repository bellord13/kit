<div class="flex-list" style="--col-count: 3">
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'text',
            'HINT' => 'time mask',
            'NAME' => 'time_mask',
            'VALUE' => '',
            'PLACEHOLDER' => '',
            'LABEL' => 'time mask',
            'ATTR' => [
                'data-mask' => '99:99'
            ]
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'text',
            'HINT' => 'text hint',
            'NAME' => 'input_name',
            'VALUE' => '10',
            'PLACEHOLDER' => 'number',
            'LABEL' => 'Test input',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'text',
            'HINT' => 'air-datepicker',
            'NAME' => 'datepicker',
            'VALUE' => '',
            'PLACEHOLDER' => '',
            'LABEL' => 'datepicker',
            'DATEPICKER' => true,
            'ATTR' => [
                'data-mask' => '99.99.9999'
            ]
        ]); ?>
    </div>


</div>