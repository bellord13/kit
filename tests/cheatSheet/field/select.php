<div class="flex-list" style="--col-count: 3">

    <div class="flex-list-item"><?
        service()->templater()->render('kit:select', [
            'HINT' => 'text hint',
            'NAME' => 'select',
            'LABEL' => 'select',
            'OPTIONS' => [
                [
                    'ID' => '1',
                    'NAME' => 'Значение выпадающего списка 1',
                ],
                [
                    'ID' => '2',
                    'NAME' => 'Значение выпадающего списка 2',
                ],
                [
                    'ID' => '3',
                    'NAME' => 'Значение выпадающего списка 3',
                ],
                [
                    'ID' => '4',
                    'NAME' => 'Значение выпадающего списка 4',
                ],
                [
                    'ID' => '5',
                    'NAME' => 'Значение выпадающего списка 5',
                ],
            ]
        ]); ?>
    </div>
</div>