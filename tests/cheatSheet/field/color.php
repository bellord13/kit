<div class="flex-list" style="--col-count: 3">
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'color',
            'HINT' => 'color picker by xu-ui',
            'NAME' => 'color',
            'VALUE' => '#1a8cff',
            'LABEL' => 'color',
        ]); ?>
    </div>
</div>