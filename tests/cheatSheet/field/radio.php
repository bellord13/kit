<div class="flex-list" style="--col-count: 3">

    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio',
            'VALUE' => 'radio1',
            'CHECKED' => true,
            'LABEL' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio',
            'VALUE' => 'radio2',
            'LABEL' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio',
            'VALUE' => 'radio3',
            'LABEL' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        ]); ?>
    </div>


    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-stroke',
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio-stroke',
            'VALUE' => 'radio4',
            'CHECKED' => true,
            'LABEL' => 'kit-field--check-mark-stroke',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-stroke',
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio-stroke',
            'VALUE' => 'radio5',
            'LABEL' => 'kit-field--check-mark-stroke',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-stroke',
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio-stroke',
            'VALUE' => 'radio6',
            'LABEL' => 'kit-field--check-mark-stroke',
        ]); ?>
    </div>


    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-material',
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio-material',
            'VALUE' => 'radio7',
            'CHECKED' => true,
            'LABEL' => 'kit-field--check-mark-material',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-material',
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio-material',
            'VALUE' => 'radio8',
            'LABEL' => 'kit-field--check-mark-material',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-material',
            'TYPE' => 'radio',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'radio-material',
            'VALUE' => 'radio9',
            'LABEL' => 'kit-field--check-mark-material',
        ]); ?>
    </div>
</div>