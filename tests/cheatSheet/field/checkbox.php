<div class="flex-list" style="--col-count: 3">
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'TYPE' => 'checkbox',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'agreement2',
            'VALUE' => 'Y',
            'CHECKED' => true,
            'LABEL' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-material',
            'TYPE' => 'checkbox',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'agreement',
            'VALUE' => 'Y',
            'LABEL' => 'kit-field--check-mark-material',
        ]); ?>
    </div>
    <div class="flex-list-item"><?
        service()->templater()->render('kit:input', [
            'CLASS' => 'kit-field--check-mark-stroke',
            'TYPE' => 'checkbox',
            'HINT' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis, consectetur hic illum in itaque, labore libero molestiae nisi, placeat possimus quasi quia quis ratione tempora voluptas voluptatum? Eaque, voluptatem.',
            'NAME' => 'agreement',
            'VALUE' => 'Y',
            'LABEL' => 'kit-field--check-mark-stroke',
        ]); ?>
    </div>
</div>