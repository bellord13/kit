<div class="flex-list" style="--col-count: 3">
    <div class="flex-list-item"><?
        $inputFile = new \Wt\Core\Form\InputFile($fieldName = 'MULTIPLE_FIELD_INPUT_FILE');
        $inputFile->multiple = true;
        $inputFile->hint = 'Поле для ввода файлов';
        $inputFile->render();
        ?>
    </div>
    <div class="flex-list-item"><?
        $inputFile = new \Wt\Core\Form\InputFile($fieldName = 'SINGLE_FIELD_INPUT_FILE');
        $inputFile->multiple = false;
        $inputFile->hint = 'Поле для ввода файлов';
        $inputFile->render();
        ?>
    </div>
</div>