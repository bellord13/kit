<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>

<script>
    kit.ready(['Tooltip2', 'testing'], function () {

        var unit = new kit.testing.Unit();

        unit.logger.info('Тестирование tooltip kit');

        var hint = new kit.Tooltip2('#master');
        hint.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint.run();


        var hint1m = new kit.align.Class('#master > mark', '#master');
        hint1m.setOptions({hDirection: 'center', hAlign: 'center', vAlign: 'top', vDirection: 'top'});


        unit.assertSame(true, !!hint, 'Проверка наличия класса');
        unit.assertSame(true, !!hint.viewer, 'Проверка наличия viewer');
        unit.assertSame(true, !!hint.align, 'Проверка наличия align');
        unit.assertSame(true, !!hint.action, 'Проверка наличия action');
        //unit.assertSame(hint, hint.masterNode.kit['tooltip2'], 'set kit data');

        var hint2 = new kit.Tooltip2('#master2');
        hint2.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint2.run();
        var x = 0;
        var n = 0;
        hint2.viewer.onShowBefore.push(function () {
            if(x) {
                return
            }
            x = setInterval(function () {
                hint2.setContent(++n);
                hint2.render();
            }.bind(this), 50);
        });
        hint2.viewer.onHideBefore.push(function () {
            clearInterval(x);x = 0;
        });

        var hint2m = new kit.align.Class('#master2 > mark', '#master2');
        hint2m.setOptions({hDirection: 'center', hAlign: 'center', vAlign: 'top', vDirection: 'top'});


        var hint3 = new kit.Tooltip2('#master3');
        hint3.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint3.run();
        hint3.setOptions({vAlign: 'bottom', vDirection: 'bottom'});

        var hint3m = new kit.align.Class('#master3 > mark', '#master3');
        hint3m.setOptions({hDirection: 'center', hAlign: 'center', vAlign: 'bottom', vDirection: 'bottom'});


        var hint4 = new kit.Tooltip2('#master4');
        hint4.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint4.run();
        hint4.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'center', vDirection: 'center'});

        var hint4m = new kit.align.Class('#master4 > mark', '#master4');
        hint4m.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'center', vDirection: 'center'});


        var hint5 = new kit.Tooltip2('#master5');
        hint5.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint5.run();
        hint5.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'top', vDirection: 'top'});

        var hint5m = new kit.align.Class('#master5 > mark', '#master5');
        hint5m.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'top', vDirection: 'top'});


        var hint6 = new kit.Tooltip2('#master6');
        hint6.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint6.run();
        hint6.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'bottom', vDirection: 'bottom'});

        var hint6m = new kit.align.Class('#master6 > mark', '#master6');
        hint6m.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'bottom', vDirection: 'bottom'});


        var hint7 = new kit.Tooltip2('#master7');
        hint7.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint7.run();
        hint7.setOptions({hDirection: 'right', hAlign: 'left', vAlign: 'bottom', vDirection: 'top'});

        var hint7m = new kit.align.Class('#master7 > mark', '#master7');
        hint7m.setOptions({hDirection: 'right', hAlign: 'left', vAlign: 'bottom', vDirection: 'top'});




        var hint8 = new kit.Tooltip2('#master8');
        hint8.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint8.run();
        hint8.setOptions({hDirection: 'right', hAlign: 'center', vAlign: 'top', vDirection: 'top'});

        var hint8_2 = new kit.Tooltip2('#master8');
        hint8_2.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint8_2.run();
        hint8_2.setOptions({hDirection: 'left', hAlign: 'center', vAlign: 'top', vDirection: 'top'});

        var hint8m = new kit.align.Class('#master8 > mark', '#master8');
        hint8m.offset = [1, 1];
        hint8m.setOptions({hDirection: 'left', hAlign: 'center', vAlign: 'top', vDirection: 'top'});

        var hint8m2 = new kit.align.Class('#master8 > mark2', '#master8');
        hint8m2.offset = [1, 1];
        hint8m2.setOptions({hDirection: 'right', hAlign: 'center', vAlign: 'top', vDirection: 'top'});



        var hint9 = new kit.Tooltip2('#master9');
        hint9.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint9.run();
        hint9.setOptions({hDirection: 'right', hAlign: 'center', vAlign: 'top', vDirection: 'top'});

        var hint9_2 = new kit.Tooltip2('#master9');
        hint9_2.setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aut fugiat inventore, modi provident quia repellendus saepe sed sequi sit');
        hint9_2.run();
        hint9_2.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'top', vDirection: 'bottom'});

        var hint9m = new kit.align.Class('#master9 > mark', '#master9');
        hint9m.offset = [1, 1];
        hint9m.setOptions({hDirection: 'right', hAlign: 'right', vAlign: 'top', vDirection: 'bottom'});

        var hint9m2 = new kit.align.Class('#master9 > mark2', '#master9');
        hint9m2.offset = [1, 1];
        hint9m2.setOptions({hDirection: 'right', hAlign: 'center', vAlign: 'top', vDirection: 'top'});


    });
</script>
<style>
    .test-block {
        padding: 100px;
        float: left;
    }
    .test-mater {
        width: 40px;height: 40px;background-color: #0a51ae;
    }
    mark, mark2 {
        display: block;
        width: 6px;
        height: 6px;
        background-color: red;
    }
</style>
<div  class="kit-wrapper">
    <div>
        <div class="test-block"><div id="master" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master2" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master3" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master4" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master5" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master6" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master7" class="test-mater"><mark></mark></div></div>
        <div class="test-block"><div id="master8" class="test-mater"><mark></mark><mark2></mark2></div></div>
        <div class="test-block"><div id="master9" class="test-mater"><mark></mark><mark2></mark2></div></div>

        <div class="test-block"><div class="test-mater" data-kit-tooltip2="autoload" data-kit-tooltip2-content="Инициализация по data"></div></div>
        <div style="clear: both"></div>
    </div>

    <div class="kit-logger"></div>
</div>

    <div style="width: 3000px; height: 3000px"></div>
<?
?>