<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<style>
    .hide {
        display: none;
    }
    .main-body {
        display: flex;
        width: 250px;
        height: 250px;
        justify-content: center;
        align-items: center;
        float: left;
    }
    .main-list {
        width: 100px;
        height: 100px;
        background-color: #ffc400;
        position: relative;
    }
    .main-item {
        width: 50px;
        height: 50px;
        color: whitesmoke;
        font-size: 10px;
        line-height: 1.2em;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
    }
</style>
<div class="kit-wrapper">
    <div class="main-body">
        <div id="object-main" class="main-list">
            <div id="object1" class="main-item" style="background-color: green;">'left', 'top', 'left', 'top'</div>
            <div id="object2" class="main-item" style="background-color: #0a51ae;">'right', 'top', 'right', 'top'</div>
            <div id="object3" class="main-item" style="background-color: #c23a3a;">'right', 'bottom', 'right', 'bottom'</div>
            <div id="object4" class="main-item" style="background-color: rebeccapurple;">'left', 'bottom', 'left', 'bottom'</div>
        </div>
    </div>
    <div class="main-body">
        <div id="object-main2" class="main-list">
            <div id="object5" class="main-item" style="background-color: green;">'left', 'top', 'right', 'top'</div>
            <div id="object6" class="main-item" style="background-color: #0a51ae;">'right', 'top', 'right', 'bottom'</div>
            <div id="object7" class="main-item" style="background-color: #c23a3a;">'right', 'bottom', 'left', 'bottom'</div>
            <div id="object8" class="main-item" style="background-color: rebeccapurple;">'left', 'bottom', 'left', 'top'</div>
        </div>
    </div>
    <div class="main-body">
        <div id="object-main3" class="main-list">
            <div id="object9" class="main-item" style="background-color: green;">'left', 'top', 'center', 'center'</div>
            <div id="object10" class="main-item" style="background-color: #0a51ae;">'right', 'top', 'center', 'center'</div>
            <div id="object11" class="main-item" style="background-color: #c23a3a;">'right', 'bottom', 'center', 'center'</div>
            <div id="object12" class="main-item" style="background-color: rebeccapurple;">'left', 'bottom', 'center', 'center'</div>
        </div>
    </div>
    <div class="main-body">
        <div id="object-main4" class="main-list">
            <div id="object13" class="main-item" style="background-color: green;">'center', 'center', 'left', 'top'</div>
            <div id="object14" class="main-item" style="background-color: #0a51ae;">'center', 'center', 'right', 'top'</div>
            <div id="object15" class="main-item" style="background-color: #c23a3a;">'center', 'center', 'right', 'bottom'</div>
            <div id="object16" class="main-item" style="background-color: rebeccapurple;">'center', 'center', 'left', 'bottom'</div>
        </div>
    </div>
</div>
<script>
    kit.ready(['align'], function () {
        var align1 = new kit.align.Class('#object1', '#object-main');
        align1.set('left', 'top', 'left', 'top');
        var align2 = new kit.align.Class('#object2', '#object-main');
        align2.set('right', 'top', 'right', 'top');
        var align3 = new kit.align.Class('#object3', '#object-main');
        align3.set('right', 'bottom', 'right', 'bottom');
        var align4 = new kit.align.Class('#object4', '#object-main');
        align4.set('left', 'bottom', 'left', 'bottom');

        var align5 = new kit.align.Class('#object5', '#object-main2');
        align5.set('left', 'top', 'right', 'top');
        var align6 = new kit.align.Class('#object6', '#object-main2');
        align6.set('right', 'top', 'right', 'bottom');
        var align7 = new kit.align.Class('#object7', '#object-main2');
        align7.set('right', 'bottom', 'left', 'bottom');
        var align8 = new kit.align.Class('#object8', '#object-main2');
        align8.set('left', 'bottom', 'left', 'top');

        var align9 = new kit.align.Class('#object9', '#object-main3');
        align9.set('left', 'top', 'center', 'center');
        var align10 = new kit.align.Class('#object10', '#object-main3');
        align10.set('right', 'top', 'center', 'center');
        var align11 = new kit.align.Class('#object11', '#object-main3');
        align11.set('right', 'bottom', 'center', 'center');
        var align12 = new kit.align.Class('#object12', '#object-main3');
        align12.set('left', 'bottom', 'center', 'center');

        var align13 = new kit.align.Class('#object13', '#object-main4');
        align13.offset = [5, 5];
        align13.set('center', 'center', 'left', 'top');
        var align14 = new kit.align.Class('#object14', '#object-main4');
        align14.offset = [5, 5];
        align14.set('center', 'center', 'right', 'top');
        var align15 = new kit.align.Class('#object15', '#object-main4');
        align15.offset = [5, 5];
        align15.set('center', 'center', 'right', 'bottom');
        var align16 = new kit.align.Class('#object16', '#object-main4');
        align16.offset = [5, 5];
        align16.set('center', 'center', 'left', 'bottom');

    });
</script>