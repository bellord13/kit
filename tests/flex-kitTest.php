<?php
?>
<style>
    div {
        border: 1px solid hsl(195, 100%, 50%);
        background: aliceblue;
    }
    * {
        box-sizing: border-box;
    }
</style>
<link rel="stylesheet" href="./../assets/flex-kit/flex-kit.css?<?=time()?>">

.flex-line
<div class="flex-line">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line //cell .flex-col
<div class="flex-line">
    <div class="flex-col">
        <div>1</div>
        <div>1</div>
        <div>1</div>
        <div>1</div>
    </div>
    <div></div>
    <div></div>
    <div class="flex-line">
        <div class="flex-col">
            <div>X</div>
            <div>************************</div>
            <div>X</div>
            <div class="flex-line-wrap">
                <div class="flex-cell-full-content flex-line-wrap flex-col-count" style="--flex-col-count: 3;">
                    <div>X</div>
                    <div>X<br>X</div>
                    <div>X</div>
                    <div>X</div>
                    <div>X</div>
                    <div>X</div>
                    <div>X<br>X<br>X</div>
                    <div>X</div>
                </div>
            </div>
            <div class="flex-line">
                <div class="flex-cell-full-content flex-line-wrap" >
                    <div>X</div>
                    <div>X<br>X</div>
                    <div>X</div>
                    <div>X</div>
                    <div>X</div>
                    <div>X</div>
                    <div>X<br>X<br>X</div>
                    <div>X</div>
                </div>
            </div>
        </div>
    </div>
    <div class="flex-cell-full-content">
        X
    </div>
</div>
.flex-line // cell: .flex-cell-full-content
<div class="flex-line">
    <div></div>
    <div class="flex-cell-full-content"></div>
    <div></div>
    <div></div>
</div>
.flex-line // cell: .flex-cell-full-content (2 cell)
<div class="flex-line">
    <div></div>
    <div class="flex-cell-full-content"></div>
    <div></div>
    <div class="flex-cell-full-content"></div>
</div>

.flex-col
<div class="flex-col">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>


.flex-line-wrap
<div class="flex-line-wrap">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>

.flex-line-wrap --flex-col-count:5
<div class="flex-line-wrap flex-col-count" style="--flex-col-count:5;">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:10
<div class="flex-line-wrap flex-col-count" style="--flex-col-count:10">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:2
<div class="flex-line-wrap flex-col-count" style="--flex-col-count:2">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:27
<div class="flex-line-wrap flex-col-count" style="--flex-col-count:27">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:5 #kirpich
<style>
    #kirpich > div:nth-child(20n+1) {
        --flex-col-count:20;
    }
    #kirpich > div:nth-child(20n+6) {
        --flex-col-count:6;
    }
    #kirpich > div:nth-child(20n+11) {
        --flex-col-count:10;
    }
</style>
<div id="kirpich" class="flex-line-wrap flex-col-count" style="--flex-col-count:5">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:27; --flex-aspect-ratio:0.5
<div class="flex-line-wrap flex-col-count flex-aspect-ratio" style="--flex-col-count:27; --flex-aspect-ratio:0.5">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:4; --flex-aspect-ratio:3
<div class="flex-line-wrap flex-col-count flex-aspect-ratio" id="test1" style="--flex-col-count:4; --flex-aspect-ratio:3">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div><div></div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-line-wrap --flex-col-count:5; --flex-aspect-ratio:2
<div class="flex-line-wrap flex-col-count flex-aspect-ratio" id="test1" style="--flex-col-count:5; --flex-aspect-ratio:2">
    <div><div><div>1</div></div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
.flex-col-wrap --flex-col-count:5; --flex-line-count:3; height: 200px;
<div class="flex-col-wrap flex-line-count flex-col-count" style="--flex-col-count:5; --flex-line-count:3; height: 200px;">
    <div><div>1</div></div>
    <div>2</div>
    <div>3</div>
    <div>4</div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
overflow
<div class="flex-col" style="height: 300px;">
    <div class="flex-line">
        <div></div>
        <div class="flex-cell-full-content"></div>
        <div></div>
    </div>
    <div class="flex-cell-full-content flex-col" style="overflow-y: scroll;">
        <div class="flex-col">
            <?
            foreach (range(1, 20) as $i) {
                echo "<div>text $i</div>";
            }
            ?>
        </div>
    </div>
    <div class="flex-line">
        <div></div>
        <div class="flex-cell-full-content"></div>
        <div></div>
    </div>
</div>

chat

<div id="chat" style="height: 300px;" class="flex-col">
    <div class="chat-header flex-line">
        <div class="header flex-line flex-cell-full-content">
            <div class="left-block"></div>
            <div class="title flex-cell-full-content">Чат города Хэдберн</div>
            <div class="right-block"></div>
        </div>
    </div>
    <div class="chat-content flex-col flex-cell-full-content" style="overflow-x: auto; overflow-y: visible;">
        <div class="room-wrapper flex-col"><div class="one-mess system"><span class="date">04:10:02</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">05:00:01</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>1 уровне</i></b> и <b><i>купленных свитках</i></b></span></div><div class="one-mess system"><span class="date">05:10:02</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">06:00:02</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>2 уровне</i></b> и <b><i>случайных свитках</i></b></span></div><div class="one-mess system"><span class="date">06:10:02</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">07:00:02</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>1 уровне</i></b> и <b><i>случайных свитках</i></b></span></div><div class="one-mess system"><span class="date">07:10:02</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">08:00:01</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>2 уровне</i></b> и <b><i>купленных свитках</i></b></span></div><div class="one-mess system"><span class="date">08:10:01</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">09:00:02</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>1 уровне</i></b> и <b><i>купленных свитках</i></b></span></div><div class="one-mess system"><span class="date">09:10:03</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">10:00:02</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>2 уровне</i></b> и <b><i>случайных свитках</i></b></span></div><div class="one-mess system"><span class="date">10:10:03</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">11:00:02</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>1 уровне</i></b> и <b><i>случайных свитках</i></b></span></div><div class="one-mess system"><span class="date">11:10:02</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">12:00:01</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>2 уровне</i></b> и <b><i>купленных свитках</i></b></span></div><div class="one-mess system"><span class="date">12:10:03</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess system"><span class="date">13:00:02</span><span> : </span><span class="msg">Через 10 минут состоится турнир в <b><i>левом зале</i></b> малого колизея на <b><i>3 уровне</i></b> и <b><i>купленных свитках</i></b></span></div><div class="one-mess system"><span class="date">13:10:02</span><span> : </span><span class="msg"><b>Турнир</b> в левом зале малого колизея <b>не состоялся</b> из-за малого количества участников</span></div><div class="one-mess"><span class="date">13:46:02</span><span> : </span><span class="msg">К нам приходит <span class="user-login" data-login="bellord">bellord</span></span></div></div></div>
    <div class="chat-footer flex-col"><div class="flex-line"><div class="flex-cell-full-content"><input type="text" name="mess" class="mess-text action--keypress-SendMessage"></div><div><button type="button" class="note-send action--click-SendMess">Ok</button></div><div><button type="button" class="select-smile action--click-SelectSmile"></button></div></div></div>
</div>