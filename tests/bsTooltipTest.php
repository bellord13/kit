<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>

<button aria-label="Tooltip on top" type="button" title="" class="kit-button kit-button--primary kit-button--radius  not-preloader " onclick=""
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        data-bs-original-title="Tooltip on top">
    <div class="kit-button__icon kit-icon"></div>
    <div class="kit-button__label kit-line-clamp">Tooltip on top</div>
    <div class="kit-button__border"></div>
</button>

<script>
    kit.ready(['tooltip', 'testing' ], function () {


    });
</script>

<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>