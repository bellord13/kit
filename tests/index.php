<script src="../assets/config/config.js"></script>
<script src="../assets/core/core.plugins.config.js"></script>
<script src="../assets/core/core.js"></script>

<div style="display: flex; flex-direction: column; align-items: center;">
    <script>
        var iframeList = [], iframe;
    </script>
    <?php
    $arFilesItems = glob(__DIR__ . '/*Test.php');
    foreach ($arFilesItems as $file) {
        $file = basename($file);
        $clName = substr($file, 0, -8);
        //echo "<a href=\"$file\"><b>$clName</b></a>";
        ?>
        <script>
            iframe = document.createElement("frame");
            iframe.src = '<?=$file?>';
            iframeList.push({
                fileName: '<?=$file?>',
                clName: '<?=$clName?>',
                iframe: iframe,
            });
        </script>
        <?
    }
    ?>
</div>
<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>
<script>
    kit.ready(['Collection', 'flexKit', 'log', 'support'], function () {

        var support = kit.support;
        window.frameList = new kit.Collection(iframeList);

        frameList.map(function (frameData) {
            kit.service.logger.notice('<a href="'+frameData.fileName+'">'+frameData.clName+'</a>');
        });

        kit.service.logger.notice(' ');
        kit.service.logger.warning('FRAME LIST <span id="total-unit-testing-counter">...<span>');
        kit.service.logger.notice(' ');

        var totalCountTest = 0;
        var totalCountTestNode = document.getElementById('total-unit-testing-counter');
        //console.log(frameList);
        frameList.map(function (data) {
            data.iframe.onload = function() {
                data.window = data.iframe.contentWindow;
                data.document = data.iframe.contentWindow.document;
                data.kit = data.window.kit;
                if(!data.kit) {
                    return;
                }
                data.kit.ready([], function () {
                    setTimeout(function () {
                        var result = {
                            fail: new kit.Collection(),
                            warn: new kit.Collection(),
                            pass: new kit.Collection(),
                        };
                        var countTest = 0;
                        if(!data.kit.service.logger){
                            return;
                        }

                        function pushResult(log){
                            var acceptKeys = new kit.Collection(['fail', 'warn', 'pass']);
                            acceptKeys.each(function (key) {
                                if(!support.isObject(log.message)) {
                                    return;
                                }
                                if(log.message.hasOwnProperty(key)) {
                                    result[key].push(log);
                                    countTest++;
                                    totalCountTest++;
                                    if(totalCountTestNode) {
                                        totalCountTestNode.innerHTML = totalCountTest.toString();
                                    }
                                    return false;
                                }
                            });
                        }
                        data.kit.service.logger.collection.map(function (log) {
                            pushResult(log);
                        });


                        kit.service.logger.info(data.clName);

                        if(result.pass.count()){
                            kit.service.logger.success({
                                pass: result.pass.count(),
                            });
                        }
                        if(result.warn.count()) {
                            kit.service.logger.warning({
                                warn: result.warn.count(),
                            }, result.warn.map(function (log) {
                                return {
                                    expected: log.context.expected,
                                    actual: log.context.actual,
                                    message: log.message.warn,
                                };
                            }).toObject());
                        }
                        if(result.fail.count()) {
                            kit.service.logger.error({
                                fail: result.fail.count(),
                            }, result.fail.map(function (log) {
                                return {
                                    expected: log.context.expected,
                                    actual: log.context.actual,
                                    message: log.message.fail,
                                };
                            }).toObject());
                        }
                    }, 1000);
                });
            };
            document.body.appendChild(data.iframe);
        })
    });
</script>