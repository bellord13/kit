<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(\Bitrix\Main\Loader::includeModule('wt.core')) {
    app()->service()->assets()->setPlugins([
        'kit:config',
        'kit:flex-kit',
        'kit:core',
        'kit:bsCore',
        'kit:collapse',
        'kit:site',
        'kit:toolkit',
        'kit:theme',
    ])->render();
}
$APPLICATION->ShowHead();

?>
<script>
    kit.ready(['mustache', 'testing'], function () {
        const view = {
            title: "Joe",
            calc: () => ( 2 + 4 )
        };
        var unit = new kit.testing.Unit();
        unit.logger.info(kit.mustache.version);
        const output = kit.mustache.render("{{title}} spends {{calc}}", view);

        unit.assertSame('Joe spends 6', output, 'countTime');
    });
</script>

<div  class="kit-wrapper">
    <div class="kit-logger"></div>
</div>
<?
?>